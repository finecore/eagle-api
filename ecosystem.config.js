module.exports = {
  apps: [
    {
      name: "eagle-api",
      script: "build/index.js",
      watch: false,
      max_memory_restart: "50M",
      out_file: "/dev/null",
      error_file: "/dev/null",
    },
  ],
};
