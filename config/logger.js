const { createLogger, format, transports } = require("winston");

require("winston-daily-rotate-file");
require("date-utils");

const logDir = "log";

const dailyRotateFileTransport = new transports.DailyRotateFile({
  filename: `${logDir}/%DATE%-app.log`,
  datePattern: "YYYY-MM-DD",
  zippedArchive: false,
  maxSize: "10m",
  maxFiles: "30d",
});

const dailyRotateFileTransportError = new transports.DailyRotateFile({
  filename: `${logDir}/%DATE%-err.log`,
  datePattern: "YYYY-MM-DD",
  zippedArchive: false,
  maxSize: "10m",
  maxFiles: "30d",
});

const logger = createLogger({
  // change level if in dev environment versus production
  level: "debug",
  format: format.combine(
    format.timestamp({
      format: "YYYY-MM-DD HH:mm:ss",
    }),
    format.printf((info) => `${info.timestamp} ${info.level}: ${info.message}`)
  ),
  transports: [
    // write console log (stop)
    // new transports.Console({
    //   level: "debug",
    //   format: format.combine(
    //     format.colorize(),
    //     format.printf((info) => `${info.timestamp} ${info.level}: ${info.message}`)
    //   ),
    // }),
    dailyRotateFileTransport,
  ],
  exceptionHandlers: [
    // new transports.Console({
    //   level: "error",
    //   format: format.combine(
    //     format.colorize(),
    //     format.printf((info) => `${info.timestamp} ${info.level}: ${info.message}`)
    //   ),
    // }),
    dailyRotateFileTransportError,
  ],
});

logger.br = (line) => {
  logger.debug("\r\n");
};

logger.substr = (data, len = 200) => {
  if (!data) return "";

  let logdate = typeof data === "object" ? JSON.stringify(data) : data;

  return logdate.length > len
    ? logdate.substring(0, len / 2) + "\n    .............................. skip ...............................  \n" + logdate.substring(logdate.length - len / 2, logdate.length - 1)
    : logdate;
};

module.exports = logger;
