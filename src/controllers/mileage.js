import Mileage from "model/mileage";
import MileageLog from "model/mileage-log";
import { jsonRes, sendRes, isValidToken, checkPermission } from "utils/api-util";
import { ERROR } from "constants/constants";
import _ from "lodash";

const list_a_mileages = (req, res) => {
  let { filter = "1=1", limit = "10000", order = "a.id", desc = "desc" } = req.params;

  Mileage.selectMileageCount(filter, (err, count) => {
    if (err) {
      jsonRes(req, res, err, { count });
    } else
      Mileage.selectMileages(filter, order, desc, limit, (err, mileages) => {
        jsonRes(req, res, err, { count, mileages });
      });
  });
};

const list_place_mileages = (req, res) => {
  const { place_id } = req.params;

  if (!place_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide place_id for get mileage list");
  } else {
    Mileage.selectPlaceMileages(place_id, (err, mileages) => {
      jsonRes(req, res, err, { mileages });
    });
  }
};

const read_a_mileage = (req, res) => {
  let { id, phone } = req.params;

  if (!id && !phone) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide mileage id or phone for select");
  } else {
    if (phone) phone = phone.replace(/[^\d]/g, "");
    Mileage.selectMileage(id, phone, (err, mileage) => {
      jsonRes(req, res, err, { mileage });
    });
  }
};

const read_place_phone_mileage = (req, res) => {
  let { place_id, phone } = req.params;

  if (!place_id || !phone) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide mileage place_id and phone for select");
  } else {
    phone = phone.replace(/[^\d]/g, "");
    Mileage.selectPlaceMileageByPhone(place_id, phone, (err, mileage) => {
      jsonRes(req, res, err, { mileage });
    });
  }
};

const create_a_mileage = (req, res) => {
  let { mileage } = req.body;

  if (!mileage || !mileage.place_id || !mileage.phone) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide mileage data for insert");
  } else {
    mileage.phone = mileage.phone.replace(/[^\d]/g, "");

    Mileage.selectPlaceMileageByPhone(mileage.place_id, mileage.phone, (err, result) => {
      if (result[0]) {
        jsonRes(req, res, ERROR.DUPLICATE_DATA, "");
      } else {
        Mileage.insertMileage(mileage, (err, info) => {
          if (info && info.insertId) req.body.mileage.id = info.insertId; // 채널 전송 시 id 입력.

          jsonRes(req, res, err, { info }, "Mileage successfully inserted");

          if (!err) {
            let mileage_log = {
              place_id: mileage.place_id,
              mileage_id: info.insertId,
              user_id: mileage.user_id,
              phone: mileage.phone,
              change_point: mileage.point,
              point: mileage.point,
              type: 1, // 1: 관리자 변경, 2: 사용자 적립, 3: 사용자 사용
            };

            if (mileage_log.change_point != undefined && mileage_log.change_point !== 0) MileageLog.insertMileageLog(mileage_log, (err, info) => {});
          }
        });
      }
    });
  }
};

const update_a_mileage = (req, res) => {
  const { id } = req.params;
  let { mileage } = req.body;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide mileage id for update");
  } else {
    if (mileage.phone) mileage.phone = mileage.phone.replace(/[^\d]/g, "");

    Mileage.selectMileage(id, null, (err, result) => {
      if (!err && result[0]) {
        let change_point = Number(mileage.point) - Number(result[0].point);

        Mileage.updateMileage(id, mileage, (err, info) => {
          jsonRes(req, res, err, { info }, "Mileage successfully updated");

          mileage = _.merge({}, result[0], mileage);

          if (!err) {
            let mileage_log = {
              place_id: mileage.place_id,
              mileage_id: mileage.id,
              user_id: mileage.user_id,
              phone: mileage.phone,
              change_point,
              point: mileage.point,
              type: mileage.type, // 1: 관리자 변경, 2: 사용자 적립, 3: 사용자 사용
            };

            if (mileage_log.change_point != undefined && mileage_log.change_point !== 0) MileageLog.insertMileageLog(mileage_log, (err, info) => {});
          }
        });
      } else {
        jsonRes(req, res, ERROR.NO_DATA, "");
      }
    });
  }
};

const update_place_mileage_phone = (req, res) => {
  let { place_id, phone } = req.params;
  let { mileage } = req.body;

  if (!place_id || !phone) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide mileage place_id and phone for update");
  } else {
    phone = phone.replace(/[^\d]/g, "");

    Mileage.selectPlaceMileageByPhone(place_id, phone, (err, result) => {
      if (!err && result[0]) {
        let change_point = Number(mileage.point) - Number(result[0].point);

        Mileage.updatePlaceMileageByPhone(place_id, phone, mileage, (err, info) => {
          jsonRes(req, res, err, { info, mileage }, "Mileage successfully updated");

          mileage = _.merge({}, result[0], mileage);

          if (!err) {
            let mileage_log = {
              place_id: mileage.place_id,
              mileage_id: mileage.id,
              user_id: mileage.user_id,
              phone: mileage.phone,
              change_point,
              point: mileage.point,
              type: mileage.type, // 1: 관리자 변경, 2: 사용자 적립, 3: 사용자 사용
            };

            if (mileage_log.change_point != undefined && mileage_log.change_point !== 0) MileageLog.insertMileageLog(mileage_log, (err, info) => {});
          }
        });
      } else {
        jsonRes(req, res, ERROR.NO_DATA, "");
      }
    });
  }
};

const increase_place_mileage_phone = (req, res) => {
  let { place_id, phone, user_id, point, rollback = 2 } = req.params;

  if (!place_id || !phone || !user_id || !point) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide mileage place_id ,phone ,user_id ,point for increase");
  } else {
    phone = phone.replace(/[^\d]/g, "");

    Mileage.selectPlaceMileageByPhone(place_id, phone, (err, result) => {
      if (!err && result[0]) {
        let { id: mileage_id, point: mileage_point } = result[0];

        Mileage.increaseMileage(mileage_id, point, (err, info) => {
          let mileage = result[0];
          mileage.point = Number(mileage_point) + Number(point);

          jsonRes(req, res, err, { info, mileage }, "Mileage successfully updated");

          if (!err) {
            let mileage_log = {
              place_id,
              mileage_id,
              user_id,
              phone,
              change_point: point,
              point: mileage.point,
              type: rollback === "1" ? 5 : 2, // 1: 관리자 변경, 2: 고객 적립, 3: 고객 사용, 4: 적립 취소, 5: 사용 취소
            };

            if (mileage_log.change_point != undefined && mileage_log.change_point !== 0) MileageLog.insertMileageLog(mileage_log, (err, info) => {});
          }
        });
      } else {
        jsonRes(req, res, ERROR.NO_DATA, "");
      }
    });
  }
};

const decrease_place_mileage_phone = (req, res) => {
  let { place_id, phone, user_id, point, rollback } = req.params;

  // console.log("- decrease_place_mileage_phone", { place_id, phone, user_id, point, rollback });

  if (!place_id || !phone || !user_id || !point) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide mileage place_id ,phone ,user_id ,point for decrease");
  } else {
    phone = phone.replace(/[^\d]/g, "");

    Mileage.selectPlaceMileageByPhone(place_id, phone, (err, result) => {
      if (!err && result[0]) {
        let { id: mileage_id, point: mileage_point } = result[0];

        Mileage.decreaseMileage(mileage_id, point, (err, info) => {
          let mileage = result[0];
          mileage.point = Number(mileage_point) - Number(point);

          jsonRes(req, res, err, { info, mileage }, "Mileage successfully updated");

          if (!err) {
            let mileage_log = {
              place_id,
              mileage_id,
              user_id,
              phone: phone,
              change_point: -point,
              point: mileage.point,
              type: rollback === "1" ? 4 : 3, // 1: 관리자 변경, 2: 고객 적립, 3: 고객 사용, 4: 적립 취소, 5: 사용 취소
            };

            if (mileage_log.change_point != undefined && mileage_log.change_point !== 0) MileageLog.insertMileageLog(mileage_log, (err, info) => {});
          }
        });
      } else {
        jsonRes(req, res, ERROR.NO_DATA, "");
      }
    });
  }
};

const delete_a_mileage = (req, res) => {
  const { id, user_id } = req.params;
  let { mileage } = req.body;

  console.log("- delete_a_mileage", id, user_id);

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide mileage id for delete");
  } else {
    Mileage.selectMileage(id, null, (err, result) => {
      if (!err && result[0]) {
        Mileage.deleteMileage(id, (err, info) => {
          jsonRes(req, res, err, { info }, "Mileage successfully deleted");

          mileage = _.merge({}, result[0], mileage);

          if (!err) {
            let mileage_log = {
              place_id: mileage.place_id,
              mileage_id: mileage.id,
              user_id: user_id || mileage.user_id,
              phone: mileage.phone,
              change_point: -result[0].point,
              point: 0,
              type: mileage.type, // 1: 관리자 변경, 2: 사용자 적립, 3: 사용자 사용
            };

            if (mileage_log.change_point != undefined && mileage_log.change_point !== 0) MileageLog.insertMileageLog(mileage_log, (err, info) => {});
          }
        });
      } else {
        jsonRes(req, res, ERROR.NO_DATA, "");
      }
    });
  }
};

export default {
  list_a_mileages,
  list_place_mileages,
  create_a_mileage,
  read_a_mileage,
  read_place_phone_mileage,
  update_a_mileage,
  update_place_mileage_phone,
  increase_place_mileage_phone,
  decrease_place_mileage_phone,
  delete_a_mileage,
};
