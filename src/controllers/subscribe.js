import Subscribe from "model/subscribe";
import File from "model/file";
import { jsonRes, sendRes, isValidToken, checkPermission } from "utils/api-util";
import { ERROR } from "constants/constants";

const list_a_subscribes = (req, res) => {
  let { filter = "1=1", limit = "10000", order = "a.reg_date", desc = "desc" } = req.params;

  Subscribe.selectSubscribeCount(filter, (err, count) => {
    if (err) {
      jsonRes(req, res, err, { count });
    } else
      Subscribe.selectSubscribes(filter, order, desc, limit, (err, subscribes) => {
        jsonRes(req, res, err, { count, subscribes });
      });
  });
};

const read_a_subscribe = (req, res) => {
  const { id } = req.params;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide subscribe id for select");
  } else {
    Subscribe.selectSubscribe(id, (err, subscribe) => {
      jsonRes(req, res, err, { subscribe });
    });
  }
};

const create_a_subscribe = (req, res) => {
  const { subscribe } = req.body;

  Subscribe.insertSubscribe(subscribe, (err, info) => {
    if (info && info.insertId) req.body.subscribe.id = info.insertId; // 채널 전송 시 id 입력.

    jsonRes(req, res, err, { info }, "Subscribe successfully inserted");
  });
};

const update_a_subscribe = (req, res) => {
  const { id } = req.params;
  const { subscribe } = req.body;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide subscribe id for update");
  } else {
    Subscribe.updateSubscribe(id, subscribe, (err, info) => {
      jsonRes(req, res, err, { info }, "Subscribe successfully updated");
    });
  }
};

const delete_a_subscribe = (req, res) => {
  const { id } = req.params;
  const { subscribe } = req.body;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide subscribe id for delete");
  } else {
    Subscribe.deleteSubscribe(id, (err, info) => {
      jsonRes(req, res, err, { info }, "Subscribe successfully deleted");
    });
  }
};

export default {
  list_a_subscribes,
  create_a_subscribe,
  read_a_subscribe,
  update_a_subscribe,
  delete_a_subscribe,
};
