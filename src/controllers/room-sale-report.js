import RoomSaleReport from "model/room-sale-report";
import RoomState from "model/room-state";

import { jsonRes, sendRes, isValidToken, checkPermission } from "utils/api-util";
import { ERROR } from "constants/constants";
import moment from "moment";

import makeExcel from "helper/excel-helper";

const list_place_sale_hours_report = (req, res) => {
  const { place_id, between = "a.reg_date", begin, end, order = "a.reg_date", limit } = req.params;

  RoomSaleReport.selectPlaceSaleReportHoursCount(place_id, between, begin, end, (err, count) => {
    if (err) {
      jsonRes(req, res, err, { count });
    } else
      RoomSaleReport.selectPlaceSaleReportHours(place_id, between, begin, end, order, limit, (err, report) => {
        jsonRes(req, res, err, { count, report });
      });
  });
};

const list_place_sale_days_report = (req, res) => {
  const { place_id, between = "a.reg_date", begin, end, order = "a.reg_date", limit } = req.params;

  RoomSaleReport.selectPlaceSaleReportDaysCount(place_id, between, begin, end, (err, count) => {
    if (err) {
      jsonRes(req, res, err, { count });
    } else
      RoomSaleReport.selectPlaceSaleReportDays(place_id, between, begin, end, order, limit, (err, report) => {
        jsonRes(req, res, err, { count, report });
      });
  });
};

const list_place_sale_months_report = (req, res) => {
  const { place_id, between = "a.reg_date", begin, end, order = "a.reg_date", limit } = req.params;

  RoomSaleReport.selectPlaceSaleReportMonthsCount(place_id, between, begin, end, (err, count) => {
    if (err) {
      jsonRes(req, res, err, { count });
    } else
      RoomSaleReport.selectPlaceSaleReportMonths(place_id, between, begin, end, order, limit, (err, report) => {
        jsonRes(req, res, err, { count, report });
      });
  });
};

const list_place_sale_years_report = (req, res) => {
  const { place_id, between = "a.reg_date", begin, end, order = "a.reg_date", limit } = req.params;

  RoomSaleReport.selectPlaceSaleReportYearsCount(place_id, between, begin, end, (err, count) => {
    if (err) {
      jsonRes(req, res, err, { count });
    } else
      RoomSaleReport.selectPlaceSaleReportYears(place_id, between, begin, end, order, limit, (err, report) => {
        jsonRes(req, res, err, { count, report });
      });
  });
};

export default {
  list_place_sale_hours_report,
  list_place_sale_days_report,
  list_place_sale_months_report,
  list_place_sale_years_report,
};
