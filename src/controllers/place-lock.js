import PlaceLock from "model/place-lock";
import { jsonRes, sendRes, isValidToken, checkPermission } from "utils/api-util";
import { ERROR } from "constants/constants";
import moment from "moment";

const list_all_place_lock = (req, res) => {
  const { place_id } = req.params;

  if (!place_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide place_id for get place lock list");
  } else {
    PlaceLock.selectAllPlaceLocks(place_id, (err, all_place_locks) => {
      jsonRes(req, res, err, { all_place_locks });
    });
  }
};

const read_a_place_lock = (req, res) => {
  if (!req.params.id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide place lock id for select");
  } else {
    PlaceLock.selectPlaceLock(req.params.id, (err, place_lock) => {
      jsonRes(req, res, err, { place_lock });
    });
  }
};

const create_a_place_lock = (req, res) => {
  var new_place_lock = req.body.place_lock; // post body.

  if (!new_place_lock.place_id || !new_place_lock.user_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide place lock data for insert");
  } else {
    PlaceLock.insertPlaceLock(new_place_lock, (err, info) => {
      if (info && info.insertId) req.body.place_lock.id = info.insertId; // 채널 전송 시 id 입력.

      jsonRes(req, res, err, { info }, "PlaceLock successfully inserted");
    });
  }
};

const update_a_place_lock = (req, res) => {
  const { id } = req.params;
  let { place_lock } = req.body; // post body.

  if (!id || !place_lock) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide place lock data for update");
  } else {
    PlaceLock.updatePlaceLock(id, place_lock, (err, info) => {
      jsonRes(req, res, err, { info }, "PlaceLock successfully updated");
    });
  }
};

const delete_a_place_lock = (req, res) => {
  const { id } = req.params;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide place lock id for delete");
  } else {
    PlaceLock.deletePlaceLock(id, (err, info) => {
      jsonRes(req, res, err, { info }, "PlaceLock successfully deleted");
    });
  }
};

const delete_all_place_lock = (req, res) => {
  if (!req.params.place_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide place_id for delete");
  } else {
    PlaceLock.deleteAllPlaceLock(req.params.place_id, (err, info) => {
      jsonRes(req, res, err, { info }, "PlaceLock successfully all deleted");
    });
  }
};

export default {
  list_all_place_lock,
  read_a_place_lock,
  create_a_place_lock,
  update_a_place_lock,
  delete_a_place_lock,
  delete_all_place_lock,
};
