import RoomType from "model/room-type";
import { jsonRes, sendRes, isValidToken, checkPermission } from "utils/api-util";
import { ERROR } from "constants/constants";

const list_all_room_type = (req, res) => {
  if (!req.params.place_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide place_id for get room type list");
  } else {
    RoomType.selectAllRoomTypes(req.params.place_id, (err, all_room_types) => {
      jsonRes(req, res, err, { all_room_types });
    });
  }
};

const read_a_room_type = (req, res) => {
  if (!req.params.id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room type id or name for select");
  } else {
    RoomType.selectRoomType(req.params.id, (err, room_type) => {
      jsonRes(req, res, err, { room_type });
    });
  }
};

const create_a_room_type = (req, res) => {
  var new_room_type = req.body.room_type; // post body.

  if (!new_room_type.name || !new_room_type.place_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room type data for insert");
  } else {
    RoomType.insertRoomType(new_room_type, (err, info) => {
      if (info && info.insertId) req.body.room_type.id = info.insertId; // 채널 전송 시 id 입력.

      jsonRes(req, res, err, { info }, "RoomType successfully inserted");
    });
  }
};

const update_a_room_type = (req, res) => {
  const { id } = req.params;
  let { room_type } = req.body; // post body.

  if (!id || !room_type) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room type id for update");
  } else {
    RoomType.updateRoomType(id, room_type, (err, info) => {
      jsonRes(req, res, err, { info }, "RoomType successfully updated");
    });
  }
};

const delete_a_room_type = (req, res) => {
  const { id } = req.params;
  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room type id for delete");
  } else {
    RoomType.deleteRoomType(id, (err, info) => {
      jsonRes(req, res, err, { info }, "RoomType successfully deleted");
    });
  }
};

const delete_all_room_type = (req, res) => {
  if (!req.params.place_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room type id for delete");
  } else {
    RoomType.deleteAllRoomTypes(req.params.place_id, (err, info) => {
      jsonRes(req, res, err, { info }, "RoomType successfully all deleted");
    });
  }
};

export default {
  list_all_room_type,
  create_a_room_type,
  read_a_room_type,
  update_a_room_type,
  delete_a_room_type,
  delete_all_room_type,
};
