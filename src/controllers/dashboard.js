import DashBoard from "model/dashboard";
import { jsonRes, sendRes, isValidToken, checkPermission } from "utils/api-util";
import { ERROR, PREFERENCES } from "constants/constants";
import moment from "moment";

const total_place_sido = (req, res) => {
  DashBoard.selectTotalPlaceSido((err, result) => {
    jsonRes(req, res, err, { result });
  });
};

const total_now_ieg = (req, res) => {
  DashBoard.selectTotalNowIeg((err, result) => {
    jsonRes(req, res, err, { result });
  });
};

const list_now_ieg = (req, res) => {
  let { place_id, filter = "1=1", limit = "10000", order = "a.reg_date", desc = "desc" } = req.params;

  if (place_id) filter = `a.place_id=${place_id}`;

  DashBoard.selectListNowIeg(filter, order, desc, limit, (err, result) => {
    jsonRes(req, res, err, { result });
  });
};

const total_now_isg = (req, res) => {
  DashBoard.selectTotalNowIsg((err, result) => {
    jsonRes(req, res, err, { result });
  });
};

const list_now_isg = (req, res) => {
  let { place_id, filter = "1=1", limit = "10000", order = "a.reg_date", desc = "desc" } = req.params;

  if (place_id) filter = `a.place_id=${place_id}`;

  DashBoard.selectListNowIsg(filter, order, desc, limit, (err, result) => {
    jsonRes(req, res, err, { result });
  });
};

const total_reserv_sum = (req, res) => {
  let { filter = "1=1", begin = moment().format("YYYY-MM-DD 00:00"), end = moment().format("YYYY-MM-DD 59:59"), type = "day", groups = "stay_type" } = req.params;

  let format = type === "day" ? "'%Y-%m-%d %H'" : type === "week" ? "'%Y-%m-%d'" : "'%Y-%m-%d'";

  DashBoard.selectTotalReservSum(filter, begin, end, format, groups, (err, result) => {
    jsonRes(req, res, err, { result });
  });
};

const total_room_status_sum = (req, res) => {
  let { filter = "1=1", groups = "signal" } = req.params;

  DashBoard.selectTotalRoomStatusSum(filter, groups, (err, result) => {
    jsonRes(req, res, err, { result });
  });
};

const total_isg_status_sum = (req, res) => {
  let { filter = "1=1", groups = "state" } = req.params;

  DashBoard.selectTotalIsgStatusSum(filter, groups, (err, result) => {
    jsonRes(req, res, err, { result });
  });
};

const list_now_event = (req, res) => {
  let { place_id, filter = "1=1", limit = "100" } = req.params;

  if (place_id) filter = `b.place_id=${place_id}`;

  DashBoard.selectListNowEvent(filter, limit, (err, result) => {
    jsonRes(req, res, err, { result });
  });
};

const total_report_ieg = (req, res) => {
  let { filter = "1=1", begin = moment().format("YYYY-MM-DD 00:00"), end = moment().format("YYYY-MM-DD 59:59"), type = "day", groups = "stay_type" } = req.params;

  let format = type === "year" ? "'%Y'" : type === "month" ? "'%Y-%m'" : "'%Y-%m-%d'";

  DashBoard.selectTotalReportIeg(filter, begin, end, format, groups, (err, result) => {
    jsonRes(req, res, err, { result });
  });
};

const total_report_isg = (req, res) => {
  let { filter = "1=1", begin = moment().format("YYYY-MM-DD 00:00"), end = moment().format("YYYY-MM-DD 59:59"), type = "day", groups = "stay_type" } = req.params;

  let format = type === "year" ? "'%Y'" : type === "month" ? "'%Y-%m'" : "'%Y-%m-%d'";

  DashBoard.selectTotalReportIsg(filter, begin, end, format, groups, (err, result) => {
    jsonRes(req, res, err, { result });
  });
};

export default {
  total_place_sido,
  total_now_ieg,
  total_now_isg,
  list_now_ieg,
  list_now_isg,
  total_reserv_sum,
  total_room_status_sum,
  total_isg_status_sum,
  list_now_event,
  total_report_ieg,
  total_report_isg,
};
