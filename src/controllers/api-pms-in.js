// import ApiPms from "model/api-pms";
import {jsonRes, sendRes, isValidToken, checkPermission} from "utils/api-util";
import {ERROR, PREFERENCES} from "constants/constants";
import {STATE_CODE_TO_VLUE, STATE_VALUE_TO_TXT, STATE_VALUE_TO_CODE, RES_JSON} from "constants/api-constants";

import jwt from "jsonwebtoken";
import {verify, sign} from "utils/jwt-util";
import crypto from "crypto";

import roomState from "controllers/room-state";
import roomSale from "controllers/room-sale";

import RoomSale from "model/room-sale";
import RoomSalePay from "model/room-sale-pay";
import RoomState from "model/room-state";
import User from "model/user";

import moment from "moment";
import _ from "lodash";

const create_token = (req, res) => {
  let {
    headers: {channel = "pms"},
    body: {id, pwd},
    params: {pms = ""},
  } = req;

  console.log("create_token request:", {id, pwd});

  if (pwd && pwd.length < 20) {
    // 해시 생성
    var shasum = crypto.createHash("sha1");
    shasum.update(pwd);

    // 비밀번호 sha1 형식으로 암호화.
    pwd = shasum.digest("hex");
    console.log("- pwd sha1 ", pwd);
  } else {
    return RES_JSON(pms, res, 400, "비밀번호를 확인해 주세요.");
  }

  // query.
  User.selectUserByIdPwd(id, pwd, (err, rows) => {
    let user = rows ? rows[0] : null;

    if (err) {
      return RES_JSON(pms, res, 500, JSON.stringify(err));
    } else if (!user) {
      return RES_JSON(pms, res, 400, "아이디 와 비밀번호를 확인해 주세요.");
    } else if (user.pwd != pwd) {
      return RES_JSON(pms, res, 400, "비밀번호가 맞지 않습니다.");
    } else if (user.use_yn !== "Y") {
      return RES_JSON(pms, res, 400, "이용 정지된 사용자 입니다.");
    } else if (!user.pms) {
      return RES_JSON(pms, res, 400, "사용자의 PMS 설정을 먼저해 주세요.");
    } else if (user.pms.toUpperCase() !== pms.toUpperCase()) {
      return RES_JSON(pms, res, 400, `업소의 PMS(${user.pms}) 와 API 요청 PMS(${pms})가 다릅니다.`);
    } else {
      // JWT(Json Web Token) 생성.
      const {channel, id, place_id, level, type} = user;
      const payload = {pms, channel, id, place_id, level, type, mode: process.env.REACT_APP_MODE}; // mode 는 소켓 토큰 에서 API 검증 시 사용.

      sign(payload, function (err, token) {
        return RES_JSON(pms, res, 200, null, {token});
      });
    }
  });
};

const read_all_room = (req, res) => {
  let {pms} = req.params;
  let {
    auth: {place_id},
  } = req;

  if (!pms) {
    return RES_JSON(pms, res, 500, "전체객실 상태 요청 파라메터가 올바르지 않습니다.", {pms});
  } else {
    pms = pms.toUpperCase();

    RoomState.selectRoomStateAll(place_id, (err, room_states) => {
      console.log("-> place_id", place_id);

      let code = 200;
      let roomstates = "";

      if (err) {
        code = 500;
      } else {
        _.each(room_states, (data, idx) => {
          if (data) {
            let code = STATE_VALUE_TO_CODE(pms, data); // 객실상태
            roomstates += (roomstates ? "/" : "") + code;
          }
        });
      }

      console.log("- roomstates", roomstates);

      return RES_JSON(pms, res, code, null, {roomstates});
    });
  }
};

const read_room = (req, res) => {
  let {pms, room} = req.params;
  let {
    auth: {place_id},
  } = req;

  if (!pms || !room) {
    return RES_JSON(pms, res, 500, "객실 상태 요청 파라메터가 올바르지 않습니다.", {pms, room});
  } else {
    pms = pms.toUpperCase();

    RoomState.selectRoomStateByName(place_id, room, (err, room_state) => {
      console.log("-> read_room", room, room_state);

      let code = 200;
      let roomstate = room_state[0] ? STATE_VALUE_TO_TXT(pms, room_state[0]) : "";

      if (err) code = 500;
      else if (!room_state[0]) code = 201;

      return RES_JSON(pms, res, code, null, {roomstate});
    });
  }
};

const checkin_room = (req, res) => {
  let {pms, room, start, end} = req.params;
  let {
    auth: {place_id},
  } = req;

  console.log("-> checkin_room", {pms, place_id, room, start, end});

  if (!pms || !room || !start || !end) {
    return RES_JSON(pms, res, 500, "체크인 파라메터가 올바르지 않습니다.", {pms, place_id, room, start, end});
  } else {
    let check_in_exp = moment(start, "YY:MM:DD:HH:mm").format("YYYY-MM-DD HH:mm");
    let check_out_exp = moment(end, "YY:MM:DD:HH:mm").format("YYYY-MM-DD HH:mm");

    console.log("-> check_in_exp", {check_in_exp, check_out_exp});

    RoomState.selectRoomStateByName(place_id, room, (err, result) => {
      if (!err && result[0]) {
        let {sale, room_id, key, clean, outing, air_set_temp, check_in, check_out} = result[0];

        if (room_id) {
          if (sale !== undefined && sale === 0) {
            const {
              headers: {token},
              auth: {place_id, id: user_id}, // token 에서 추출 한 정보.
            } = req;

            let room_sale = {channel: "pms", user_id};

            room_sale.room_id = room_id;
            room_sale.check_in = check_in_exp; // 입실 시간.
            room_sale.check_in_exp = check_in_exp;
            room_sale.check_out_exp = check_out_exp;
            room_sale.stay_type = 1; // 숙박만 가능.

            // 객실 기본 요금 은 PMS 프로그램과 요금 연동이 안되므로 0 로 설정.
            room_sale.default_fee = 0;
            room_sale.add_fee = 0;

            console.log("-> select room_sale", {room_id, room_sale});

            // 매출 중복 판매 여부 검사.
            RoomSale.selectIsRoomSaleNow(room_id, (err, row) => {
              let {count = 0} = row[0] || {};

              // 중복 시.
              if (count) {
                return RES_JSON(pms, res, 500, "중복 체크인 요청 입니다.");
              } else {
                RoomSale.insertRoomSale(room_sale, req, (err, info) => {
                  if (!err) {
                    let room_sale_id = info.insertId;

                    room_sale.id = room_sale_id;
                    room_sale.reg_date = new Date();

                    let {
                      user_id = "",
                      phone = "",
                      stay_type,
                      rollback = 0,
                      serialno,
                      default_fee = 0,
                      add_fee = 0,
                      pay_card_amt = 0,
                      pay_cash_amt = 0,
                      pay_point_amt = 0,
                      prepay_otamt = 0,
                      prepay_card_amt = 0,
                      prepay_cash_amt = 0,
                      prepay_point_amt = 0,
                      save_point = 0,
                      card_approval_num,
                      card_merchant,
                      card_no,
                    } = room_sale;

                    console.log("- room_sale", room_sale);

                    let room_sale_pay = {
                      channel: "pms",
                      first_pay: 1, // 매출의 첫 결재.
                      sale_id: room_sale_id,
                      user_id,
                      phone,
                      stay_type,
                      rollback,
                      serialno,
                      default_fee,
                      add_fee,
                      pay_card_amt,
                      pay_cash_amt,
                      pay_point_amt,
                      prepay_otamt,
                      prepay_card_amt,
                      prepay_cash_amt,
                      prepay_point_amt,
                      save_point,
                      card_approval_num,
                      card_merchant,
                      card_no,
                      fee_total: Number(default_fee) + Number(add_fee),
                    };

                    console.log("- room_sale_pay", room_sale_pay);

                    insert_room_sale_pay(room_sale_id, room_sale_pay);

                    const json = {
                      type: "SET_ROOM_SALE", // 웹 reducer type, 어드민/모바일 store 타입은 socket server 에서 각각 변경.
                      headers: {
                        method: "post",
                        url: "room/sale/" + room_sale_id,
                        token,
                        channel: "api",
                      },
                      body: {
                        room_sale,
                        place_id,
                      },
                    };

                    // 소켓 서버로 전송.
                    global.dispatcher.dispatch(json);

                    PREFERENCES.Get(place_id, (preferences) => {
                      // console.log("---> preferences ", preferences);

                      const {temp_key_1, temp_key_2, temp_key_3, temp_key_4, temp_key_5} = preferences;

                      let room_state = {channel: "pms"};

                      room_state.sale = 1; // 자동 입실 시 숙박 형태 1:숙박 2:대실 3:장기
                      room_state.check_in_exp = check_in_exp;
                      room_state.check_out_exp = check_out_exp;
                      room_state.room_sale_id = room_sale_id;

                      if (key !== 3 && clean) room_state.clean = 0;
                      if (outing) room_state.outing = 0;

                      if (key === 1) {
                        if (air_set_temp !== temp_key_1) room_state.air_set_temp = temp_key_1;
                        room_state.inspect = 0;
                      }

                      console.log("-> room_state", {room_id, room_state});

                      RoomState.updateRoomState(room_id, room_state, req, pms, (err, info) => {
                        if (!err) {
                          room_state.room_id = room_id;
                          delete room_state.mod_date;

                          const json = {
                            type: "SET_ROOM_STATE",
                            headers: {
                              method: "put",
                              url: "room/state/" + room_id,
                              token,
                              channel: "api",
                            },
                            body: {
                              room_state,
                              place_id,
                            },
                          };

                          // 소켓 서버로 전송.
                          global.dispatcher.dispatch(json);
                        }

                        return RES_JSON(pms, res, err ? 500 : 200, err ? JSON.stringify(err) : "");
                      });
                    });
                  } else {
                    return RES_JSON(pms, res, 500, JSON.stringify(err));
                  }
                });
              }
            });
          } else {
            return RES_JSON(pms, res, 400, `${room} 객실은 현재 체크인 상태 입니다. 체크아웃을 먼저 해주세요.`);
          }
        } else {
          return RES_JSON(pms, res, 400, `${room} 객실 상태를 찻을 수 없습니다`);
        }
      } else {
        return RES_JSON(pms, res, 500, JSON.stringify(err));
      }
    });
  }
};

const checkout_room = (req, res) => {
  let {pms, room} = req.params;

  console.log("-> checkout_room", {pms, room});

  if (!pms || !room) {
    return RES_JSON(pms, res, 500, "체크아웃 요청 파라메터가 올바르지 않습니다");
  } else {
    return checkout_exe(req, res, pms, room, true);
  }
};

const checkout_exe = (req, res, pms, room, send) => {
  let {
    auth: {place_id},
  } = req;

  let check_out = moment().format("YYYY-MM-DD HH:mm");

  console.log("-> checkout_exe", {pms, place_id, room});

  RoomState.selectRoomStateByName(place_id, room, (err, result) => {
    if (!err && result[0]) {
      let {sale, room_id, room_sale_id, clean, outing, air_set_temp, main_relay, check_in, check_out, sale_pay_id} = result[0];

      if (sale) {
        const {
          headers: {token},
          auth: {place_id, id: user_id}, // token 에서 추출 한 정보.
        } = req;

        let room_sale = {channel: "pms", user_id};

        room_sale.room_id = room_id;
        room_sale.state = "C"; // 판매 상태 종료.
        room_sale.check_out = new Date(); // 체크 아웃.

        console.log("-> room_sale", {room_id, room_sale, room_sale_id});

        if (!room_sale_id) {
          if (send) return RES_JSON(pms, res, 200);
        }

        RoomSale.updateRoomSale(room_sale_id, room_sale, req, (err, info) => {
          if (!err) {
            room_sale.id = room_sale_id;
            room_sale.reg_date = new Date();

            const json = {
              type: "SET_ROOM_SALE", // 웹 reducer type, 어드민/모바일 store 타입은 socket server 에서 각각 변경.
              headers: {
                method: "put",
                url: "room/sale/" + room_sale_id,
                token,
                channel: "api",
              },
              body: {
                room_sale,
                place_id,
              },
            };

            // 소켓 서버로 전송.
            global.dispatcher.dispatch(json);

            // 매출 로그 업데이트
            let room_sale_pay = {
              state: "C",
              channel: "pms",
              rollback: 0,
            };

            insert_room_sale_pay(room_sale_id, room_sale_pay);

            PREFERENCES.Get(place_id, (preferences) => {
              // console.log("---> preferences ", preferences);

              const {temp_key_1, temp_key_2, temp_key_3, temp_key_4, temp_key_5} = preferences;

              // 객실 상태 공실 처리.
              let room_state = {channel: "pms"};

              if (sale) room_state.sale = 0;
              if (!clean) room_state.clean = 1; // 청소 요청.
              if (outing) room_state.outing = 0; // 외출 초기화.
              if (air_set_temp !== temp_key_5) room_state.air_set_temp = temp_key_5; // 청소대기 온도
              if (main_relay) room_state.main_relay = 0; // 메인 릴레이 0:OFF, 1:ON (전등/전열)

              console.log("-> room_state", {room_id, room_state});

              RoomState.updateRoomState(room_id, room_state, req, pms, (err, info) => {
                if (!err) {
                  room_state.room_id = room_id;
                  delete room_state.mod_date;

                  const json = {
                    type: "SET_ROOM_STATE",
                    headers: {
                      method: "put",
                      url: "room/state/" + room_id,
                      token,
                      channel: "api",
                    },
                    body: {
                      room_state,
                      place_id,
                    },
                  };

                  // 소켓 서버로 전송.
                  global.dispatcher.dispatch(json);
                }
                if (send) return RES_JSON(pms, res, err ? 500 : 200, err ? JSON.stringify(err) : "");
              });
            });
          } else {
            if (send) return RES_JSON(pms, res, 500, JSON.stringify(err));
          }
        });
      } else {
        return RES_JSON(pms, res, 400, `${room} 객실은 현재 공실 상태 입니다.`);
      }
    } else {
      if (send) return RES_JSON(pms, res, 500, JSON.stringify(err));
    }
  });
};

// 객실 초기화 (마감 시점 이후에 PMS 시스템에서 재실 중인 객실에 대해서 청소 지시 요청을 객실관리 서버로 호출한다. 하루 숙박이후 해당 객실은 다음날)
// 객실번호,객실상태,객실정비상태/ …
// 0101,E,G /0102,T,G/0103,,I/0104,E,G/ …
const init_rooms = (req, res) => {
  let {pms} = req.params;
  let {data} = req.body;

  console.log("-> init_rooms", {data});

  if (!data) {
    return RES_JSON(pms, res, 400);
  } else {
    const {
      headers: {token},
      auth: {place_id, id: user_id}, // token 에서 추출 한 정보.
    } = req;

    // 객실 상태목록
    const list = data.split(/\//);

    console.log("-> init_rooms ", {list});

    if (!list.length) return RES_JSON(pms, res, 200);

    // 모든 객실 상태 조회.
    RoomState.selectAllRoomStatesSale(place_id, (err, result) => {
      if (!err) {
        let rooms = _.map(result, (v) => {
          if (v.name) v.name = v.name.replace(/[^\d]/g, "").padStart(5, "0"); // 5 자리로 맞춘다
          // console.log("---> room name ", v.name);
          return v;
        });

        console.log("---> rooms ", rooms.length);

        PREFERENCES.Get(place_id, (preferences) => {
          // console.log("---> preferences ", preferences);

          // 사용중, 외출중, 공실, 청소중, 청소대기
          const {temp_key_1, temp_key_2, temp_key_3, temp_key_4, temp_key_5} = preferences;

          let isErr = false;
          let msg = "";

          _.each(list, (v, k) => {
            let states = v.split(/,/g);

            if (states.length > 2) {
              let name = states[0].padStart(5, "0"); // 객실번호 (5 자리로 맞춘다)
              let code1 = states[1]; // 객실상태
              let code2 = states[2]; // 객실정비상태

              console.log(k, "-> parse code ", {name, code1, code2});

              let {room_id, stay_type, sale, clean, outing, air_set_temp, room_sale_id, rollback, check_in, check_out, sale_pay_id} = _.find(rooms, {name}) || {};

              console.log(k, "-> find room", {name, room_id, sale, room_sale_id, rollback, check_in, check_out});

              if (room_id) {
                let STATE = STATE_CODE_TO_VLUE(pms, code1);
                let CLEAN = STATE_CODE_TO_VLUE(pms, code2);

                console.log("-> STATE_CODE_TO_VLUE", {STATE, CLEAN});

                // 객실 상태 (로그 정보를 위한 추가 정보)
                let room_state = {room_id, channel: "pms", stay_type, user_id, room_sale_id};

                // 공실
                if (STATE === 0) {
                  if (clean) room_state.clean = 0;
                  if (outing) room_state.outing = 0;
                  if (air_set_temp !== temp_key_3) room_state.air_set_temp = temp_key_3;

                  // 현재 체크인 이라면 체크아웃.
                  if (sale) checkout_exe(req, res, pms, name);
                }
                // 재실
                else if (STATE === 1) {
                  if (outing) room_state.outing = 0;
                  if (air_set_temp !== temp_key_1) room_state.air_set_temp = temp_key_1;
                }
                // 외출
                else if (STATE === 2) {
                  if (!outing) room_state.outing = 1;
                  if (air_set_temp !== temp_key_2) room_state.air_set_temp = temp_key_2;
                }
                // 입실 취소
                else if (STATE === 6) {
                  if (sale) room_state.sale = 0;
                  if (!clean) room_state.clean = 1;
                  if (outing) room_state.outing = 0;
                  if (air_set_temp !== temp_key_5) room_state.air_set_temp = temp_key_5;

                  room_state.room_sale_id = null;

                  // 로그 정보를 위한 추가 정보
                  room_state.state = "B";
                  room_state.rollback = 1;
                  room_state.comment = "PMS 입실취소";

                  // 입실 상태
                  if (room_sale_id) {
                    let room_sale = {channel: "pms", user_id};

                    room_sale.room_id = room_id;
                    room_sale.state = "B"; // 판매 취소.

                    console.log("-> 입실취소 room_sale", {room_id, room_sale, room_sale_id});

                    RoomSale.updateRoomSale(room_sale_id, room_sale, req, (err, info) => {
                      if (!err) {
                        room_sale.id = room_sale_id;
                        room_sale.reg_date = new Date();

                        const json = {
                          type: "SET_ROOM_SALE", // 웹 reducer type, 어드민/모바일 store 타입은 socket server 에서 각각 변경.
                          headers: {
                            method: "put",
                            url: "room/sale/" + room_sale_id,
                            token,
                            channel: "api",
                          },
                          body: {
                            room_sale,
                            place_id,
                          },
                        };

                        // 소켓 서버로 전송.
                        global.dispatcher.dispatch(json);

                        // 매출 로그 업데이트
                        let room_sale_pay = {
                          state: "B",
                          channel: "pms",
                          rollback: 1,
                        };

                        insert_room_sale_pay(room_sale_id, room_sale_pay);
                      }
                    });
                  }
                }
                // 퇴실 취소
                else if (STATE === 7) {
                  if (!sale) room_state.sale = stay_type;
                  if (clean) room_state.clean = 0;
                  if (air_set_temp !== temp_key_1) room_state.air_set_temp = temp_key_1;

                  // 로그 정보를 위한 추가 정보
                  room_state.state = "A";
                  room_state.rollback = 2;

                  // 입실 상태
                  if (room_sale_id) {
                    let room_sale = {channel: "pms", user_id};

                    room_sale.room_id = room_id;
                    room_sale.state = "A"; // 판매 상태.
                    room_sale.check_out = null; // 체크아웃 취소.

                    console.log("-> 퇴실취소 room_sale", {room_id, room_sale, room_sale_id});

                    RoomSale.updateRoomSale(room_sale_id, room_sale, req, (err, info) => {
                      if (!err) {
                        room_sale.id = room_sale_id;
                        room_sale.reg_date = new Date();

                        const json = {
                          type: "SET_ROOM_SALE", // 웹 reducer type, 어드민/모바일 store 타입은 socket server 에서 각각 변경.
                          headers: {
                            method: "put",
                            url: "room/sale/" + room_sale_id,
                            token,
                            channel: "api",
                          },
                          body: {
                            room_sale,
                            place_id,
                          },
                        };

                        // 소켓 서버로 전송.
                        global.dispatcher.dispatch(json);

                        // 매출 로그 업데이트
                        let room_sale_pay = {
                          state: "A",
                          channel: "pms",
                          rollback: 2,
                        };

                        insert_room_sale_pay(room_sale_id, room_sale_pay);
                      }
                    });
                  }
                }

                // 청소지시, 청소대기
                if (CLEAN === 3) {
                  if (!clean) room_state.clean = 1;
                  if (air_set_temp !== temp_key_5) room_state.air_set_temp = temp_key_5;
                }
                // 청소중
                else if (CLEAN === 4) {
                  if (!clean) room_state.clean = 1;
                  if (air_set_temp !== temp_key_4) room_state.air_set_temp = temp_key_4;
                }
                // 청소완료
                else if (CLEAN === 5) {
                  if (clean) room_state.clean = 0;
                }

                console.log("-> room_state", {room_id, room_state});

                RoomState.updateRoomState(room_id, room_state, req, pms, (err, info) => {
                  if (!err) {
                    room_state.room_id = room_id;
                    delete room_state.mod_date;

                    const json = {
                      type: "SET_ROOM_STATE",
                      headers: {
                        method: "put",
                        url: "room/state/" + room_id,
                        token,
                        channel: "api",
                      },
                      body: {
                        room_state,
                        place_id,
                      },
                    };

                    // 소켓 서버로 전송.
                    global.dispatcher.dispatch(json);
                  } else {
                    isErr = true;
                  }
                });
              } else {
                isErr = true;
                msg += `${name} 객실명을 찿을 수 없습니다. \n`;
              }
            }
          });
          return RES_JSON(pms, res, isErr ? 500 : 200, msg);
        });
      } else {
        return RES_JSON(pms, res, 500, JSON.stringify(err));
      }
    });
  }
};

const insert_room_sale_pay = (room_sale_id, room_sale) => {
  console.log("- insert_room_sale_pay", {room_sale_id, room_sale});

  // 매출 조회
  RoomSale.selectRoomSale(room_sale_id, (err, result) => {
    if (!err) {
      let row = result[0] || {};

      // 매출 상태에서만 결제 정보 추가.
      if (row.stay_type) {
        let {
          state,
          channel,
          room_id,
          user_id,
          phone,
          stay_type,
          rollback,
          default_fee,
          add_fee,
          pay_card_amt,
          pay_cash_amt,
          pay_point_amt,
          prepay_ota_amt,
          prepay_card_amt,
          prepay_cash_amt,
          prepay_point_amt,
          save_point,
          card_approval_num,
          card_merchant,
          card_no,
          move_from,
          prev_stay_type,
        } = room_sale;

        // 수정 유저 추가
        user_id = user_id || row.user_id;
        stay_type = stay_type || row.stay_type;

        // 입실취소
        if (rollback === 1) {
          default_fee = -row.default_fee || 0;
          add_fee = -row.add_fee || 0;
          pay_card_amt = -row.pay_card_amt || 0;
          pay_cash_amt = -row.pay_cash_amt || 0;
          pay_point_amt = -row.pay_point_amt || 0;
          prepay_ota_amt = -row.prepay_ota_amt || 0;
          prepay_card_amt = -row.prepay_card_amt || 0;
          prepay_cash_amt = -row.prepay_cash_amt || 0;
          prepay_point_amt = -row.prepay_point_amt || 0;
          save_point = -row.save_point || 0;
        } else {
          default_fee = default_fee - row.default_fee || 0;
          add_fee = add_fee - row.add_fee || 0;
          pay_card_amt = pay_card_amt - row.pay_card_amt || 0;
          pay_cash_amt = pay_cash_amt - row.pay_cash_amt || 0;
          pay_point_amt = pay_point_amt - row.pay_point_amt || 0;
          prepay_ota_amt = prepay_ota_amt - row.prepay_ota_amt || 0;
          prepay_card_amt = prepay_card_amt - row.prepay_card_amt || 0;
          prepay_cash_amt = prepay_cash_amt - row.prepay_cash_amt || 0;
          prepay_point_amt = prepay_point_amt - row.prepay_point_amt || 0;
          save_point = save_point - row.save_point || 0;
        }

        let fee_total = row.default_fee + row.add_fee + default_fee + add_fee || 0;

        // 금액 변경 여부 체크.
        let new_room_sale_pay = {
          state,
          channel,
          first_pay: 0,
          sale_id: room_sale_id,
          user_id,
          phone,
          stay_type,
          rollback,
          default_fee,
          add_fee,
          pay_card_amt,
          pay_cash_amt,
          pay_point_amt,
          prepay_ota_amt,
          prepay_card_amt,
          prepay_cash_amt,
          prepay_point_amt,
          save_point,
          card_approval_num,
          card_merchant,
          card_no,
          fee_total,
          move_from,
          prev_stay_type,
        };

        console.log("- new_room_sale_pay", new_room_sale_pay);

        RoomSalePay.insertRoomSalePay(new_room_sale_pay, (err, info) => {
          console.log("- RoomSalePay successfully inserted");
        });
      }
    }
  });
};

export default {
  create_token,
  read_all_room,
  read_room,
  checkin_room,
  checkout_room,
  init_rooms,
};
