import As from "model/as";
import File from "model/file";
import Mail from "model/mail";
import _ from "lodash";

import { jsonRes, sendRes, isValidToken, checkPermission } from "utils/api-util";
import { ERROR, PREFERENCES } from "constants/constants";

const list_a_ases = (req, res) => {
  let {
    filter = "1=1",
    between = "a.reg_date",
    begin = moment().add(-1, "month").format("YYYY-MM-DD 00:00"),
    end = moment().format("YYYY-MM-DD HH:mm"),
    limit = "10000",
    order = "a.reg_date",
    desc = "desc",
  } = req.params;

  As.selectAsCount(filter, between, begin, end, (err, count) => {
    if (err) {
      jsonRes(req, res, err, { count });
    } else
      As.selectAses(filter, between, begin, end, order, desc, limit, (err, ases) => {
        jsonRes(req, res, err, { count, ases });
      });
  });
};

const read_a_as = (req, res) => {
  const { id } = req.params;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide as id for select");
  } else {
    As.selectAs(id, (err, as) => {
      jsonRes(req, res, err, { as });
    });
  }
};

const create_a_as = (req, res) => {
  const { as } = req.body;

  As.insertAs(as, (err, info) => {
    if (info && info.insertId) req.body.as.id = info.insertId; // 채널 전송 시 id 입력.

    jsonRes(req, res, err, { info }, "As successfully inserted");

    if (!err) {
      As.selectAs(info.insertId, (err, as) => {
        if (!err && as[0]) {
          let { title, content, place_name } = as[0];

          // 메일 타입(01: AS 메일, 02: 구독 메일,  03: OTA예약 연동 실패 메일, 09: 오류 메일)
          PREFERENCES.MailReceiverList("01", (list) => {
            _.each(list, (v) => {
              if (v.email && v.receive_yn === "Y") {
                let mail = {
                  type: 1, // 메일 타입 (0: 고객메일, 1: 관리자메일, 9: 서버메일)
                  to: v.email,
                  from: "as@icrew.kr",
                  subject: `[아이크루 관리자] ${process.env.REACT_APP_MODE !== "PROD" ? "[" + process.env.REACT_APP_MODE + "]" : ""} [${place_name}] AS 요청 입니다.`,
                  content: `<strong>${title}</strong><br/></br> ${content}<br/><br/><br/><br/>주식회사 아이크루컴퍼니<br/>1600-5356`,
                };
                Mail.insertMail(mail, (err, info) => {});
              }
            });
          });
        }
      });
    }
  });
};

const update_a_as = (req, res) => {
  const { id } = req.params;
  const { as } = req.body;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide as id for update");
  } else {
    As.updateAs(id, as, (err, info) => {
      jsonRes(req, res, err, { info }, "As successfully updated");

      if (!err) {
        As.selectAs(id, (err, as) => {
          if (!err && as[0]) {
            let { title, content, place_name } = as[0];

            // 메일 타입(01: AS 메일, 02: 구독 메일,  03: OTA예약 연동 실패 메일, 09: 오류 메일)
            PREFERENCES.MailReceiverList("01", (list) => {
              _.each(list, (v) => {
                if (v.email && v.receive_yn === "Y") {
                  let mail = {
                    type: 1, // 메일 타입 (0: 고객메일, 1: 관리자메일, 9: 서버메일)
                    to: v.email,
                    from: "as@icrew.kr",
                    subject: `[아이크루 관리자] ${process.env.REACT_APP_MODE !== "PROD" ? "[" + process.env.REACT_APP_MODE + "]" : ""} [${place_name}] AS 수정 입니다.`,
                    content: `<strong>${title}</strong><br/></br> ${content}<br/><br/><br/><br/>주식회사 아이크루컴퍼니<br/>1600-5356`,
                  };
                  Mail.insertMail(mail, (err, info) => {});
                }
              });
            });
          }
        });
      }
    });
  }
};

const delete_a_as = (req, res) => {
  const { id } = req.params;
  const { as } = req.body;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide as id for delete");
  } else {
    let selAs = null;

    As.selectAs(id, (err, as) => {
      if (!err && as[0]) {
        selAs = as[0];
      }
    });

    As.deleteAs(id, (err, info) => {
      jsonRes(req, res, err, { info }, "As successfully deleted");

      if (!err && selAs) {
        // 메일 타입(01: AS 메일, 02: 구독 메일,  03: OTA예약 연동 실패 메일, 09: 오류 메일)
        PREFERENCES.MailReceiverList("01", (list) => {
          let { title, content, place_name } = selAs;

          _.each(list, (v) => {
            if (v.email && v.receive_yn === "Y") {
              let mail = {
                type: 1, // 메일 타입 (0: 고객메일, 1: 관리자메일, 9: 서버메일)
                to: v.email,
                from: "as@icrew.kr",
                subject: `[아이크루 관리자] ${process.env.REACT_APP_MODE !== "PROD" ? "[" + process.env.REACT_APP_MODE + "]" : ""} [${place_name}] AS 삭제 입니다.`,
                content: `<strong>${title}</strong><br/></br> ${content}<br/><br/><br/><br/>주식회사 아이크루컴퍼니<br/>1600-5356`,
              };

              Mail.insertMail(mail, (err, info) => {});
            }
          });
        });
      }
    });
  }
};

export default {
  list_a_ases,
  create_a_as,
  read_a_as,
  update_a_as,
  delete_a_as,
};
