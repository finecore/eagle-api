import RoomFee from "model/room-fee";
import { jsonRes, sendRes, isValidToken, checkPermission } from "utils/api-util";
import { ERROR } from "constants/constants";
import moment from "moment";

const list_all_room_fee = (req, res) => {
  const { place_id, channel } = req.params;

  if (!place_id && !channel) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide place_id or channel for get room fee list");
  } else {
    RoomFee.selectAllRoomFees(place_id, channel, (err, all_room_fees) => {
      jsonRes(req, res, err, { all_room_fees });
    });
  }
};

const list_room_type_fee = (req, res) => {
  const { room_type_id, channel } = req.params;

  if (!room_type_id || !channel) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room fees room_type_id and channel for select");
  } else {
    RoomFee.selectRoomFees(room_type_id, channel, (err, room_fees) => {
      jsonRes(req, res, err, { room_fees });
    });
  }
};

const read_a_room_fee = (req, res) => {
  if (!req.params.id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room fee id for select");
  } else {
    RoomFee.selectRoomFee(req.params.id, (err, room_fee) => {
      jsonRes(req, res, err, { room_fee });
    });
  }
};

const create_a_room_fee = (req, res) => {
  var new_room_fee = req.body.room_fee; // post body.

  if (!new_room_fee.room_type_id || !new_room_fee.day || !new_room_fee.add_fee) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room fee data for insert");
  } else {
    // 요금 복제 시 기존 정보 삭제.
    delete new_room_fee.id;
    delete new_room_fee.reg_date;
    delete new_room_fee.mod_date;

    RoomFee.insertRoomFee(new_room_fee, (err, info) => {
      if (info && info.insertId) req.body.room_fee.id = info.insertId; // 채널 전송 시 id 입력.

      jsonRes(req, res, err, { info }, "RoomFee successfully inserted");
    });
  }
};

const update_a_room_fee = (req, res) => {
  const { id } = req.params;
  let { room_fee } = req.body; // post body.

  if (!id || !room_fee) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room fee data for update");
  } else {
    RoomFee.updateRoomFee(id, room_fee, (err, info) => {
      jsonRes(req, res, err, { info }, "RoomFee successfully updated");
    });
  }
};

const create_list_room_fee = (req, res) => {
  var room_fees = req.body.room_fees; // post body.

  let errors = "";
  let new_room_fees = [];

  room_fees.map((room_fee, idx) => {
    if (!room_fee.room_type_id) {
      jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room fee data for insert");
      return false;
    } else {
      // 요금 복제 시 기존 정보 삭제.
      delete room_fee.id;
      delete room_fee.reg_date;
      delete room_fee.mod_date;

      RoomFee.insertRoomFee(room_fee, (err, info) => {
        if (err) errors += err;
        else {
          room_fee.id = info.insertId;
          new_room_fees.push(room_fee);
        }

        if (idx === room_fees.length - 1) {
          jsonRes(req, res, errors, { info: {}, new_room_fees }, errors ? "RoomFee failue inserted" : "RoomFee successfully inserted");
        }
      });
    }
  });
};

const update_list_room_fee = (req, res) => {
  let { room_fees } = req.body; // post body.

  let errors = "";
  let mod_room_fees = [];

  room_fees.map((room_fee, idx) => {
    if (!room_fee.id) {
      jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room fee id for update");
      return false;
    } else {
      RoomFee.updateRoomFee(room_fee.id, room_fee, (err, info) => {
        if (err) errors += err;
        else mod_room_fees.push(room_fee);

        if (idx === room_fees.length - 1) {
          jsonRes(req, res, errors, { info: {}, mod_room_fees }, errors ? "RoomFee failue updated" : "RoomFee successfully updated");
        }
      });
    }
  });
};

const delete_list_room_fee = (req, res) => {
  var room_fees = req.body.room_fees; // post body.

  let errors = "";
  let del_room_fees = [];

  console.log("- delete_list_room_fee", room_fees);

  room_fees.map((room_fee, idx) => {
    if (room_fee.id) {
      RoomFee.deleteRoomFee(room_fee.id, (err, info) => {
        if (err) errors += err;
        else del_room_fees.push(room_fee);
      });
    }

    console.log("- del_room_fees", del_room_fees);

    if (idx === room_fees.length - 1) {
      return jsonRes(req, res, errors, { info: {}, del_room_fees }, errors ? "RoomFee failue deleted" : "RoomFee successfully deleted");
    }
  });
};

const delete_a_room_fee = (req, res) => {
  if (!req.params.id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room fee id for delete");
  } else {
    RoomFee.deleteRoomFee(req.params.id, (err, info) => {
      jsonRes(req, res, err, { info }, "RoomFee successfully deleted");
    });
  }
};

const delete_room_type_fee = (req, res) => {
  const { room_type_id, channel } = req.params;

  if (!room_type_id || !channel) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room_type_id and channel for delete");
  } else {
    RoomFee.deleteRoomFees(room_type_id, channel, (err, info) => {
      jsonRes(req, res, err, { info }, "RoomFee successfully room type fee deleted");
    });
  }
};

const delete_all_room_fee = (req, res) => {
  if (!req.params.place_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide place_id for delete");
  } else {
    RoomFee.deleteRoomFees(req.params.room_id, (err, info) => {
      jsonRes(req, res, err, { info }, "RoomFee successfully all deleted");
    });
  }
};

export default {
  list_all_room_fee,
  list_room_type_fee,
  read_a_room_fee,
  create_a_room_fee,
  update_a_room_fee,
  create_list_room_fee,
  update_list_room_fee,
  delete_list_room_fee,
  delete_a_room_fee,
  delete_room_type_fee,
  delete_all_room_fee,
};
