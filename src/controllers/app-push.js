import AppPush from "model/app-push";

import { jsonRes, sendRes, isValidToken, checkPermission } from "utils/api-util";
import { ERROR } from "constants/constants";

import _ from "lodash";

const list_a_app_pushs = (req, res) => {
  let { place_id, filter = "1=1", limit = "10000", order = "a.id", desc = "desc" } = req.params;

  if (place_id) filter = `c.place_id=${place_id}`;

  AppPush.selectAppPushCount(filter, (err, count) => {
    if (err) {
      jsonRes(req, res, err, { count });
    } else
      AppPush.selectAppPushs(filter, order, desc, limit, (err, app_pushs) => {
        jsonRes(req, res, err, { count, app_pushs });
      });
  });
};

const list_token_app_pushs = (req, res) => {
  let { token } = req.params;

  if (!token) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide list_token_app_pushs token for select");
  } else {
    AppPush.selectTokenAppPushs(token, (err, app_pushs) => {
      jsonRes(req, res, err, { app_pushs });
    });
  }
};

const read_a_app_push = (req, res) => {
  const { id } = req.params;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide app_push token for select");
  } else {
    AppPush.selectAppPush(id, (err, app_push) => {
      jsonRes(req, res, err, { app_push });
    });
  }
};

const create_a_app_push = (req, res) => {
  let { app_push } = req.body; // post body.

  if (!app_push || !app_push.token) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide app_push token  for insert");
  } else {
    AppPush.insertAppPush(app_push, (err, info) => {
      app_push.token = token;
      app_push.inserted = true;
      jsonRes(req, res, err, { info, app_push }, "AppPush successfully inserted");
    });
  }
};

const update_a_app_push = (req, res) => {
  const { id } = req.params;
  let { app_push } = req.body;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide app_push id for update");
  } else {
    AppPush.updateAppPush(id, app_push, (err, info) => {
      app_push.id = id;
      app_push.updated = true;
      jsonRes(req, res, err, { info, app_push }, "AppPush successfully updated");
    });
  }
};

const update_token_app_push = (req, res) => {
  const { token } = req.params;
  let { app_push } = req.body;

  if (!token) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide app_push token for update");
  } else {
    AppPush.updateTokenAppPush(token, app_push, (err, info) => {
      app_push.token = token;
      app_push.updated = true;
      jsonRes(req, res, err, { info, app_push }, "AppPush successfully updated");
    });
  }
};

const delete_a_app_push = (req, res) => {
  const { id } = req.params;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide app_push id for delete");
  } else {
    AppPush.deleteAppPush(id, (err, info) => {
      let app_push = { id, deleted: true };
      jsonRes(req, res, err, { info, app_push }, "AppPush successfully deleted");
    });
  }
};

const delete_token_app_push = (req, res) => {
  const { token } = req.params;

  if (!token) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide app_push token for delete");
  } else {
    AppPush.deleteTokenAppPush(token, (err, info) => {
      let app_push = { token, deleted: true };
      jsonRes(req, res, err, { info, app_push }, "AppPush successfully deleted");
    });
  }
};

// 앱 푸시는 channel 에서 요청 하는게 아니라 서버에서 필요에 따라 발송 하므로 클라이언트에 등록/수정/삭제 정보를 전송 해 준다.
// (jsonRes 에서 일괄 전송)
const send_socket = (req, app_push) => {
  console.log("---> send_socket SET_APP_PUSH", app_push);

  let {
    url,
    headers: { channel, "x-access-token": xtoken, token, uuid },
    auth: { place_id }, // token 에서 추출 한 정보.
  } = req;

  // 토큰 생성.
  if (place_id) {
    const json = {
      type: "SET_APP_PUSH", // 웹 reducer type, 어드민/모바일 store 타입은 socket server 에서 각각 변경.
      headers: {
        method,
        url,
        token: token || xtoken,
        channel,
        req_channel: channel,
        uuid,
        place_id,
      },
      body: {
        app_push,
      },
    };

    // 소켓 서버로 전송.
    global.dispatcher.dispatch(json);
  }
};

export default {
  list_a_app_pushs,
  list_token_app_pushs,
  read_a_app_push,
  create_a_app_push,
  update_a_app_push,
  update_token_app_push,
  delete_a_app_push,
  delete_token_app_push,
  send_socket,
};
