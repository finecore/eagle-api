import Preferences from "model/preferences";
import { jsonRes, sendRes, isValidToken, checkPermission } from "utils/api-util";
import { ERROR, PREFERENCES } from "constants/constants";
import moment from "moment";

const list_all_preferences = (req, res) => {
  Preferences.selectAllPreferences((err, all_preferences) => {
    jsonRes(req, res, err, { all_preferences });
  });
};

const read_a_preferences = (req, res) => {
  if (!req.params.place_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide preferences id for select");
  } else {
    Preferences.selectPreferences(req.params.place_id, (err, preferences) => {
      jsonRes(req, res, err, { preferences });
    });
  }
};

const create_a_preferences = (req, res) => {
  var new_preferences = req.body.preferences; // post body.

  if (!new_preferences.place_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide preferences place id for insert");
  } else {
    new_preferences.reg_date = new Date();

    Preferences.insertPreferences(new_preferences, (err, info) => {
      if (info && info.insertId) req.body.preferences.id = info.insertId; // 채널 전송 시 id 입력.

      jsonRes(req, res, err, { info }, "Preferences successfully inserted");

      // 환경 설정값 캐싱.
      PREFERENCES.Set(new_preferences.place_id, new_preferences);
    });
  }
};

const update_a_preferences = (req, res) => {
  var preferences = req.body.preferences; // post body.

  // console.log("- update_a_preferences", preferences);

  if (!req.params.id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide preferences id for update");
  } else {
    Preferences.updatePreferences(req.params.id, preferences, (err, info) => {
      jsonRes(req, res, err, { info }, "Preferences successfully updated");

      // 환경 설정값 캐싱.
      if (!err) PREFERENCES.Set(preferences.place_id, preferences);
    });
  }
};

const delete_a_preferences = (req, res) => {
  if (!req.params.id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide preferences id for delete");
  } else {
    Preferences.deletePreferences(req.params.id, (err, info) => {
      jsonRes(req, res, err, { info }, "Preferences successfully deleted");
    });
  }
};

export default {
  list_all_preferences,
  create_a_preferences,
  read_a_preferences,
  update_a_preferences,
  delete_a_preferences,
};
