import Member from "model/member";
import { jsonRes, sendRes, isValidToken, checkPermission } from "utils/api-util";
import { ERROR } from "constants/constants";

const list_a_members = (req, res) => {
  let { filter = "1=1", limit = "10000", order = "a.id", desc = "desc" } = req.params;

  Member.selectMemberCount(filter, (err, count) => {
    if (err) {
      jsonRes(req, res, err, { count });
    } else
      Member.selectMembers(filter, order, desc, limit, (err, members) => {
        jsonRes(req, res, err, { count, members });
      });
  });
};

const list_place_members = (req, res) => {
  const { place_id } = req.params;

  if (!place_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide place_id for get member list");
  } else {
    Member.selectPlaceMembers(place_id, (err, members) => {
      jsonRes(req, res, err, { members });
    });
  }
};

const read_a_member = (req, res) => {
  const { id, phone } = req.params;

  if (!id && !phone) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide member id or phone for select");
  } else {
    Member.selectMember(id, phone, (err, member) => {
      jsonRes(req, res, err, { member });
    });
  }
};

const read_place_phone_member = (req, res) => {
  const { place_id, phone } = req.params;

  if (!place_id || !phone) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide member place_id and phone for select");
  } else {
    Member.selectPlaceMemberByPhone(place_id, phone, (err, member) => {
      jsonRes(req, res, err, { member });
    });
  }
};

const create_a_member = (req, res) => {
  let { member } = req.body;

  if (!member.place_id || !member.phone) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide member data for insert");
  } else {
    Member.insertMember(member, (err, info) => {
      if (info && info.insertId) req.body.member.id = info.insertId; // 채널 전송 시 id 입력.

      jsonRes(req, res, err, { info }, "Member successfully inserted");
    });
  }
};

const update_a_member = (req, res) => {
  const { id } = req.params;
  let { member } = req.body;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide member id for update");
  } else {
    Member.updateMember(id, member, (err, info) => {
      jsonRes(req, res, err, { info }, "Member successfully updated");
    });
  }
};

const update_place_member_phone = (req, res) => {
  const { place_id, phone } = req.params;
  let { member } = req.body;

  if (!place_id || !phone) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide member place_id and phone for update");
  } else {
    Member.updatePlaceMemberByPhone(place_id, phone, member, (err, info) => {
      jsonRes(req, res, err, { info }, "Member successfully updated");
    });
  }
};

const delete_a_member = (req, res) => {
  const { id } = req.params;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide member id for delete");
  } else {
    Member.deleteMember(id, (err, info) => {
      jsonRes(req, res, err, { info }, "Member successfully deleted");
    });
  }
};

export default {
  list_a_members,
  list_place_members,
  create_a_member,
  read_a_member,
  read_place_phone_member,
  update_a_member,
  update_place_member_phone,
  delete_a_member,
};
