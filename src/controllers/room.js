import Room from "model/room";
import RoomState from "model/room-state";
import RoomDoorLock from "model/room-doorlock";
import { jsonRes, sendRes, isValidToken, checkPermission } from "utils/api-util";
import { ERROR } from "constants/constants";
import moment from "moment";

const list_all_rooms = (req, res) => {
  Room.selectAllRooms((err, rooms) => {
    jsonRes(req, res, err, { rooms });
  });
};

const list_place_rooms = (req, res) => {
  const { place_id } = req.params;

  if (!place_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide place_id for get room list");
  } else {
    Room.selectPlaceRooms(place_id, (err, rooms) => {
      jsonRes(req, res, err, { rooms });
    });
  }
};

const list_a_rooms = (req, res) => {
  let { filter = "1=1", limit = "10000", order = "a.name", desc = "asc" } = req.params;

  Room.selectRoomCount(filter, (err, count) => {
    if (err) {
      jsonRes(req, res, err, { count });
    } else
      Room.selectRooms(filter, order, desc, limit, (err, rooms) => {
        jsonRes(req, res, err, { count, rooms });
      });
  });
};

// 예약 가능 객실 목록.
const list_a_enable_reserv_rooms = (req, res) => {
  let { place_id, room_type_id, filter = "1=1", order = "a.room_type_id, a.name" } = req.params;

  if (room_type_id) filter = "a.room_type_id = " + room_type_id;

  if (!place_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room reserv place_id for select");
  } else {
    Room.selectReservEnableRooms(place_id, filter, order, (err, rooms) => {
      jsonRes(req, res, err, { rooms });
    });
  }
};

const read_a_room = (req, res) => {
  const { id } = req.params;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room id for select");
  } else {
    Room.selectRoom(id, (err, room) => {
      jsonRes(req, res, err, { room });

      if (!err && id !== "all") {
        const new_room_state = {
          room_id: id,
        };
        RoomState.insertRoomState(new_room_state, (err, info) => {});
      }
    });
  }
};

const read_a_room_by_name = (req, res) => {
  const { place_id, name } = req.params;

  if (!place_id || !name) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room place_id or name for select");
  } else {
    Room.selectRoomByName(place_id, name, (err, room) => {
      jsonRes(req, res, err, { room });
    });
  }
};

const create_a_room = (req, res) => {
  var new_room = req.body.room; // post body.

  console.log("- create_a_room", new_room);

  if (!new_room.place_id || !new_room.name) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room data for insert");
  } else {
    Room.insertRoom(new_room, (err, info) => {
      if (info && info.insertId) req.body.room.id = info.insertId; // 채널 전송 시 id 입력.

      if (!err) {
        const new_room_state = { room_id: info.insertId };
        // 객실 상태 등록.
        RoomState.insertRoomState(new_room_state, (err, info) => {});
      }

      jsonRes(req, res, err, { info }, "Room successfully inserted");
    });
  }
};

const update_a_room = (req, res) => {
  const { id } = req.params;
  let { room } = req.body;

  if (!id || !room) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room id for update");
  } else {
    Room.updateRoom(id, room, (err, info) => {
      jsonRes(req, res, err, { info }, "Room successfully updated");
    });
  }
};

const delete_a_room = (req, res) => {
  const { id } = req.params;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room id for delete");
  } else {
    // FOREIGN KEY Constraints table ON DELETE CASCADE ON UPDATE CASCADE
    Room.deleteRoom(id, (err, info) => {
      jsonRes(req, res, err, { info }, "Room successfully deleted");
    });
  }
};

export default {
  list_all_rooms,
  list_place_rooms,
  list_a_rooms,
  create_a_room,
  read_a_room,
  read_a_room_by_name,
  update_a_room,
  delete_a_room,
  list_a_enable_reserv_rooms,
};
