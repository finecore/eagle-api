import { jsonRes, sendRes, isValidToken, checkPermission } from "utils/api-util";
import { ERROR, PREFERENCES } from "constants/constants";
import { STATE_CODE_TO_VLUE, STATE_VALUE_TO_TXT, STATE_VALUE_TO_CODE, RES_JSON } from "constants/api-constants";

import moment from "moment";
import _ from "lodash";
import md5 from "md5";

import { YYDoorLock } from "utils/doorlock-util";

import RoomDoorLock from "model/room-doorlock";
import Room from "model/room";
import Sms from "model/sms";

// YY 도어락 API 유틸.
let yYDoorLock = new YYDoorLock();

const get_yy_qrcode = (req, res) => {
  let { pms, room, check_in, check_out } = req.params;
  let {
    auth: { place_id },
  } = req;

  console.log("- get_yy_qrcode", { place_id, pms, room, check_in, check_out });

  if (!place_id || !pms || !room || !check_in || !check_out) {
    return RES_JSON(pms, res, 500, "요청 파라메터가 올바르지 않습니다.", { pms, room });
  } else {
    PREFERENCES.Get(place_id, (preferences) => {
      const { doorlock_send_qr_code, doorlock_appid, doorlock_username, doorlock_userid, doorlock_password } = preferences;

      // 도어락 로그인 토큰 조회.(QR 도어락 사용 시)
      if (doorlock_send_qr_code && doorlock_appid) {
        let params = {
          APPID: doorlock_appid,
          AT: String(new Date().getTime()), // Time Stam（미리초 단위）
          NONCESTR: String(new Date().getTime()), // 램덤 코드
          PASSWORD: doorlock_password, // 고객 비밀번호
          USERNAME: doorlock_username, // 고객 이름
        };

        // 파라메터명 정렬.
        var query = yYDoorLock.orderedQuery(params);

        yYDoorLock.login(query).then((data) => {
          let {
            result,
            token,
            dhUser: { USERNAME, USER_ID },
            msg,
          } = data;

          if (result !== 0) {
            return RES_JSON(pms, res, 400, "YYQR 로그인오류 입니다.", { query });
          } else {
            Room.selectRoomByName(place_id, room, (err, row) => {
              console.log("-> read_room", room, row);

              if (err || !row[0]) {
                return RES_JSON(pms, res, 400, "객실 상태 조회 오류 입니다.", { pms, room });
              } else {
                let { id, doorlock_id } = row[0];

                if (doorlock_id) {
                  let sDate = moment(check_in, "YYYYMMDDHHmm");
                  let eDate = moment(check_out, "YYYYMMDDHHmm");

                  // QR코드 유효시간
                  let ms = sDate.diff(sDate);
                  let d = moment.duration(ms);
                  let hh = Math.floor(d.asHours());
                  let mm = Math.floor(d.asMinutes());

                  console.log("- YYDoorLock date ", { sDate, eDate, mm });

                  let params = {
                    APPID: doorlock_appid,
                    AT: String(new Date().getTime()), // Time Stam（미리초 단위）
                    USERNAME: doorlock_username, // 고객 이름
                    PASSWORD: doorlock_password, // 고객 비밀번호
                    USERID: doorlock_userid, // user china Phone number (중국 김문표 전화번호: 18669800621)
                    KEYLOCKID: doorlock_id, // 객실 도어락의 장비ID
                    TOKEN: token, // 대화token
                    STARTDATE: sDate.format("YYYYMMDDHHmmss"), // Start Time(Format：년월일 시간 분 초)
                    ENDDATE: eDate.format("YYYYMMDDHHmmss"), // End Time(Format：yyyymmddhhmmss)
                    VALIDMINUTE: mm, // QR코드 유효시간（분,minute 단위）
                    NONCESTR: String(new Date().getTime()), // 램덤 코드
                    TIMEZONE: "-9", // TimeZone（;// TimeZone,eg. Chinese timezone is -8, and South Korea's timezone is -9）
                    SAVEFLAGE: "0", // Save flag， 1：저장 ,0：저장안함
                  };

                  // 파라메터명 정렬.
                  var query = yYDoorLock.orderedQuery(params);

                  // 서명 암호화.
                  // 앱 개발자 키는 서명에는 참여 하지만 파라메터 전송은 하지 않는다.
                  let signStr = `${query}&APPKEY=${yYDoorLock.APPKEY}`;

                  let sign = md5(signStr).toUpperCase();

                  query += `&SIGN=${sign}`;

                  yYDoorLock.getLockQRCode(query).then((qrRes) => {
                    let { result, data, msg } = qrRes;

                    if (result === 0) {
                      return RES_JSON(pms, res, 200, null, { data });
                    } else {
                      return RES_JSON(pms, res, 500, msg);
                    }
                  });
                } else {
                  return RES_JSON(pms, res, 400, "객실 도어락 ID를 등록해주세요.", { pms, room });
                }
              }
            });
          }
        });
      }
    });
  }
};

export default {
  get_yy_qrcode,
};
