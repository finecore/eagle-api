import RoomView from "model/room-view";
import { jsonRes, sendRes, isValidToken, checkPermission } from "utils/api-util";
import { ERROR } from "constants/constants";

const list_all_room_views = (req, res) => {
  if (!req.params.place_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide place_id for get room view list");
  } else {
    RoomView.selectAllRoomViews(req.params.place_id, (err, all_room_views) => {
      jsonRes(req, res, err, { all_room_views });
    });
  }
};

const read_a_room_view = (req, res) => {
  if (!req.params.id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room view id for select");
  } else {
    RoomView.selectRoomView(req.params.id, (err, room_view) => {
      jsonRes(req, res, err, { room_view });
    });
  }
};

const create_a_room_view = (req, res) => {
  const { place_id } = req.params;
  const new_room_view = req.body.room_view; // post body.

  if (!place_id || !new_room_view.title) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room view data for insert");
  } else {
    RoomView.insertRoomView(new_room_view, (err, info) => {
      if (info && info.insertId) req.body.room_view.id = info.insertId; // 채널 전송 시 id 입력.

      jsonRes(req, res, err, { info }, "RoomView successfully inserted");
    });
  }
};

const update_a_room_view = (req, res) => {
  if (!req.params.id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room view id for update");
  } else {
    RoomView.updateRoomView(req.params.id, req.body.room_view, (err, info) => {
      jsonRes(req, res, err, { info }, "RoomView successfully updated");
    });
  }
};

const delete_a_room_view = (req, res) => {
  if (!req.params.id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room view id for delete");
  } else {
    // FOREIGN KEY Constraints ON DELETE CASCADE ON UPDATE CASCADE
    RoomView.deleteRoomView(req.params.id, (err, info) => {
      jsonRes(req, res, err, { info }, "RoomView successfully deleted");
    });
  }
};

const delete_all_room_view = (req, res) => {
  if (!req.params.place_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide place_id for place all room views delete");
  } else {
    RoomView.deleteAllRoomViews(req.params.place_id, (err, info) => {
      jsonRes(req, res, err, { info }, "RoomView successfully deleted");
    });
  }
};

export default {
  list_all_room_views,
  create_a_room_view,
  read_a_room_view,
  update_a_room_view,
  delete_a_room_view,
};
