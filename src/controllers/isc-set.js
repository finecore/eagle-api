import IscSet from "model/isc-set";
import { jsonRes, sendRes, isValidToken, checkPermission } from "utils/api-util";
import { ERROR } from "constants/constants";

const list_a_isc_sets = (req, res) => {
  let { filter = "1=1", limit = "10000", order = "reg_date", desc = "desc" } = req.query; // paging.

  console.log("-> filter", filter);

  IscSet.selectIscSetCount(filter, (err, count) => {
    if (err) {
      jsonRes(req, res, err, { count });
    } else
      IscSet.selectIscSets(filter, order, desc, limit, (err, isc_sets) => {
        jsonRes(req, res, err, { count, isc_sets });
      });
  });
};

const list_a_isc_set_place_id = (req, res) => {
  const { place_id, type = "02" } = req.params;

  if (!place_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide place_id for get isc_set list");
  } else {
    IscSet.selectIscSetByPlaceId(place_id, type, (err, isc_sets) => {
      jsonRes(req, res, err, { isc_sets });
    });
  }
};

const read_a_isc_set_serialno = (req, res) => {
  const { serialno } = req.params;

  if (!serialno) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide serialno for get isc_set list");
  } else {
    IscSet.selectSerialnoIscSet(serialno, (err, isc_set) => {
      jsonRes(req, res, err, { isc_set });
    });
  }
};

const read_a_isc_set = (req, res) => {
  const { id } = req.params;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide isc_set id for select");
  } else {
    IscSet.selectIscSet(id, (err, isc_set) => {
      jsonRes(req, res, err, { isc_set });
    });
  }
};

const create_a_isc_set = (req, res) => {
  let new_isc_set = req.body.isc_set; // post body.

  console.log("- create_a_isc_set", new_isc_set);

  if (!new_isc_set.serialno) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide isc_set serialno for insert");
  } else {
    IscSet.insertIscSet(new_isc_set, (err, info) => {
      if (info && info.insertId) req.body.isc_set.id = info.insertId; // 채널 전송 시 id 입력.

      jsonRes(req, res, err, { info }, "IscSet successfully inserted");
    });
  }
};

const update_a_isc_set = (req, res) => {
  const { id } = req.params;
  let { isc_set } = req.body; // post body.

  if (!id || !isc_set) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide isc_set id for update");
  } else {
    IscSet.updateIscSet(id, isc_set, (err, info) => {
      jsonRes(req, res, err, { info }, "IscSet successfully updated");
    });
  }
};

const delete_a_isc_set = (req, res) => {
  const { id } = req.params;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide isc_set id for delete");
  } else {
    // FOREIGN KEY Constraints table ON DELETE CASCADE ON UPDATE CASCADE
    IscSet.deleteIscSet(id, (err, info) => {
      jsonRes(req, res, err, { info }, "IscSet successfully deleted");
    });
  }
};

export default {
  list_a_isc_sets,
  list_a_isc_set_place_id,
  read_a_isc_set_serialno,
  create_a_isc_set,
  read_a_isc_set,
  update_a_isc_set,
  delete_a_isc_set,
};
