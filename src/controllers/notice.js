import Notice from "model/notice";
import File from "model/file";
import { jsonRes, sendRes, isValidToken, checkPermission } from "utils/api-util";
import { ERROR } from "constants/constants";

const list_a_notices = (req, res) => {
  let {
    filter = "1=1",
    between = "a.reg_date",
    begin = moment().add(-1, "year").format("YYYY-MM-DD 00:00"),
    end = moment().format("YYYY-MM-DD HH:mm"),
    limit = "10000",
    order = "a.reg_date",
    desc = "desc",
    all = "0",
  } = req.params;

  Notice.selectNoticeCount(filter, between, begin, end, all, (err, count) => {
    if (err) {
      jsonRes(req, res, err, { count });
    } else
      Notice.selectNotices(filter, between, begin, end, order, desc, limit, all, (err, notices) => {
        jsonRes(req, res, err, { count, notices });
      });
  });
};

const read_a_notice = (req, res) => {
  const { id } = req.params;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide notice id for select");
  } else {
    Notice.selectNotice(id, (err, notice) => {
      jsonRes(req, res, err, { notice });
    });
  }
};

const create_a_notice = (req, res) => {
  const { notice } = req.body;

  if (!notice) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide notice data for update");
  } else {
    Notice.insertNotice(notice, (err, info) => {
      if (info && info.insertId) req.body.notice.id = info.insertId; // 채널 전송 시 id 입력.

      jsonRes(req, res, err, { info }, "Notice successfully inserted");
    });
  }
};

const update_a_notice = (req, res) => {
  const { id } = req.params;
  const { notice } = req.body;

  if (!id || !notice) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide notice id or data for update");
  } else {
    Notice.updateNotice(id, notice, (err, info) => {
      jsonRes(req, res, err, { info }, "Notice successfully updated");
    });
  }
};

const delete_a_notice = (req, res) => {
  const { id } = req.params;
  const { notice } = req.body;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide notice id for delete");
  } else {
    Notice.deleteNotice(id, (err, info) => {
      jsonRes(req, res, err, { info }, "Notice successfully deleted");
    });
  }
};

export default {
  list_a_notices,
  create_a_notice,
  read_a_notice,
  update_a_notice,
  delete_a_notice,
};
