import MileageLog from "model/mileage-log";
import { jsonRes, sendRes, isValidToken, checkPermission } from "utils/api-util";
import { ERROR } from "constants/constants";
import moment from "moment";

const list_all_mileage_log = (req, res) => {
  const {
    place_id,
    filter = "1=1",
    between = "a.reg_date",
    begin = moment().add(-1, "day").format("YYYY-MM-DD HH:mm"),
    end = moment().format("YYYY-MM-DD HH:mm"),
    order = "a.reg_date desc",
  } = req.params;

  if (!place_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide place_id for get mileage_log log list");
  } else {
    MileageLog.selectAllMileageLogs(place_id, filter, between, begin, end, order, (err, all_mileage_logs) => {
      jsonRes(req, res, err, { all_mileage_logs });
    });
  }
};

const read_a_mileage_logs = (req, res) => {
  let { place_id, limit = 500 } = req.params;

  if (!place_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide mileage_log log place_id for select");
  } else {
    MileageLog.selectMileageLogs(place_id, Number(limit), (err, mileage_logs) => {
      jsonRes(req, res, err, { mileage_logs });
    });
  }
};

const read_a_mileage_day_logs = (req, res) => {
  let { place_id, day = -1 } = req.params;

  if (!place_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide mileage_log place_id for select");
  } else {
    MileageLog.selectMileageDayLogs(place_id, day, (err, mileage_logs) => {
      jsonRes(req, res, err, { mileage_logs });
    });
  }
};

const read_a_mileage_last_logs = (req, res) => {
  let { place_id, last_id = 0 } = req.params;

  if (!place_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide mileage_log place_id for select");
  } else {
    MileageLog.selectMileageLastLogs(place_id, last_id, (err, mileage_logs) => {
      jsonRes(req, res, err, { mileage_logs });
    });
  }
};

const read_a_mileage_log = (req, res) => {
  let { id } = req.params;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide mileage_log id for select");
  } else {
    MileageLog.selectMileageLog(id, (err, mileage_log) => {
      jsonRes(req, res, err, { mileage_log });
    });
  }
};

const create_a_mileage_log = (req, res) => {
  var new_mileage_log = req.body.mileage_log; // post body.

  if (!new_mileage_log.room_id || !new_mileage_log.data) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide mileage_log data for insert");
  } else {
    if (new_mileage_log.change_point !== undefined && new_mileage_log.change_point !== 0)
      MileageLog.insertMileageLog(new_mileage_log, (err, info) => {
        if (info && info.insertId) req.body.mileage_log.id = info.insertId; // 채널 전송 시 id 입력.

        jsonRes(req, res, err, { info }, "MileageLog successfully inserted");
      });
  }
};

const update_a_mileage_log = (req, res) => {
  var mileage_log = req.body.mileage_log; // post body.

  if (!req.params.id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide mileage_log id for update");
  } else {
    MileageLog.updateMileageLog(req.params.id, mileage_log, (err, info) => {
      jsonRes(req, res, err, { info }, "MileageLog successfully updated");
    });
  }
};

const delete_a_mileage_log = (req, res) => {
  if (!req.params.id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide mileage_log id for delete");
  } else {
    MileageLog.deleteMileageLog(req.params.room_id, (err, info) => {
      jsonRes(req, res, err, { info }, "MileageLog successfully deleted");
    });
  }
};

export default {
  list_all_mileage_log,
  create_a_mileage_log,
  read_a_mileage_logs,
  read_a_mileage_day_logs,
  read_a_mileage_last_logs,
  read_a_mileage_log,
  update_a_mileage_log,
  delete_a_mileage_log,
};
