// import ApiPms from "model/api-pms";
import { jsonRes, sendRes, isValidToken, checkPermission } from "utils/api-util";
import { ERROR, PREFERENCES } from "constants/constants";
import { HAS_PMS, GET_PMS_DATA, STATE_CODE_TO_VLUE, STATE_VALUE_TO_TXT, STATE_VALUE_TO_CODE, RES_JSON } from "constants/api-constants";
import { displayState } from "constants/key-map";
import RoomState from "model/room-state";

import moment from "moment";
import _ from "lodash";
import request from "request";

import ip from "ip";

const ipaddr = ip.address(); // ip address

const MODE = process.env.REACT_APP_MODE;

// 객실 상태 전송
const send_pms = (options) => {
  console.log("-> send_pms", { options });

  return new Promise(function (resolve, reject) {
    request(options, function (error, res, body) {
      if (!error && res.statusCode == 200) {
        resolve(body);
      } else {
        reject(error);
      }
    });
  });
};

// 객실 체크인 체크아웃
const put_pms_room_sale = (roomSale) => {
  let { room_id, state, check_in, check_out } = roomSale || {};

  console.log("-> put_pms_room_sale", { room_id, state, check_in, check_out });

  if (room_id && state) {
    let roomState = { room_id, state, check_in, check_out };

    put_pms_room_state(roomState);
  }
};

// 객실 상태/정비상태 단건 업데이트API
const put_pms_room_state = (roomState) => {
  let { room_id, name, sale, clean, key, outing, state, check_in, check_out } = roomState || {};

  console.log("-> put_pms_room_state", { room_id, name, sale, clean, outing, state, check_in, check_out });

  if (room_id && (sale !== undefined || clean !== undefined || outing !== undefined || key !== undefined || state !== undefined)) {
    // 객실 상태 조회.
    RoomState.selectRoomState(room_id, (err, result) => {
      let data = result[0];

      if (!err && data) {
        let { place_id } = data;

        PREFERENCES.Get(place_id, (preferences) => {
          const { pms = "", pms_api_key, pms_company_id } = preferences;

          console.log("---> preferences ", { pms, pms_api_key, pms_company_id });

          // API 통신 PMS 업체만 전송(Sanha, Roonets), 소켓 통신 PMS 업체는 소케서 서버에서 연동(Micronic)
          let hasPms = HAS_PMS(pms);

          console.log("---> hasPms ", hasPms);

          // API 연동 PMS 업소만 처리.
          if (hasPms) {
            try {
              let options = null;

              if (pms.toUpperCase() === "GIGAGENIE") {
                console.log("---> GIGAGENIE ", { state });

                // 기가지니는 체크인/체크아웃만 전송.
                if (state) {
                  let { name } = data;

                  let dateTime = moment((state === "A" ? check_in : check_out) || new Date()).format("YYYYMMDDHHmmss");

                  options = {
                    method: "POST",
                    url: GET_PMS_DATA(pms, "URL")[MODE] + "/api/v1/events",
                    headers: _.merge({}, GET_PMS_DATA(pms, "HEADERS"), { Authorization: pms_api_key }), // Authorization
                    body: {
                      prprtyId: pms_company_id,
                      roomNo: name,
                      reqData: [
                        {
                          evtType: state === "A" ? "et00001001" : "et00001002", // 체크인/체크아웃 구분
                          svcevtPrprtys: [
                            {
                              svcevtPrprtyCd: state === "A" ? "pp00001401" : "pp00001402", // 서비스 속성 코드 (pp00001401 : 체크인 시간, pp00001402 : 체크아웃 시간)
                              svcevtPrprtyVal: dateTime, // 서비스 속성 값
                            },
                            {
                              svcevtPrprtyCd: "pp00002101", // pp00002101 : 체크인시 선택한 언어코드 (ko, en, ja, ch)
                              svcevtPrprtyVal: "ko",
                            },
                          ],
                        },
                      ],
                    },
                    json: true, // Automatically stringifies the body to JSON
                  };
                }
              } else {
                let code = STATE_VALUE_TO_CODE(pms, data); // 객실상태

                options = {
                  method: "PUT",
                  url: GET_PMS_DATA(pms, "URL") + "/updateRoomStatus",
                  headers: _.merge({}, GET_PMS_DATA(pms, "HEADERS"), { "API-KEY": pms_api_key }),
                  formData: _.merge(
                    {},
                    GET_PMS_DATA(pms, "BODY"),
                    pms.toUpperCase() === "SANHA"
                      ? {
                          companyId: pms_company_id,
                          intfType: "SINGLE",
                          recvXML: code,
                          roomNo: data.name,
                          userIp: ipaddr,
                        }
                      : {
                          data: code,
                        }
                  ),
                };
              }

              // console.log("-> put_pms_room_state options", options);

              // 전송.
              if (options) {
                send_pms(options)
                  .then((body) => {
                    console.log("-> send_pms done", body);
                  })
                  .catch((error) => {
                    console.log("-> send_pms error ", error);
                  });
              }
            } catch (e) {
              console.error("- PMS 연동 오류", e);
            }
          }
        });
      }
    });
  }
};

// 객실 상태/정비 상태 All Sync(동기화)
const put_pms_room_state_all = (place_id, pms) => {
  console.log("-> put_pms_room_state_all", { place_id, pms });

  // 모든 객실 상태 조회.
  RoomState.selectAllRoomStatesSale(place_id, (err, result) => {
    if (!err && result[0]) {
      PREFERENCES.Get(place_id, (preferences) => {
        const { pms_api_key, pms_company_id } = preferences;

        // console.log("---> preferences ", { pms_api_key, pms_company_id });

        let hasPms = HAS_PMS(pms);

        // API 연동 PMS 업소만 처리.(산하 정보기술)
        if (hasPms && pms_company_id) {
          let codes = "";

          _.each(result, (data, idx) => {
            if (data) {
              let code = STATE_VALUE_TO_CODE(pms, data); // 객실상태
              codes += (codes ? "/" : "") + code;
            }
          });

          if (codes) {
            var options = {
              method: "PUT",
              url: GET_PMS_DATA(pms, "URL") + "/syncAllRoomStatusUpdate",
              headers: _.merge({}, GET_PMS_DATA(pms, "HEADERS"), { "API-KEY": pms_api_key }),
              formData: _.merge(
                {},
                GET_PMS_DATA(pms, "BODY"),
                pms === "sanha"
                  ? {
                      companyId: pms_company_id,
                      intfType: "MULTI",
                      recvXML: codes,
                      roomNo: "0000",
                      userIp: ipaddr,
                    }
                  : {
                      data: codes,
                    }
              ),
            };

            // 전송.
            send_pms(options)
              .then((body) => {
                console.log("-> send_pms done", body);
              })
              .catch((error) => {
                console.log("-> send_pms error ", error);
              });
          }
        }
      });
    }
  });
};

// PMS 싱크 연동.
setInterval(() => {
  const mmss = Number(moment().format("mmss"));

  if (MODE !== "LOCAL") {
    // 매 시간 전송.
    if (mmss === 0) {
      console.log("- mmss", mmss);

      PREFERENCES.List((list) => {
        // PMS 사용 업소 모두 전송.
        _.map(list, (preference) => {
          const { place_id, pms, pms_api_key } = preference;

          if (pms && pms_api_key) put_pms_room_state_all(place_id, pms);
        });
      });
    }
  }
}, 1000);

export default {
  put_pms_room_sale,
  put_pms_room_state,
  put_pms_room_state_all,
};
