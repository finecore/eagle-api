import RoomStateLog from "model/room-state-log";
import { jsonRes, sendRes, isValidToken, checkPermission } from "utils/api-util";
import { ERROR } from "constants/constants";
import moment from "moment";
import _ from "lodash";

const list_all_room_state_log = (req, res) => {
  const {
    place_id,
    filter = "1=1",
    between = "a.reg_date",
    begin = moment().add(-1, "day").format("YYYY-MM-DD HH:mm"),
    end = moment().format("YYYY-MM-DD HH:mm"),
    order = "a.reg_date desc",
    limit = 100000,
  } = req.params;

  console.log("- list_all_room_state_log", { limit });

  if (!place_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide place_id for get room_state_log log list");
  } else {
    RoomStateLog.selectAllRoomStateLogs(place_id, filter, between, begin, end, order, limit, (err, all_room_state_logs) => {
      if (all_room_state_logs) {
        _.each(all_room_state_logs, (row) => (row.data = JSON.parse(row.data)));
      }
      jsonRes(req, res, err, { all_room_state_logs });
    });
  }
};

const read_a_room_state_logs = (req, res) => {
  let { place_id, limit = 500 } = req.params;

  if (!place_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room_state_log log place_id for select");
  } else {
    RoomStateLog.selectRoomStateLogs(place_id, Number(limit), (err, room_state_logs) => {
      jsonRes(req, res, err, { room_state_logs });
    });
  }
};

const read_a_room_state_day_logs = (req, res) => {
  let { place_id, day = -1 } = req.params;

  if (!place_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room_state_log place_id for select");
  } else {
    RoomStateLog.selectRoomStateDayLogs(place_id, day, (err, room_state_logs) => {
      if (room_state_logs) {
        _.each(room_state_logs, (row) => (row.data = JSON.parse(row.data)));
      }
      jsonRes(req, res, err, { room_state_logs });
    });
  }
};

const read_a_room_state_last_logs = (req, res) => {
  let { place_id, last_id = 0 } = req.params;

  if (!place_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room_state_log place_id for select");
  } else {
    RoomStateLog.selectRoomStateLastLogs(place_id, last_id, (err, room_state_logs) => {
      if (room_state_logs) {
        _.each(room_state_logs, (row) => (row.data = JSON.parse(row.data)));
      }
      jsonRes(req, res, err, { room_state_logs });
    });
  }
};

const read_a_room_state_log = (req, res) => {
  let { id } = req.params;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room_state_log id for select");
  } else {
    RoomStateLog.selectRoomStateLog(id, (err, room_state_log) => {
      jsonRes(req, res, err, { room_state_log });
    });
  }
};

const create_a_room_state_log = (req, res) => {
  var room_state_log = req.body.room_state_log; // post body.

  let {
    headers: { channel },
    auth: { id, name },
  } = req;

  if (!room_state_log.room_id || !room_state_log.data) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room_state_log data for insert");
  } else {
    if (!room_state_log.channel) room_state_log.channel = channel || "web";
    room_state_log.user_id = room_state_log.channel === "device" ? name : id;

    // console.log("---> create_a_room_state_log", JSON.stringify(room_state_log));

    RoomStateLog.insertRoomStateLog(room_state_log, req, (err, info) => {
      if (info && info.insertId) req.body.room_state_log.id = info.insertId; // 채널 전송 시 id 입력.

      jsonRes(req, res, err, { info }, "RoomStateLog successfully inserted");
    });
  }
};

const update_a_room_state_log = (req, res) => {
  const { id } = req.params;
  let { room_state_log } = req.body; // post body.
  let {
    headers: { channel },
    auth,
  } = req;

  console.log("- update_a_room_state_log ", channel, room_state_log);

  if (!id || !room_state_log) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room_state_log id for update");
  } else {
    room_state_log.channel = channel || "web";
    room_state_log.user_id = room_state_log.channel === "device" ? auth.name : auth.id;

    RoomStateLog.updateRoomStateLog(id, room_state_log, (err, info) => {
      jsonRes(req, res, err, { info }, "RoomStateLog successfully updated");
    });
  }
};

const delete_a_room_state_log = (req, res) => {
  if (!req.params.id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room_state_log id for delete");
  } else {
    // FOREIGN KEY Constraints ON DELETE CASCADE ON UPDATE CASCADE
    RoomStateLog.deleteRoomStateLog(req.params.room_id, (err, info) => {
      jsonRes(req, res, err, { info }, "RoomStateLog successfully deleted");
    });
  }
};

export default {
  list_all_room_state_log,
  create_a_room_state_log,
  read_a_room_state_logs,
  read_a_room_state_day_logs,
  read_a_room_state_last_logs,
  read_a_room_state_log,
  update_a_room_state_log,
  delete_a_room_state_log,
};
