import UserPlace from "model/user-place";
import { jsonRes, sendRes, isValidToken, checkPermission } from "utils/api-util";
import { ERROR } from "constants/constants";
import crypto from "crypto";

const list_a_user_places = (req, res) => {
  let { user_id, filter = "1=1", limit = "10000", order = "a.id", desc = "desc" } = req.params;

  if (user_id) filter = "user_id='" + user_id + "'";

  UserPlace.selectUserPlaceCount(filter, (err, count) => {
    if (err) {
      jsonRes(req, res, err, { count });
    } else
      UserPlace.selectUserPlaces(filter, order, desc, limit, (err, user_places) => {
        jsonRes(req, res, err, { count, user_places });
      });
  });
};

const read_a_user_place = (req, res) => {
  const { id } = req.params;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide user_place id for select");
  } else {
    UserPlace.selectUserPlace(id, (err, user_place) => {
      jsonRes(req, res, err, { user_place });
    });
  }
};

const create_a_user_place = (req, res) => {
  const { user_place } = req.body; // post body.

  if (!user_place.place_id || !user_place.user_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide user_place place_id ,user_id for insert");
  } else {
    UserPlace.insertUserPlace(user_place, (err, info) => {
      if (info && info.insertId) req.body.user_place.id = info.insertId; // 채널 전송 시 id 입력.

      jsonRes(req, res, err, { info }, "UserPlace successfully inserted");
    });
  }
};

const update_a_user_place = (req, res) => {
  const { id } = req.params;
  const { user_place } = req.body; // post body.

  if (!id || !user_place) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide user_place id or data for update");
  } else {
    UserPlace.updateUserPlace(id, user_place, (err, info) => {
      jsonRes(req, res, err, { info }, "UserPlace successfully updated");
    });
  }
};

const delete_a_user_place = (req, res) => {
  const { id } = req.params;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide user_place id for delete");
  } else {
    // FOREIGN KEY Constraints table ON DELETE CASCADE ON UPDATE CASCADE
    UserPlace.deleteUserPlace(id, (err, info) => {
      jsonRes(req, res, err, { info }, "UserPlace successfully deleted");
    });
  }
};

export default {
  list_a_user_places,
  create_a_user_place,
  read_a_user_place,
  update_a_user_place,
  delete_a_user_place,
};
