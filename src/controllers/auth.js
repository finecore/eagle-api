import jwt from "jsonwebtoken";
import User from "model/user";
import { jsonRes } from "utils/api-util";
import { ERROR } from "constants/constants";
import { verify, sign } from "utils/jwt-util";
import Device from "model/device";
import crypto from "crypto";

const user_login = (req, res) => {
  let {
    headers: { channel = "web" },
    body: { id, pwd },
  } = req;

  console.log("login request:", req.body);

  if (pwd) {
    if (pwd.length < 20) {
      // 해시 생성
      var shasum = crypto.createHash("sha1");
      shasum.update(pwd);

      // 비밀번호 sha1 형식으로 암호화.
      pwd = shasum.digest("hex");
      console.log("- pwd sha1 ", pwd);
    } else {
      return jsonRes(req, res, ERROR.INVALID_ARGUMENT, "비밀번호를 확인해 주세요.");
    }
  }

  // query.
  User.selectUserByIdPwd(id, pwd, (err, rows) => {
    let user = rows ? rows[0] : null;

    if (err) {
      return jsonRes(req, res, err);
    } else if (!user) {
      jsonRes(req, res, ERROR.NO_DATA, "아이디 와 비밀번호를 확인해 주세요.");
    } else if (user.pwd != pwd) {
      jsonRes(req, res, ERROR.INVALID_ARGUMENT, "비밀번호를 확인해 주세요.");
    } else if (user.use_yn !== "Y") {
      // 사용자 사용 체크.
      jsonRes(req, res, ERROR.INVALID_USER);
    } else if (!user.place_expire) {
      // 업소 인증 만료 체크.
      jsonRes(req, res, ERROR.EXPIRE_CERTIFICATION);
    } else if (channel === "admin" && user.level > 2) {
      // 매니저 권한 이상만 관리자 로그인 가능.
      jsonRes(req, res, ERROR.INVALID_AUTHORITY);
    } else {
      // JWT(Json Web Token) 생성.
      const { channel, id, place_id, level, type, pms, eagle_url } = user;
      const {
        headers: { uuid = "" },
      } = req;

      const payload = { channel, id, place_id, level, type, uuid, pms, eagle_url };

      sign(payload, function (err, token) {
        return jsonRes(req, res, err, { user, token });
      });
    }
  });
};

const create_device_token = (req, res) => {
  const {
    headers: { serialno },
  } = req;

  console.log("-----> create_device_token serialno ", serialno);

  if (!serialno) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Device serialno 가 없습니다.");
  } else {
    Device.selectDeviceBySerialNo(serialno, (err, rows) => {
      const device = rows ? rows[0] : null;

      console.log("- create_device_token ", device);

      if (err) {
        return jsonRes(req, res, err);
      } else if (!device) {
        jsonRes(req, res, ERROR.NO_DATA);
      } else if (!device.expire) {
        // 장비 인증 만료 체크. type (01: IDM, 02: ISG, 10:RPT)
        jsonRes(req, res, device.type === "01" ? ERROR.IDM_EXPIRE_CERTIFICATION : device.type === "02" ? ERROR.ISG_EXPIRE_CERTIFICATION : ERROR.EXPIRE_CERTIFICATION);
      } else if (!device.place_id || !device.place_expire) {
        // 업소 인증 만료 체크.
        jsonRes(req, res, ERROR.EXPIRE_CERTIFICATION);
      } else {
        // JWT(Json Web Token) 생성.
        const { channel = "device", id, place_id, type, name, serialno, pms } = device;

        const payload = { channel, id, place_id, type, name, serialno, pms };

        sign(payload, function (err, token) {
          return jsonRes(req, res, err, { token, device });
        });
      }
    });
  }
};

const delete_device_token = (req, res) => {
  const { headers: serialno } = req;

  if (serialno) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Device seriano 가 없습니다.");
  } else {
    // JWT(Json Web Token) 생성.
    const payload = ({ seriano } = req.params);

    sign(
      payload,
      function (err, token) {
        return jsonRes(req, res, err, { token });
      },
      -1 // 생성 후 만료 시킴.
    );
  }
};

const alive = (req, res) => {
  jsonRes(req, res, null, { alive: true });
};

export default {
  user_login,
  create_device_token,
  delete_device_token,
  alive,
};
