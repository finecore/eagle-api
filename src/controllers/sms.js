import Sms from "model/sms";
import { jsonRes, sendRes, isValidToken, checkPermission } from "utils/api-util";
import { ERROR } from "constants/constants";

const makeAuthNo = (n) => {
  var value = "";
  for (var i = 0; i < n; i++) {
    let n1 = 0;
    let n2 = 9;
    value += parseInt(Math.random() * (n2 - n1 + 1)) + n1;
  }
  console.log("- make authNo", value);
  return value;
};

const list_a_smses = (req, res) => {
  let { filter = "1=1", limit = "10000", order = "a.tr_num", desc = "asc" } = req.params;

  Sms.selectSmsCount(filter, (err, count) => {
    if (err) {
      jsonRes(req, res, err, { count });
    } else
      Sms.selectSmses(filter, order, desc, limit, (err, smses) => {
        jsonRes(req, res, err, { count, smses });
      });
  });
};

const read_a_sms = (req, res) => {
  const { tr_num } = req.params;

  if (!tr_num) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide sms id or phone for select");
  } else {
    Sms.selectSms(tr_num, (err, sms) => {
      jsonRes(req, res, err, { count, sms });
    });
  }
};

const send_auth_no = (req, res) => {
  const { phone } = req.params;

  let authNo = makeAuthNo(5);

  let sms = {
    tr_senddate: new Date(),
    tr_sendstat: "0",
    tr_msgtype: "0",
    tr_callback: "1600-5356",
    tr_phone: phone,
    tr_msg: "[아이크루 인증번호]\n" + authNo,
  };

  console.log("- authNo", authNo);

  if (!sms.tr_phone) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide sms data for insert");
  } else {
    Sms.insertSms(sms, (err, info) => {
      jsonRes(req, res, err, { info, authNo }, "Sms successfully inserted");
    });
  }
};

const create_a_sms = (req, res) => {
  let { sms } = req.body;

  console.log("- sms", sms);

  if (!sms.tr_senddate) sms.tr_senddate = new Date();
  if (!sms.tr_sendstat) sms.tr_sendstat = "0";
  if (!sms.tr_msgtype) sms.tr_msgtype = "0";
  if (!sms.tr_callback) sms.tr_callback = "1600-5356";

  if (!sms.tr_phone || !sms.tr_msg) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide sms data for insert");
  } else {
    Sms.insertSms(sms, (err, info) => {
      if (info && info.insertId) req.body.sms.id = info.insertId; // 채널 전송 시 id 입력.

      jsonRes(req, res, err, { info }, "Sms successfully inserted");
    });
  }
};

const update_a_sms = (req, res) => {
  const { tr_num } = req.params;
  let { sms } = req.body;

  if (!tr_num || !sms.tr_msg) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide sms id for update");
  } else {
    Sms.updateSms(tr_num, sms, (err, info) => {
      jsonRes(req, res, err, { info }, "Sms successfully updated");
    });
  }
};

const delete_a_sms = (req, res) => {
  const { tr_num } = req.params;

  if (!tr_num) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide sms id for delete");
  } else {
    Sms.deleteSms(tr_num, (err, info) => {
      jsonRes(req, res, err, { info }, "Sms successfully deleted");
    });
  }
};

export default {
  list_a_smses,
  send_auth_no,
  create_a_sms,
  read_a_sms,
  update_a_sms,
  delete_a_sms,
  makeAuthNo,
};
