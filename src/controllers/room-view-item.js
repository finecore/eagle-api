import RoomViewItem from "model/room-view-item";
import { jsonRes, sendRes, isValidToken, checkPermission } from "utils/api-util";
import { ERROR } from "constants/constants";

const list_all_room_view_items = (req, res) => {
  if (!req.params.place_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide place_id for get room view items list");
  } else {
    RoomViewItem.selectAllRoomViewItems(req.params.place_id, (err, all_room_view_items) => {
      jsonRes(req, res, err, { all_room_view_items });
    });
  }
};

const list_room_view_items = (req, res) => {
  if (!req.params.view_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room view items id for select");
  } else {
    RoomViewItem.selectRoomViewItems(req.params.view_id, (err, room_view_items) => {
      jsonRes(req, res, err, { room_view_items });
    });
  }
};

const read_a_room_view_item = (req, res) => {
  if (!req.params.id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room view items id for select");
  } else {
    RoomViewItem.selectRoomViewItem(req.params.id, (err, room_view_item) => {
      jsonRes(req, res, err, { room_view_item });
    });
  }
};

const create_a_room_view_item = (req, res) => {
  var new_room_view_item = req.body.room_view_item; // post body.
  console.log("- create_a_room_view_item", new_room_view_item);
  if (!new_room_view_item.view_id || !new_room_view_item.room_id || !new_room_view_item.col || !new_room_view_item.row) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room view items data for insert");
  } else {
    RoomViewItem.insertRoomViewItem(new_room_view_item, (err, info) => {
      if (info && info.insertId) req.body.room_view_item.id = info.insertId; // 채널 전송 시 id 입력.

      jsonRes(req, res, err, { info }, "RoomViewItem succesfully inserted");
    });
  }
};

const update_a_room_view_item = (req, res) => {
  if (!req.params.id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room view items id for update");
  } else {
    RoomViewItem.putRoomViewItem(req.params.id, req.body.room_view_item, (err, info) => {
      jsonRes(req, res, err, { info }, "RoomViewItem succesfully updated");
    });
  }
};

const update_a_room_view_items = (req, res) => {
  const updateItems = req.body.room_view_items || [];

  if (!req.params.view_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room view id for update");
  } else {
    let isErr = null;

    updateItems.map((item, idx) => {
      console.log("- update_all_room_view_item item", item);
      const { mode } = item;

      if (mode === "new") {
        const { view_id, room_id, row, col } = item;

        RoomViewItem.insertRoomViewItem({ view_id, room_id, row, col }, (err, info) => {
          if (err) isErr = err;

          console.log("- view item inserted ", info, err);
        });
      } else if (mode === "update") {
        const { id, view_id, room_id, row, col } = item;

        RoomViewItem.putRoomViewItem(id, { view_id, room_id, row, col }, (err, info) => {
          if (err) isErr = err;

          console.log("- view item updated ", info, err);
        });
      } else if (mode === "del") {
        const { id } = item;

        RoomViewItem.deleteRoomViewItem(id, (err, info) => {
          if (err) isErr = err;

          console.log("- view item deleted ", info, err);
        });
      }

      if (idx === updateItems.length - 1) {
        jsonRes(req, res, isErr, { info: {} }, isErr ? "RoomViewItem failue updated" : "RoomViewItem succesfully updated");
      }
    });
  }
};

const delete_a_room_view_item = (req, res) => {
  if (!req.params.view_id || !new_room_view_item.room_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room view items id for delete");
  } else {
    RoomViewItem.deleteRoomViewItem(req.params.view_id, req.params.room_id, (err, info) => {
      jsonRes(req, res, err, { info }, "RoomViewItem succesfully deleted");
    });
  }
};

const delete_a_room_view_items = (req, res, callback) => {
  if (!req.params.view_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide view_id for view all room view items delete");
  } else {
    RoomViewItem.deleteAllRoomViewItems(req.params.view_id, (err, info) => {
      jsonRes(req, res, err, { info }, "RoomViewItem succesfully deleted");
    });
  }
};

export default {
  list_all_room_view_items,
  list_room_view_items,
  read_a_room_view_item,
  create_a_room_view_item,
  update_a_room_view_item,
  update_a_room_view_items,
  delete_a_room_view_item,
  delete_a_room_view_items,
};
