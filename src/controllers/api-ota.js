import MmsMo from "model/mms-mo";
import { jsonRes, sendRes, isValidToken, checkPermission } from "utils/api-util";
import { ERROR } from "constants/constants";
import moment from "moment";
import _ from "lodash";

const mms = (req, res) => {
  let { name = "" } = req.params;

  console.log("--> ota mms", name, req.body);

  let { date, from, to, message } = req.body;

  // 특수 문자 삭제
  from = from.replace(/[^\d]/gi, "");
  to = to.replace(/[^\d]/gi, "");
  message = message.replace(/[`~!@#$%^&*|※+;` ]/gi, "");

  console.log("--> ota parse", { date, from, to, message });

  if (message) {
    let mms_mo = {
      MSG_TYPE: "LMS",
      PHONE: from,
      CALLBACK: to,
      SUBJECT: name,
      CONTENT: message,
      RESULT_DATE: moment().format("YYYY-MM-DD HH:mm:ss"),
      REPORT_DATE: date,
      TEL: "SKT",
      IS_REPLY: "N",
    };

    MmsMo.insertMmsMo(mms_mo, (err, info) => {
      jsonRes(req, res, err, { info }, "MmsMo successfully inserted");
    });
  } else {
    console.error("-> OTA 예약정보가 없습니다", { name, date, from, to, message });
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "OTA 예약정보가 없습니다.");
  }
};

export default {
  mms,
};
