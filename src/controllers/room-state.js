import RoomState from "model/room-state";
import RoomStateLog from "model/room-state-log";
import RoomSale from "model/room-sale";
import RoomSalePay from "model/room-sale-pay";
import RoomFee from "model/room-fee";
import Room from "model/room";
import SeasonPremium from "model/season-premium";
import Place from "model/place";
import RoomDoorLock from "model/room-doorlock";

import { jsonRes, sendRes, isValidToken, checkPermission } from "utils/api-util";
import { ERROR, PREFERENCES } from "constants/constants";

import _ from "lodash";
import moment from "moment";
import axios from "axios";

const { sign } = require("utils/jwt-util");

const list_all_room_state = (req, res) => {
  if (!req.params.place_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide place_id for get room state list");
  } else {
    RoomState.selectAllRoomStates(req.params.place_id, (err, all_room_states) => {
      jsonRes(req, res, err, { all_room_states });
    });
  }
};

const list_a_room_states = (req, res) => {
  let { place_id, filter = "1=1", limit = "10000", order = "a.reg_date", desc = "desc" } = req.params;

  if (place_id) filter = `b.place_id=${place_id}`;

  RoomState.selectRoomStateCount(filter, (err, count) => {
    if (err) {
      jsonRes(req, res, err, { count });
    } else
      RoomState.selectRoomStates(filter, order, desc, limit, (err, isc_states) => {
        jsonRes(req, res, err, { count, isc_states });
      });
  });
};

const read_a_room_state = (req, res) => {
  const { room_id } = req.params;

  if (!req.params.room_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room state id for select");
  } else {
    RoomState.selectRoomState(room_id, (err, room_state) => {
      if (!err && !room_state[0]) {
        const new_room_state = {};
        new_room_state["room_id"] = room_id;

        // 없으면 등록 후 재조회
        RoomState.insertRoomState(new_room_state, (err, info) => {
          if (!err) {
            RoomState.selectRoomState(room_id, (err, room_state) => {
              jsonRes(req, res, err, { room_state });
            });
          }
        });
      } else jsonRes(req, res, err, { room_state });
    });
  }
};

const read_a_room_state_by_name = (req, res) => {
  const { place_id, room_name } = req.params;

  if (!place_id || !room_name) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room state place_id or room_name for select");
  } else {
    RoomState.selectRoomStateByName(place_id, room_name, (err, room_state) => {
      if (!err) {
        jsonRes(req, res, err, { room_state });
      }
    });
  }
};

const list_all_room_state_sale = (req, res) => {
  if (!req.params.place_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide place_id for get room state list");
  } else {
    RoomState.selectAllRoomStatesSale(req.params.place_id, (err, all_room_states) => {
      jsonRes(req, res, err, { all_room_states });
    });
  }
};

const read_a_room_state_sale = (req, res) => {
  const { room_id } = req.params;

  if (!req.params.room_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room state id for select");
  } else {
    RoomState.selectRoomState(room_id, (err, room_state) => {
      if (!err && !room_state[0]) {
        const new_room_state = {};
        new_room_state["room_id"] = room_id;

        // 없으면 등록 후 재조회
        RoomState.insertRoomState(new_room_state, (err, info) => {
          if (!err) {
            RoomState.selectRoomStateSale(room_id, (err, room_state) => {
              jsonRes(req, res, err, { room_state });
            });
          }
        });
      } else jsonRes(req, res, err, { room_state });
    });
  }
};

const create_a_room_state = (req, res) => {
  let new_room_state = req.body.room_state; // post body.
  const { room_id, channel } = new_room_state;

  if (!channel) room_state.channel = req.headers.channel || "web";

  if (!room_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room state data for insert");
  } else {
    console.log("---> insertRoomState ", new_room_state);

    // 객실 삭제 시 데몬에서 없는 객실을 올릴때 오류 방지.(객실 수정 시 데몬은 재시작 해줘야 한다)
    Room.selectRoom(room_id, (err, room) => {
      if (err) {
        jsonRes(req, res, err, {}, "RoomState not room id ");
      } else {
        RoomState.insertRoomState(new_room_state, (err, info) => {
          if (info && info.insertId) req.body.room_state.id = info.insertId; // 채널 전송 시 id 입력.

          jsonRes(req, res, err, { info }, "RoomState successfully inserted");
        });
      }
    });
  }
};

const update_signal_room_state = (req, res) => {
  const { signal } = req.body.room_state; // post body.
  const { place_id } = req.params;

  if (!place_id || signal === undefined) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide update_signal_room_state place_id , signal for update");
  } else {
    RoomState.updateRoomStateSignal(place_id, signal, (err, info) => {
      jsonRes(req, res, err, { info }, "RoomState successfully updated");
    });
  }
};

const update_all_room_state = (req, res) => {
  const { place_id } = req.params;
  const { room_state } = req.body; // body.

  if (!place_id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide update_all_room_state place_id for update");
  } else {
    RoomState.updateRoomStateAll(place_id, room_state, (err, info) => {
      jsonRes(req, res, err, { info }, "RoomStateAll successfully updated");
    });
  }
};

const update_a_room_state = (req, res) => {
  let room_state = req.body.room_state; // post body.

  const room_id = req.params.room_id;

  const { channel, user_id, clean, key, sale, inspect, move_from, move_to, room_sale_id, clean_change_time, key_change_time, air_temp, main_relay, car_call, qr_key_yn } = room_state;

  if (!room_id || !room_state) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room state room_id for update");
  } else {
    // 현재 상태 조회.
    RoomState.selectRoomState(room_id, (err, row) => {
      if (!err && row[0]) {
        let data = row[0];

        let { place_id, temp_key_1, temp_key_2, temp_key_3, temp_key_4, temp_key_5, keyless } = data;

        PREFERENCES.Get(place_id, (preferences) => {
          const { pms, inspect_use, air_set_min, air_set_max } = preferences; // PMS 정보조회.

          // 지동 청소 요청.
          if (Number(key) === 3) room_state["clean"] = 1; // 청소 키 삽입 시 청소 요청.

          // 자동 청소 완료.
          if (Number(key) === 6) room_state["clean"] = 0; // 청소 키 제거 시 청소 완료.

          /*
          객실 매출 발생 시 key 가 4,5,6 이면 key = 0 으로 초기화 해야된다.
          아니면 매출 발생 시 화면에 마지막 키 상태가 (청소완료) 가 보이게 된다.
          현재는 카운터/자판기에서 key = 0 으로 보내주나 추 후 다른 장비 추가 시 문제가 발생 할 수 있으니 미리 방지 로직을 추가 해야 한다.
          단) 객실 이동 시는 제외!
         */
          if (sale && !move_from && !move_to && key === undefined && data.key > 3) {
            room_state["key"] = 0; // 키 상태 초기화.
          }

          if (sale === 0 && room_sale_id === undefined) {
            room_state["room_sale_id"] = null;
          }

          let _key = room_state["key"];
          let _clean = room_state["clean"] !== undefined ? room_state["clean"] : data.clean;
          let _outing = room_state["outing"] !== undefined ? room_state["outing"] : data.outing;
          let _sale = room_state["sale"] !== undefined ? room_state["sale"] : data.sale;
          let _qr_key_yn = room_state["qr_key_yn"] !== undefined ? room_state["qr_key_yn"] : data.qr_key_yn; // QR 코드 사용 여부

          if (room_state["key"] > 0) {
            // 키값 변경 시 온도 적용.
            if (_clean === 1 && _key !== 3) {
              room_state["air_set_temp"] = temp_key_5; // 청소대기 온도.
            } else if (_outing === 1 && _key !== 3) {
              room_state["air_set_temp"] = temp_key_2; // 외출 시 온도.
            } else {
              if (_key === 3) {
                room_state["air_set_temp"] = temp_key_4; // 청소중 온도.
              } else if (_sale > 0 && !data.check_out) {
                room_state["air_set_temp"] = temp_key_1; // 사용중 온도.
              } else if (_sale === 0) {
                room_state["air_set_temp"] = temp_key_3; // 공실 온도.
              } else {
                room_state["air_set_temp"] = temp_key_5; // 청소 대기 온도
              }
            }

            // 손님키/청소키 삽입 시
            if (Number(room_state["key"]) === 1 || Number(room_state["key"]) === 3) {
              room_state["air_power_type"] = 0; // 예약 냉방 초기화
            }

            // console.log("- air_set_temp  ", room_state["air_set_temp"], { _key, _clean, _outing, _sale });
          }

          // 공실 시
          if (_sale === 0) {
            // 키리스 강제 전원 OFF
            room_state["keyless"] = 0;
            room_state["qr_key_yn"] = 0;

            // 도어락 정보 삭제.
            RoomDoorLock.updateRoomDoorLockByRoomId(
              room_id,
              {
                qr_key_phone: null,
                qr_key_data: null,
                start_date: null,
                end_date: null,
                cancel: 0,
                send_sms: 0,
                reg_date: null,
                mod_date: null,
              },
              (err, info) => {}
            );
          } else {
            if (_qr_key_yn) room_state["keyless"] = 1; // ON
          }

          // 현재 인스펙트 상태 저장 (인스펙트 중 다른 키 삽입/제거 시 인스펙트 상태 유지를 위해)
          if (room_state["key"] !== undefined) {
            if ((inspect_use === 1 && data.check_out) || inspect_use === 2) {
              if (room_state["key"] === 0) {
                // 공실 시 초기화.
                room_state["inspect"] = 0; // 인스펙트 초기화
              } else if (room_state["key"] === 6) {
                room_state["inspect"] = 1; // 인스펙트 대기
              } else if (room_state["key"] === 2) {
                room_state["inspect"] = 2; // 인스펙트 중
              } else if (room_state["key"] === 5) {
                room_state["inspect"] = 3; // 인스펙트 완료
              } else {
              }
            }
          }

          if (room_state["inspect"] === 1) {
            room_state["air_set_temp"] = temp_key_5; // 청소/점검 대기 온도
          }

          if (room_state["inspect"] === 2) {
            room_state["air_set_temp"] = temp_key_4; // 청소/점검 중 온도
          }

          if (room_state["air_set_temp"] !== undefined) {
            // 최소/최대 설정 온도
            if (Number(room_state["air_set_temp"]) < air_set_min) room_state["air_set_temp"] = air_set_min;
            else if (Number(room_state["air_set_temp"]) > air_set_max) room_state["air_set_temp"] = air_set_max;
          }

          console.log("- update_a_room_state  ", sale, data.sale, car_call, { room_id, room_state });

          let {
            auth: { id, name },
          } = req;

          let changed = {};

          // 변한 값 체크.
          _.map(room_state, (v, k) => {
            if (String(data[k]) !== String(v) && k !== "user_id" && k !== "channel") {
              console.log("- changed data ", k, String(data[k]), String(v));

              // 온도 변화는 1도가 넘을때 저장.
              if (k === "air_temp" && Math.abs(Number(data.air_temp || 0) - Number(v || 0)) < 1) return;

              changed[k] = v;
            }
          });

          if (changed["key"] === undefined) {
            delete changed["key_change_time"];
          } else {
            // 키값 변경 시간
            changed["key_change_time"] = Number(changed["key"]) > 0 ? new Date() : null;

            if (Number(changed["key"]) === 3 || Number(changed["key"]) === 6) {
              changed["clean_change_time"] = Number(changed["key"]) === 3 ? new Date() : null; // 청소 완료 시 초기화.
            }

            if (changed["main_relay"] === undefined) {
              changed["main_relay"] = Number(changed["key"]) > 0 && Number(changed["key"]) < 4 ? 1 : 0; // 메인 릴레이 0:OFF, 1:ON (전등/전열)
            }
          }

          if (changed["clean"] !== undefined) {
            changed["clean_change_time"] = Number(changed["clean"]) === 1 ? new Date() : null; // 청소 완료 시 초기화.
          }

          console.log("- changed state ", changed, _.isEmpty(changed));

          // 현재와 다른 값이 있다면 업데이트
          if (!_.isEmpty(changed)) {
            changed.channel = req.headers.channel || channel || "web";
            changed.user_id = changed.channel === "device" ? name : id;

            console.log("- changed state update", changed);

            RoomState.updateRoomState(room_id, changed, req, pms, (err, info) => {
              if (!err) {
                const { channel, uuid, token } = req.headers;

                changed.room_id = Number(room_id);

                const json = {
                  type: "SET_ROOM_STATE", // 웹 reducer type, 어드민/모바일 store 타입은 socket server 에서 각각 변경.
                  headers: {
                    method: "put",
                    url: "room/state/" + room_id,
                    token,
                    channel: "api",
                    uuid,
                    req_channel: channel,
                  },
                  body: {
                    room_state: changed,
                    place_id,
                  },
                };

                // 소켓 서버로 전송.
                global.dispatcher.dispatch(json);

                // 환경 설정에 따른 객실 상태 변경.
                if (changed.key !== undefined || changed.door !== undefined) {
                  update_state_by_preferences(preferences, room_id, changed, false, req);
                }

                if (changed.sale === 0) {
                  // 객실 상태가 공실 인데 체크아웃 안된 객실 매출 체크아웃 시킨다.
                  RoomSale.updateCheckOut(room_id, (err, row) => {});
                }
              }

              jsonRes(req, res, err, { info }, "RoomState successfully updated");
            });

            // 마지막 이벤트 시간 기록.
            Place.updatePlace(place_id, { last_event_date: new Date() }, (err, info) => {});
          } else {
            jsonRes(req, res, err, { info: {} }, "no changed state");
          }
        });
      } else {
        jsonRes(req, res, err, "No room state by room_id");
      }
    });
  }
};

/*
  clean = 0:없음, 1:있음 (청소요청)
  sale  = 0:업음 1:숙박 2:대실 3:장기
  key   = 0:없음, 1:고객키,2:마스터키 3:청소키, 4:고객키 제거, 5:마스터키 제거, 6:청소키 제거
  state = 0:공실, 1:입실, 2:외출, 3:청소대기, 4:청소중, 5:청소완료, 6:퇴실 (상태 조건이 변경 될 때마다 체크 하여 반영 한다)
  isDelayed: 손님키 제거시 일정 시간 딜레이 후 제거 되었다면 처리.
*/
const update_state_by_preferences = (preferences, room_id, room_state, isDelayed, req) => {
  room_id = Number(room_id); // 정수로 변환.

  console.log();
  console.log("-------------------------------------");
  console.log("--- 키 상태 변경에 따른 처리 프로세스 ", room_state.key, isDelayed);
  console.log("-------------------------------------");

  RoomState.selectRoomStateAndFee(room_id, (err, row) => {
    if (!err && row[0]) {
      const roomState = row[0];

      // 키값 온도.
      let { temp_key_1, temp_key_2, temp_key_3, temp_key_4, temp_key_5, inspect } = roomState;

      let token = "";

      // 토큰 생성.
      sign({ channel: "api", place_id: roomState.place_id }, (_err, _token) => {
        // console.log("-- token", _token);
        token = _token;
      });

      const {
        place_id,
        pms,
        inspect_use,
        use_rent_begin,
        use_rent_end,
        stay_time_weekend,
        stay_time,
        rent_time_am_weekend,
        rent_time_am,
        rent_time_pm_weekend,
        rent_time_pm,
        auto_check_in,
        master_key_out_type,
        clean_key_out_type,
        clean_key_out_type_force,
        stay_time_type,
        no_sale_user_key_in_power_off_time,
        clean_key_in_power_off_time,
        user_key_in_check_out_power_off_time,
        user_key_out_stay,
        user_key_out_rent,
        auto_check_out_key_out,
        user_key_out_delay_time,
        auto_check_out_door_open,
      } = preferences;

      const day = moment().isoWeekday();
      const hour = moment().format("HH");
      let hhmm = moment().format("HHmm");

      // 숙박/대실 시간대 (대실 가능 시간 이외는 숙발으로 처리)
      const auto_check_in_type = Number(hour) >= Number(use_rent_begin) && Number(hour) < Number(use_rent_end) ? 2 : 1;

      // console.log("---> hour ", hour, " use_rent_begin hour ", hour, use_rent_begin, " use_rent_end", use_rent_end);

      // 이용 시간.
      let useTime = 0;

      if (auto_check_in_type !== 2) {
        // 기본 숙박 시간
        useTime =
          day > 5 // 주말
            ? stay_time_weekend
            : stay_time;
      } else {
        // 기본 대실 시간
        useTime =
          Number(hour) < 12 // 오전
            ? day > 5 // 주말
              ? rent_time_am_weekend
              : rent_time_am
            : day > 5
            ? rent_time_pm_weekend
            : rent_time_pm;
      }

      // console.log("---> useTime ", useTime);

      // console.log("---> roomState.sale ", roomState.sale, " key", roomState.key);
      // console.log("---> auto_check_in ", auto_check_in);
      // console.log("---> clean_key_out_type ", clean_key_out_type);

      // 자동 퇴실 여부.
      let auto_check_out_flag = false;
      // 자동 외출 여부.
      let auto_outing_flag = false;
      // 자동 퇴실 및 공실 처리(청소키 제거 시)
      let check_out_and_empty = false;

      // 체크아웃 시간 지났는지 여부.
      let isOverTime = roomState.check_in ? moment(roomState.check_out_exp) < moment() : false;

      // console.log("---> isOverTime ", isOverTime, roomState.check_in);
      console.log("---> isDelayed ", isDelayed, "key", room_state.key);

      // 손님키 삽입 시
      if (!isDelayed && room_state.key === 1) {
        // 공실 상태에서만.
        if (roomState.sale === 0) {
          // 손님키 삽입 시 자동 입실  (0: 사용안함, 1:대실만 사용, 2:숙박만 사용, 3:대실/숙박 사용)
          // 현재 대실/숙박 가능 시간과 맞을때만 자동 입실.
          if ((auto_check_in === 1 && auto_check_in_type === 2) || (auto_check_in === 2 && auto_check_in_type === 1) || auto_check_in === 3) {
            console.log("---> 손님키 삽입 시 자동 입실 처리 시작 ");

            // 매출 중복 판매 여부 검사.
            RoomSale.selectIsRoomSaleNow(room_id, (err, row) => {
              let { count = 0 } = row[0] || {};

              // 중복 시.
              if (count) {
                console.log("---> 중복 매출 방지! ");
              } else {
                const day = moment().isoWeekday();

                // 오늘 요금 조회, 적용 채널 (0: 카운터, 1:자판기, 2:예약)
                RoomFee.selectRoomDayFees(roomState.room_type_id, 0, day, (err, room_fees) => {
                  let new_sale = {};
                  new_sale.room_id = room_id;
                  new_sale.check_in = new Date();
                  new_sale.stay_type = auto_check_in_type;

                  // 객실 기본 요금
                  new_sale.default_fee = new_sale.stay_type === 2 ? roomState.default_fee_rent : roomState.default_fee_stay;

                  let hhmm = moment().format("HHmm");

                  // 요일 / 시간 요금.
                  let fee = undefined;

                  // 해당 요일의 해당 시간대의 설정 요금 조회.
                  room_fees.forEach(function (v) {
                    if (Number(v.stay_type) === Number(auto_check_in_type)) {
                      let st = Number(v.begin);
                      let et = Number(v.end);
                      let nt = Number(hhmm);

                      // 이전일 ~ 다음일 시간 이라면.
                      if (st > et) {
                        if (st <= nt || et > nt) {
                          fee = v;
                        }
                      } else {
                        if (st <= nt && et > nt) {
                          fee = v;
                        }
                      }
                    }
                  });

                  // console.log("---> roomFee fee:", fee);

                  // 현재 요일 시간의 추가 요금 적용.
                  if (fee) new_sale.default_fee += fee.add_fee;

                  SeasonPremium.selectAllSeasonPremiums(place_id, (err, all_season_premiums) => {
                    let usePeriod = 1; // 숙박 기간.

                    // 시즌 프리미엄 요금 적용 여부.
                    const season = _.filter(all_season_premiums, (v, k) => {
                      // console.log("- season", v);
                      let isSeanse = false;
                      for (let i = 0; i <= usePeriod; i++) {
                        const mmdd = moment(new_sale.check_in).add(i, "day").format("MMDD");

                        // console.log("- day", mmdd, v.begin, v.end);

                        // 기간이 다음 년도 포함 시.
                        if (Number(v.begin) > Number(v.end)) {
                          isSeanse = (Number(mmdd) <= Number("1231") && Number(mmdd) >= Number(v.begin)) || (Number(mmdd) >= Number("0101") && Number(mmdd) <= Number(v.end)); // 시즌에 속하는지 여부.
                        } else {
                          isSeanse = Number(v.begin) <= Number(mmdd) && Number(v.end) >= Number(mmdd); // 시즌에 속하는지 여부.
                        }

                        if (isSeanse) break;
                      }
                      //  console.log("- isSeanse", isSeanse);
                      return isSeanse;
                    })[0];

                    //console.log("- roomFee season:", season);

                    // 시즌 추가 요금
                    if (season && season.premium) {
                      new_sale.default_fee += season.premium;
                      // console.log("- season premium ", season.premium);
                    }

                    // 자동 입실 시 현금으로 결제
                    new_sale.pay_cash_amt = new_sale.default_fee;

                    // 입실 시간 기준 일때만 성수기/비성수기 이용 시간 추가.
                    if (stay_time_type === 0 && season && season.add_stay_time !== 0) {
                      useTime += season.add_stay_time;
                      // console.log("- season add_stay_time ", season.add_stay_time);
                    }

                    if (season && season.add_rent_time !== 0) {
                      useTime += season.add_rent_time;
                      // console.log("- season add_rent_time ", season.add_rent_time);
                    }

                    // 숙박 시간 타입 (0: 이용 시간 기준, 1:퇴실 시간 기준)
                    if (new_sale.stay_type === 1 && stay_time_type === 1) {
                      new_sale.check_out_exp = moment()
                        .hour(useTime)
                        .minute(0)
                        .add(useTime > hour ? 0 : 1, "day")
                        .format("YYYY-MM-DD HH:mm"); // 현재 시간이 퇴실 시간 보다 크다면 1일 추가
                    } else {
                      new_sale.check_out_exp = moment().add(useTime, "hour").format("YYYY-MM-DD HH:mm");
                    }

                    new_sale.user_id = "auto";
                    new_sale.channel = "api";
                    new_sale.state = "A";

                    RoomSale.insertRoomSale(new_sale, req, (err, info) => {
                      console.log("---> 손님키 삽입 시 자동 입실 매출 등록 ");

                      const { channel, uuid } = req ? req.headers : {};

                      if (!err && info.insertId) {
                        new_sale.id = info.insertId;
                        new_sale.reg_date = new Date();

                        const {
                          channel: _channel,
                          user_id = "",
                          phone = "",
                          stay_type,
                          serialno,
                          default_fee,
                          add_fee = 0,
                          pay_card_amt = 0,
                          pay_cash_amt = 0,
                          pay_point_amt = 0,
                          prepay_ota_amt = 0,
                          prepay_card_amt = 0,
                          prepay_cash_amt = 0,
                          prepay_point_amt = 0,
                          save_point = 0,
                          card_approval_num,
                          card_merchant,
                          card_no,
                        } = new_sale;

                        let new_room_sale_pay = {
                          channel: _channel || channel,
                          first_pay: 1, // 매출의 첫 결재.
                          sale_id: info.insertId,
                          user_id,
                          phone,
                          stay_type,
                          serialno,
                          default_fee,
                          add_fee,
                          pay_card_amt,
                          pay_cash_amt,
                          pay_point_amt,
                          prepay_ota_amt,
                          prepay_card_amt,
                          prepay_cash_amt,
                          prepay_point_amt,
                          save_point,
                          card_approval_num,
                          card_merchant,
                          card_no,
                          fee_total: Number(default_fee) + Number(add_fee),
                        };

                        // console.log("- new_room_sale_pay", new_room_sale_pay);

                        RoomSalePay.insertRoomSalePay(new_room_sale_pay, (err, info) => {
                          const json = {
                            type: "SET_ROOM_SALE", // 웹 reducer type, 어드민/모바일 store 타입은 socket server 에서 각각 변경.
                            headers: {
                              method: "post",
                              url: "room/sale/" + info.insertId,
                              token,
                              channel: "api",
                              uuid,
                              req_channel: channel,
                            },
                            body: {
                              room_sale: new_sale,
                              place_id: roomState.place_id,
                            },
                          };

                          // 소켓 서버로 전송.
                          global.dispatcher.dispatch(json);
                        });

                        let new_state = { channel: "api", user_id: "auto" };

                        new_state.sale = auto_check_in_type; // 자동 입실 시 숙박 형태 1:숙박 2:대실 3:장기
                        new_state.room_sale_id = info.insertId;

                        if (roomState.clean) new_state.clean = 0;
                        if (roomState.outing) new_state.outing = 0;
                        new_state.air_set_temp = temp_key_1; // 사용중 온도.

                        RoomState.updateRoomState(room_id, new_state, req, pms, (err, info) => {
                          console.log("---> 손님키 삽입 시 자동 입실 처리 완료 ");

                          new_state.room_id = room_id;

                          const { channel, uuid } = req ? req.headers : {};

                          const json = {
                            type: "SET_ROOM_STATE", // 웹 reducer type, 어드민/모바일 store 타입은 socket server 에서 각각 변경.
                            headers: {
                              method: "put",
                              url: "room/state/" + room_id,
                              token,
                              channel: "api",
                              uuid,
                              req_channel: channel,
                            },
                            body: {
                              room_state: new_state,
                              place_id: roomState.place_id,
                            },
                          };

                          // 소켓 서버로 전송.
                          global.dispatcher.dispatch(json);
                        });
                      }
                    });
                  });
                });
              }
            });
          } else {
            console.log("---> 고객키 삽입 ");
          }
        } else {
          // 외출 상태에서 고객키가 IN 되면 외출 종료.
          if (roomState.sale > 0 && !roomState.check_out && roomState.outing === 1) {
            let new_state = { channel: "api", user_id: "auto" };

            new_state.outing = 0;

            if (roomState.clean) new_state.clean = 0; // 청소 중 이라면 청소 완료.

            new_state.air_set_temp = temp_key_1; // 사용중 온도.
            new_state.inspect = 0; // 인스펙트 초기화.

            RoomState.updateRoomState(room_id, new_state, req, pms, (err, info) => {
              console.log("---> 외출 상태에서 고객키가 삽입 되면 외출 종료됨 ");

              new_state.room_id = room_id;
              delete new_state.mod_date;

              const { channel, uuid } = req ? req.headers : {};

              const json = {
                type: "SET_ROOM_STATE", // 웹 reducer type, 어드민/모바일 store 타입은 socket server 에서 각각 변경.
                headers: {
                  method: "put",
                  url: "room/state/" + room_id,
                  token,
                  channel: "api",
                  uuid,
                  req_channel: channel,
                },
                body: {
                  room_state: new_state,
                  place_id: roomState.place_id,
                },
              };

              // 소켓 서버로 전송.
              global.dispatcher.dispatch(json);
            });
          }
        }
      }
      // 손님키 제거 시
      else if (room_state.key === 4 || isDelayed) {
        // 체크인 상태
        if (roomState.check_in && !roomState.check_out) {
          // 손님키 제거시 설정(0:외출, 1:퇴실)
          let key_out_auto_check_out = roomState.sale === 2 ? user_key_out_rent === 1 : user_key_out_stay === 1;
          let key_out_auto_outing = roomState.sale === 2 ? user_key_out_rent === 0 : user_key_out_stay === 0;

          console.log("---> 손님키 제거 시 자동 퇴실/외출 여부  ", {
            key_out_auto_check_out,
            key_out_auto_outing,
          });

          // 손님키 제거 시 퇴실 설정.
          if (key_out_auto_check_out) {
            // 지연 시간 설정 시(퇴실만 해당)
            if (!isDelayed && user_key_out_delay_time) {
              console.log("---> 손님키 제거 시 자동 퇴실 지연 시간 시작 ", user_key_out_delay_time, "분", moment().format("YYYYMMDD HH:mm"));

              // new Delay(preferences, room_id, roomState, req, Number(user_key_out_delay_time) * 60 * 1000);

              _.delay(
                (preferences, room_id, roomState, req) => {
                  console.log("---> 손님키 제거 시 지연 시간 종료 update_state_by_preferences call!", room_id);

                  // 현재 상태 조회.
                  RoomState.selectRoomState(room_id, (err, row) => {
                    if (!err) {
                      let state = row[0];

                      let { sale, key, check_out, room_sale_id } = state;

                      console.log("---> 현재 객실 상태 ", {
                        sale,
                        key,
                        check_out,
                        room_sale_id,
                      });

                      // 입실 상태이고 손님키가 계속 없다면.
                      if (
                        sale > 0 &&
                        !check_out &&
                        key !== 1 &&
                        roomState.room_sale_id === room_sale_id // 동일 매출 일때.
                      ) {
                        update_state_by_preferences(preferences, room_id, state, true, req);
                      } else {
                        console.log("---> 현재 상태 변경으로 자동 퇴실 취소");
                      }
                    }
                  });
                },
                Number(user_key_out_delay_time) * 60 * 1000,
                room_id,
                preferences,
                roomState,
                req
              );
            } else {
              // 현재 손님키 제거 상태라면 처리.
              if (roomState.key !== 1 && roomState.sale > 0 && !roomState.check_out) {
                // 손님키 제거시 퇴실.
                if (key_out_auto_check_out) auto_check_out_flag = true;

                // 사용 시간 경과 후 손님키 제거 시 자동 퇴실처리
                if (isOverTime && auto_check_out_key_out) auto_check_out_flag = true;

                console.log("---> 손님키 제거 시 자동 퇴실 여부  ", { auto_check_out_flag, auto_check_out_key_out });
              }
            }
          }
          // 손님키 제거 시 외출 설정.
          else if (key_out_auto_outing) {
            auto_outing_flag = key_out_auto_outing && !roomState.outing;

            // 청소키 제거 시 무조건 공실 처리 시 외출 안함.
            if (roomState.key === 6 && clean_key_out_type_force) auto_outing_flag = false;

            console.log("---> 손님키 제거 시 자동 외출 여부 ", { auto_outing_flag, clean_key_out_type_force, key_out_auto_outing });
          }
        }
      }
      // 청소키 삽입 시
      else if (!isDelayed && room_state.key === 3) {
        console.log("---> 청소키 삽입 ");
      }
      // 마스터키 / 청소키 제거 시
      else if (!isDelayed && (room_state.key === 5 || room_state.key === 6)) {
        // 마스터키 제거 시
        if (room_state.key === 5) {
          // 퇴실처리 된 상태 .
          if (roomState.check_out) {
            // 객실 인스펙트 사용 여부 (0: 사용 안함, 1: 퇴실 상태만 사용, 2: 항상 사용)
            if (inspect_use > 0) {
              //  master_key_out_type : 마스터키 제거 시 공실로 변경 여부 (0: 사용안함, 1:대실, 2:숙박, 3: 대실 + 숙박)
              if ((roomState.sale === 1 && master_key_out_type === 2) || (roomState.sale === 2 && master_key_out_type === 1) || (roomState.sale > 0 && master_key_out_type === 3)) {
                check_out_and_empty = true;
                console.log("---> 마스터키 제거 시 공실 처리 ");
              }
            }
          }
        }
        // 청소키 제거 시
        else if (room_state.key === 6) {
          // 퇴실처리 된 상태 or 무조건 공실 처리 시.
          if (clean_key_out_type && (roomState.check_out || clean_key_out_type_force)) {
            // 인스펙트 사용 안함
            if (inspect_use === 0) {
              //  clean_key_out_type : 청소키 제거 시 공실로 변경 여부 (0: 사용안함, 1:대실, 2:숙박, 3: 대실 + 숙박)
              if ((roomState.sale === 1 && clean_key_out_type === 2) || (roomState.sale === 2 && clean_key_out_type === 1) || (roomState.sale > 0 && clean_key_out_type === 3)) {
                check_out_and_empty = true;
                console.log("---> 청소키 제거 시 공실 처리 ");
              }
            }
          }
          // 청소키 제거 시 무조건 공실 처리 시 외출 안함.
          if (clean_key_out_type_force) auto_outing_flag = false;
        }
      }

      // 문 열림 시
      if (!isDelayed && room_state.door === 1) {
        if (roomState.sale > 0 && !roomState.check_out) {
          // 사용 시간 경과 후 출입문이 열리면 자동 퇴실처리 (0: 사용안함,1:사용)
          if (isOverTime && auto_check_out_door_open) {
            auto_check_out_flag = true;
            console.log("---> 사용 시간 경과 후 출입문이 열리면 자동 퇴실처리 ");
          }
        }
      }

      console.log("---> 자동 퇴실 처리 여부 ", auto_check_out_flag);
      console.log("---> 자동 공실 처리 여부 ", check_out_and_empty);

      // 자동 퇴실 여부 0: 없음, 1:사용
      if (auto_check_out_flag || check_out_and_empty) {
        let new_state = { channel: "api", user_id: "auto" };

        // 퇴실 처리.
        if (auto_check_out_flag) {
          if (room_state.key !== 6 && roomState.key != 3 && !roomState.clean) new_state.clean = 1; // 청소 요청.
          new_state.air_set_temp = temp_key_5; // 청소대기 온도.
        }

        // 공실 처리.
        if (check_out_and_empty || roomState.sale === 0) {
          if (roomState.sale) new_state.sale = 0; // 공실 처리
          if (roomState.key) new_state.key = 0; // 키없음.
          if (roomState.room_sale_id) new_state.room_sale_id = null; // 매출 초기화.
          if (roomState.clean) new_state.clean = 0; // 청소 중 이라면 청소 완료.
          new_state.air_set_temp = temp_key_3; // 공실 온도.
        }

        if (roomState.key !== 2 || roomState.key !== 3) new_state.main_relay = 0; // 데몬에서 main_relay = 1 을 안올려 줬을경우가 있어서 무조건 0 으로 보낸다.

        RoomState.updateRoomState(room_id, new_state, req, pms, (err, info) => {
          console.log("---> 자동 퇴실 설정으로 " + (!check_out_and_empty ? "퇴실 처리 하였음" : "공실 처리 하였음"));

          new_state.room_id = room_id;

          delete new_state.mod_date;

          const { channel, uuid } = req ? req.headers : {};

          const json = {
            type: "SET_ROOM_STATE", // 웹 reducer type, 어드민/모바일 store 타입은 socket server 에서 각각 변경.
            headers: {
              method: "put",
              url: "room/state/" + room_id,
              token,
              channel: "api",
              uuid,
              req_channel: channel,
            },
            body: {
              room_state: new_state,
              place_id: roomState.place_id,
            },
          };

          // 소켓 서버로 전송.
          global.dispatcher.dispatch(json);
        });

        // 매출 퇴실 처리.
        if (roomState.room_sale_id && !roomState.check_out) {
          let new_sale = {};

          new_sale.room_id = room_id;
          new_sale.state = "C"; // 판매 상태 종료.
          new_sale.check_out = new Date(); // 체크 아웃.
          new_sale.user_id = "auto";
          new_sale.channel = "api";

          RoomSale.updateRoomSale(roomState.room_sale_id, new_sale, req, (err, info) => {
            console.log("---> 자동 퇴실 설정으로 판매 check_out 하였음");

            new_sale.id = roomState.room_sale_id;

            delete new_state.mod_date;

            const { channel, uuid } = req ? req.headers : {};

            const json = {
              type: "SET_ROOM_SALE", // 웹 reducer type, 어드민/모바일 store 타입은 socket server 에서 각각 변경.
              headers: {
                method: "put",
                url: "room/sale/" + roomState.room_sale_id,
                token,
                channel: "api",
                uuid,
                req_channel: channel,
              },
              body: {
                room_sale: new_sale,
                place_id: roomState.place_id,
              },
            };

            // 소켓 서버로 전송.
            global.dispatcher.dispatch(json);
          });
        }
      }
      // 외출 처리
      else if (auto_outing_flag) {
        let new_state = { channel: "api", user_id: "auto" };

        new_state.outing = 1;
        new_state.air_set_temp = inspect ? temp_key_5 : temp_key_2; // 외출중 온도.

        if (roomState.key !== 2 || roomState.key !== 3) new_state.main_relay = 0; // 데몬에서 main_relay = 1 을 안올려 줬을경우가 있어서 무조건 0 으로 보낸다.

        RoomState.updateRoomState(room_id, new_state, req, pms, (err, info) => {
          console.log("---> 입실 상태에서 손님키 제거 시 외출로 변경됨 ");

          new_state.room_id = room_id;
          delete new_state.mod_date;

          const { channel, uuid } = req ? req.headers : {};

          const json = {
            type: "SET_ROOM_STATE", // 웹 reducer type, 어드민/모바일 store 타입은 socket server 에서 각각 변경.
            headers: {
              method: "put",
              url: "room/state/" + room_id,
              token,
              channel: "api",
              uuid,
              req_channel: channel,
            },
            body: {
              room_state: new_state,
              place_id: roomState.place_id,
            },
          };

          // 소켓 서버로 전송.
          global.dispatcher.dispatch(json);
        });
      }
    }
  });

  return room_state;
};

const delete_a_room_state = (req, res) => {
  if (!req.params.room_id || !req.params.type) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide room state id for delete");
  } else {
    // FOREIGN KEY Constraints ON DELETE CASCADE ON UPDATE CASCADE
    RoomState.deleteRoomState(req.params.room_id, req.params.type, (err, info) => {
      jsonRes(req, res, err, { info }, "RoomState successfully deleted");
    });
  }
};

class Delay {
  constructor(preferences, room_id, room_state, req, time) {
    this.preferences = preferences;
    this.room_id = room_id;
    this.room_state = room_state;
    this.req = req;
    this.time = time;

    this.delayTimer = null;

    this.timer();
  }

  timer() {
    console.log("- Delay timer", this.time);

    // 손님키 제거 후 일정 시간 딜레이 준다.
    this.delayTimer = setTimeout(() => {
      console.log("");
      console.log("---> 손님키 제거 시 지연 시간 종료 update_state_by_preferences call!", this.room_id);

      // 현재 상태 조회.
      RoomState.selectRoomState(this.room_id, (err, row) => {
        if (!err) {
          let state = row[0];

          let { sale, key, check_out, room_sale_id } = state;

          console.log("---> 현재 객실 상태 ", {
            sale,
            key,
            check_out,
            room_sale_id,
          });

          // 입실 상태이고 손님키가 계속 없다면.
          if (
            sale > 0 &&
            !check_out &&
            key !== 1 &&
            this.room_state.room_sale_id === room_sale_id // 동일 매출 일때.
          ) {
            update_state_by_preferences(this.preferences, this.room_id, state, true, this.req);
          } else {
            console.log("---> 현재 상태 변경으로 자동 퇴실 취소");
          }
        }
      });
    }, this.time);
  }
}

// 10 초마다 주기적으로 객실 상태 체크 및 자동 설정.
setInterval(() => {
  // 판매 되었지만 room_sate 에 room_sale_id 가 없는 객실 (서버 오류/재시작 등으로 판매 후 객실 상태 업데이트 안된것)
  RoomState.selectSaleDup((err, row) => {
    if (!err && row[0]) {
      _.each(row, (item) => {
        console.log("---> 중복 판매 금지 객실 ", item);
        let { id: room_sale_id, place_id, room_id, user_id = "", channel = "web", serialno, stay_type, state_id } = item;

        if (room_sale_id && place_id && room_id && stay_type && state_id) {
          let room_state = {};
          room_state.sale = stay_type;
          room_state.room_sale_id = room_sale_id;
          room_state.channel = channel;
          room_state.user_id = user_id; // state log 용.

          let req = {
            headers: { channel },
            auth: { id: user_id, name: user_id, place_id },
          };
          let pms = null;

          RoomState.updateRoomState(room_id, room_state, req, pms, (err, info) => {
            console.log("---> 중복 판매 금지 객실 업데이트 완료", room_state);

            room_state.id = state_id;
            room_state.room_id = room_id;
            delete room_state.mod_date;

            // 토큰 생성.
            sign({ channel: "api", place_id }, (err, token) => {
              if (!err && token) {
                const json = {
                  type: "SET_ROOM_STATE", // 웹 reducer type, 어드민/모바일 store 타입은 socket server 에서 각각 변경.
                  headers: {
                    method: "put",
                    url: "room/state/" + room_id,
                    token,
                    channel: "api",
                    uuid: "",
                    req_channel: channel,
                  },
                  body: {
                    room_state,
                    place_id,
                  },
                };

                // 소켓 서버로 전송.
                global.dispatcher.dispatch(json);
              }
            });
          });
        }
      });
    }
  });

  // TODO 업소별로 객실 상태 조회 -> 전체 객실 조회(filter 걸어서 필요한 객실 상태만 조회) 후 체크로 변경 해야함!!
  PREFERENCES.List((list) => {
    console.log("---> 객실 상태 체크 루프 ", list.length);

    // 객실 상태 체크.
    _.each(list, (preferences) => {
      let { place_id, pms, auto_check_in, no_sale_user_key_in_power_off_time, user_key_in_check_out_power_off_time, clean_key_in_power_off_time, inspect_use, inspect_done_show_time } = preferences;

      // console.log("- 객실 체크 ", { place_id, no_sale_user_key_in_power_off_time, user_key_in_check_out_power_off_time, clean_key_in_power_off_time, inspect_done_show_time });

      // 업소별 객실 상태 조회.
      RoomState.selectAllRoomStatesSale(place_id, (err, room_states) => {
        if (!err) {
          _.each(room_states, (room_state) => {
            const { place_id, room_id, name, use_auto_power_off, sale, state, main_relay, key, key_change_time, sale_channel, stay_type, check_in, check_out, inspect, mod_date } = room_state;

            if (!use_auto_power_off) {
              // console.log("- room_state ", {
              //   name,
              //   room_id,
              //   use_auto_power_off,
              //   main_relay,
              //   state,
              //   key,
              //   no_sale_user_key_in_power_off_time,
              //   user_key_in_check_out_power_off_time,
              //   clean_key_in_power_off_time,
              // });
            }

            // 자동 전원 차단 사용 여부(0:안함, 1:사용) (사용 연장 시 고객키가 있는 상태에서 전원 차단을 하지 않기위해)
            if (use_auto_power_off) {
              // 비정상 입실 시 전원차단 타이머
              if (no_sale_user_key_in_power_off_time) {
                if (main_relay === 1 && sale === 0 && key === 1) {
                  let isTimeOver = moment().isSameOrAfter(moment(key_change_time || mod_date).add(no_sale_user_key_in_power_off_time, "minute"));

                  // console.log(place_id, name, "손님키 삽입시간:", moment(key_change_time || mod_date).format("YYYY-MM-DD HH:mm"), " 현재시간:", moment().format("YYYY-MM-DD HH:mm"), {
                  //   no_sale_user_key_in_power_off_time,
                  //   isTimeOver,
                  // });

                  // 전원 공급 상태고 판매 상태가 아니고 손님 키가 변함없이 있다면 전원 차단
                  if (isTimeOver) {
                    console.log("---> 손님키 삽입 시 자동 체크인 아닐때 ", name, no_sale_user_key_in_power_off_time, " 분 후 전원 차단.");

                    timerUpdateState(place_id, room_id, pms, { main_relay: 0 });
                  }
                }
              }

              // 고객카드 삽입 상태 퇴실 시 전원차단 타이머 (0 이면 즉시 차단)
              if (main_relay === 1 && sale !== 0 && state !== "A" && key === 1) {
                let isTimeOver = moment().isSameOrAfter(moment(key_change_time || mod_date).add(user_key_in_check_out_power_off_time, "minute"));

                // console.log(place_id, name, "손님키 삽입시간:", moment(key_change_time || mod_date).format("YYYY-MM-DD HH:mm"), " 현재시간:", moment().format("YYYY-MM-DD HH:mm"), {
                //   user_key_in_check_out_power_off_time,
                //   isTimeOver,
                // });

                // 전원 공급 상태고 판매 상태가 아니고 손님 키가 변함없이 있다면 전원 차단
                if (isTimeOver) {
                  console.log("---> 손님키 삽입 시 자동 체크인 아닐때 ", name, user_key_in_check_out_power_off_time, " 분 후 전원 차단.");

                  timerUpdateState(place_id, room_id, pms, { main_relay: 0 });
                }
              }

              //청소 시 전원차단 타이머
              if (clean_key_in_power_off_time) {
                if (main_relay === 1 && key === 3) {
                  let isTimeOver = moment().isSameOrAfter(moment(key_change_time || mod_date).add(clean_key_in_power_off_time, "minute"));

                  // console.log(place_id, name, "청소키 삽입시간:", moment(key_change_time || mod_date).format("YYYY-MM-DD HH:mm"), " 현재시간:", moment().format("YYYY-MM-DD HH:mm"), {
                  //   clean_key_in_power_off_time,
                  //   isTimeOver,
                  // });

                  // 전원 공급 상태고 판매 상태가 아니고 청소 키가 변함없이 있다면 전원 차단
                  if (isTimeOver) {
                    console.log("---> 청소키 삽입 시 체크인 아닐때 ", name, clean_key_in_power_off_time, " 분 후 전원 차단.");

                    timerUpdateState(place_id, room_id, pms, { main_relay: 0 });
                  }
                }
              }

              // 점검완료 표시 시간 타이머
              if (inspect_use && inspect_done_show_time) {
                if (inspect === 3) {
                  let isTimeOver = moment().isSameOrAfter(moment(key_change_time || mod_date).add(inspect_done_show_time, "minute"));

                  // console.log(place_id, name, "점검완료 표시 시간:", moment(key_change_time || mod_date).format("YYYY-MM-DD HH:mm"), " 현재시간:", moment().format("YYYY-MM-DD HH:mm"), {
                  //   inspect_done_show_time,
                  //   isTimeOver,
                  // });

                  // 전원 공급 상태고 판매 상태가 아니고 청소 키가 변함없이 있다면 전원 차단
                  if (isTimeOver) {
                    console.log("---> 점검완료 표시 ", name, inspect_done_show_time, " 분 후 종료.");

                    timerUpdateState(place_id, room_id, pms, { inspect: 0 });
                  }
                }
              }
            }
          });
        }
      });
    });
  });
}, 10 * 1000);

const timerUpdateState = (place_id, room_id, pms, data) => {
  let room_state = {
    channel: "api",
    user_id: "auto",
  };

  room_state = _.merge(room_state, data);

  let req = {
    headers: { channel: "api", uuid: "" },
    auth: { place_id },
  };

  RoomState.updateRoomState(room_id, room_state, req, pms, (err, info) => {
    console.log("---> 타이머 업데이트 완료!", room_state);

    // 토큰 생성.
    sign({ channel: "api", place_id }, (err, token) => {
      if (token) {
        const json = {
          type: "SET_ROOM_STATE", // 웹 reducer type, 어드민/모바일 store 타입은 socket server 에서 각각 변경.
          headers: {
            method: "put",
            url: "room/state/" + room_id,
            token,
            channel: "api",
            uuid: "",
            req_channel: "",
          },
          body: {
            room_state,
            place_id,
          },
        };

        // 소켓 서버로 전송.
        global.dispatcher.dispatch(json);
      }
    });
  });
};

export default {
  list_all_room_state,
  list_all_room_state_sale,
  list_a_room_states,
  create_a_room_state,
  read_a_room_state,
  read_a_room_state_sale,
  update_signal_room_state,
  update_all_room_state,
  update_a_room_state,
  delete_a_room_state,
  read_a_room_state_by_name,
};
