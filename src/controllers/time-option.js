import TimeOption from "model/time-option";
import File from "model/file";
import { jsonRes, sendRes, isValidToken, checkPermission } from "utils/api-util";
import { ERROR } from "constants/constants";

const list_a_time_options = (req, res) => {
  let { place_id = 0, filter = "1=1", limit = "10000", order = "a.reg_date", desc = "desc" } = req.params;

  if (place_id) filter = "a.place_id=" + place_id;

  TimeOption.selectTimeOptionCount(filter, (err, count) => {
    if (err) {
      jsonRes(req, res, err, { count });
    } else
      TimeOption.selectTimeOptions(filter, order, desc, limit, (err, time_options) => {
        jsonRes(req, res, err, { count, time_options });
      });
  });
};

const read_a_time_option = (req, res) => {
  const { id } = req.params;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide time_option id for select");
  } else {
    TimeOption.selectTimeOption(id, (err, time_option) => {
      jsonRes(req, res, err, { time_option });
    });
  }
};

const create_a_time_option = (req, res) => {
  const { time_option } = req.body;

  TimeOption.insertTimeOption(time_option, (err, info) => {
    if (info && info.insertId) req.body.time_option.id = info.insertId; // 채널 전송 시 id 입력.

    jsonRes(req, res, err, { info }, "TimeOption successfully inserted");
  });
};

const update_a_time_option = (req, res) => {
  const { id } = req.params;
  const { time_option } = req.body;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide time_option id for update");
  } else {
    TimeOption.updateTimeOption(id, time_option, (err, info) => {
      jsonRes(req, res, err, { info }, "TimeOption successfully updated");
    });
  }
};

const delete_a_time_option = (req, res) => {
  const { id } = req.params;
  const { time_option } = req.body;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide time_option id for delete");
  } else {
    TimeOption.deleteTimeOption(id, (err, info) => {
      jsonRes(req, res, err, { info }, "TimeOption successfully deleted");
    });
  }
};

export default {
  list_a_time_options,
  create_a_time_option,
  read_a_time_option,
  update_a_time_option,
  delete_a_time_option,
};
