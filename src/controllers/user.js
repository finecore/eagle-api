import User from "model/user";
import { jsonRes, sendRes, isValidToken, checkPermission } from "utils/api-util";
import { ERROR } from "constants/constants";
import crypto from "crypto";

const list_a_users = (req, res) => {
  let { filter = "1=1", limit = "10000", order = "a.level", desc = "asc", place_id = 0 } = req.params;

  if (place_id) filter = "a.place_id=" + place_id;

  console.log("-> filter", filter);

  User.selectUserCount(filter, (err, count) => {
    if (err) {
      jsonRes(req, res, err, { count });
    } else
      User.selectUsers(filter, order, desc, limit, (err, users) => {
        jsonRes(req, res, err, { count, users });
      });
  });
};

const read_a_user = (req, res) => {
  const { id } = req.params;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide user id for select");
  } else {
    User.selectUser(id, (err, user) => {
      jsonRes(req, res, err, { user });
    });
  }
};

const create_a_user = (req, res) => {
  const { user } = req.body; // post body.

  console.log("- create_a_user", user);

  if (!user.place_id || !user.id || !user.pwd || !user.name) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide user place_id ,id ,pwd ,name for insert");
  } else {
    // 해시 생성
    var shasum = crypto.createHash("sha1");
    shasum.update(user.pwd);

    // 비밀번호 sha1 형식으로 암호화.
    user.pwd = shasum.digest("hex");
    console.log("- pwd sha1 ", user.pwd);

    delete user.pwd_confirm;
    user.reg_date = new Date();

    User.selectUser(user.id, (err, sel_user) => {
      if (sel_user[0]) {
        jsonRes(req, res, ERROR.INVALID_USER_ID, user.id + "는 이미 사용 중인 아이디 입니다.");
      } else {
        User.insertUser(user, (err, info) => {
          jsonRes(req, res, err, { info, user }, "User successfully inserted");
        });
      }
    });
  }
};

const update_a_user = (req, res) => {
  const { id } = req.params;
  const { user } = req.body; // post body.

  if (!id || !user) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide user id or data for update");
  } else {
    if (user.pwd) {
      // 해시 생성
      var shasum = crypto.createHash("sha1");
      shasum.update(user.pwd);

      // 비밀번호 sha1 형식으로 암호화.
      user.pwd = shasum.digest("hex");
      console.log("- pwd sha1 ", user.pwd);
    } else {
      // 비밀번호 입력 안하면 해당 항목 제거.
      delete user.pwd;
    }

    delete user.pwd_confirm;

    if (user.id && user.id !== id) {
      User.selectUser(user.id, (err, sel_user) => {
        if (sel_user[0]) {
          jsonRes(req, res, ERROR.INVALID_USER_ID, user.id + "는 이미 사용 중인 아이디 입니다.");
        } else {
          User.updateUser(id, user, (err, info) => {
            jsonRes(req, res, err, { info }, "User successfully updated");
          });
        }
      });
    } else {
      User.updateUser(id, user, (err, info) => {
        jsonRes(req, res, err, { info }, "User successfully updated");
      });
    }
  }
};

const delete_a_user = (req, res) => {
  const { id } = req.params;

  if (!id) {
    jsonRes(req, res, ERROR.INVALID_ARGUMENT, "Please provide user id for delete");
  } else {
    // FOREIGN KEY Constraints table ON DELETE CASCADE ON UPDATE CASCADE
    User.deleteUser(id, (err, info) => {
      jsonRes(req, res, err, { info }, "User successfully deleted");
    });
  }
};

export default {
  list_a_users,
  create_a_user,
  read_a_user,
  update_a_user,
  delete_a_user,
};
