import RoomSaleReportHour from "model/room-sale-report-hour";
import RoomSaleReportDay from "model/room-sale-report-day";
import RoomSaleReportMonth from "model/room-sale-report-month";
import RoomSaleReportYear from "model/room-sale-report-year";
import PlaceSubscribe from "model/place-subscribe";
import RoomSale from "model/room-sale";
import RoomSalePay from "model/room-sale-pay";
import Mail from "model/mail";
import User from "model/user";

import { ERROR, PREFERENCES } from "constants/constants";
import { keyToValue } from "constants/key-map";

import makeExcel from "helper/excel-helper";

import axios from "axios";
import _ from "lodash";
import moment from "moment";
import chalk from "chalk";

import path from "path";
import fs from "fs";

import format from "utils/format-util";
import ExcelUtil from "utils/excel-util";
import { CheckSubscribe } from "utils/subscribe-util";

/**
 * RoomSale Report Class.
 */
class RoomSaleReport {
  constructor() {
    console.log(chalk.yellow("----------- RoomSaleReport Scheduler Create ----------"));
    this.timer = null;
    this.receiveMailUsers = [];
    this.otaSubscribePlace = [];

    setTimeout(
      () => {
        this.init();
      },
      process.env.REACT_APP_MODE === "LOCAL" ? 3000 : 30000
    );

    console.log("");
  }

  init() {
    this.users();
    this.subscribes();

    setTimeout(() => {
      // 초기 업데이트.
      if (process.env.REACT_APP_MODE !== "PROD") {
        this.update();

        setTimeout(() => {
          let now = moment().format("YYYY-MM-DD");

          // 입실 취소 시 리셋 한다.
          RoomSaleReportDay.deleteRoomSaleReportDay(now, (err, info) => {});
          RoomSaleReportMonth.deleteRoomSaleReportMonth(now, (err, info) => {});
          RoomSaleReportYear.deleteRoomSaleReportYear(now, (err, info) => {});

          setTimeout(() => {
            // 일간 데이터 업데이트.
            RoomSaleReportDay.insertRoomSaleReportDay(0, 0, (err, info) => {
              // 이번달 정보 업데이트.
              RoomSaleReportMonth.insertRoomSaleReportMonth(0, 0, (err, info) => {
                // 이번 년도 정보 업데이트.
                RoomSaleReportYear.insertRoomSaleReportYear(0, 0, (err, info) => {});
              });
            });
          }, 3000);
        }, 5000);
      }

      // 시작 시간 딜레이(서버 재 시작시 바로 실행 안함)
      setTimeout(() => {
        this.run();
      }, 60 * 1000);
    }, 1000);

    // 일일 매출 리포트
    // if (process.env.REACT_APP_MODE === "LOCAL") {
    //   setTimeout(() => {
    //     this.dailyReport();
    //   }, 1000);
    // }
  }

  run() {
    console.log("--------------------------------------------");
    console.log("---- RoomSaleReport Scheduler Run!");
    console.log("--------------------------------------------");

    if (this.timer) clearInterval(this.timer);

    let last = moment().add(-1, "hour");

    this.timer = setInterval(() => {
      // 매분 마다 실행.
      if (Number(moment().format("ss")) === 0) {
        console.log("------> RoomSaleReport 매 분 마다 Hour 로그 저장");

        this.users();
        this.subscribes();
        this.update();
      }

      // 매 시간 마다 실행.
      if (Number(moment().format("mmss")) === 0) {
        console.log("------> RoomSaleReport 매 시간 마다 Day 로그 저장");

        let now = moment().format("YYYY-MM-DD");

        // 입실 취소 시 리셋 한다.
        RoomSaleReportDay.deleteRoomSaleReportDay(now, (err, info) => {});
        RoomSaleReportMonth.deleteRoomSaleReportMonth(now, (err, info) => {});
        RoomSaleReportYear.deleteRoomSaleReportYear(now, (err, info) => {});

        setTimeout(() => {
          // 일간 데이터 업데이트.
          RoomSaleReportDay.insertRoomSaleReportDay(0, 0, (err, info) => {
            // 이번달 정보 업데이트.
            RoomSaleReportMonth.insertRoomSaleReportMonth(0, 0, (err, info) => {
              // 이번 년도 정보 업데이트.
              RoomSaleReportYear.insertRoomSaleReportYear(0, 0, (err, info) => {});
            });
          });
        }, 3000);

        // 일일 매출 리포트
        if (this.otaSubscribePlace.length) {
          this.dailyReport();
        }
      }

      // 1 일마다 저장 기간 지난 데이터 삭제.(매일 01시00분)
      if (Number(moment().format("hhmmss")) === 10000) {
        console.log("------> RoomSaleReport 1 일마다 저장 기간 지난 데이터 삭제.");
        this.delete();
      }
    }, 1000);
  }

  users() {
    // 업소별 메일 수신 직원 목록
    let filter = "email != '' AND ota_mo_mail_yn != '0'",
      limit = "10000",
      order = "a.id",
      desc = "asc";

    User.selectUsers(filter, order, desc, limit, (err, users) => {
      this.receiveMailUsers = users;
    });
  }

  subscribes() {
    // 일일 매출 메일링 서비스 구독 업소만 조회.
    let filter = "b.code = 20",
      limit = "10000",
      order = "a.id",
      desc = "asc";

    PlaceSubscribe.selectPlaceSubscribes(filter, order, desc, limit, (err, place_subscribes) => {
      this.otaSubscribePlace = [];

      // 서비스 구독 유효 여부.
      _.each(place_subscribes, (subscribe) => {
        if (CheckSubscribe(subscribe)) this.otaSubscribePlace.push(subscribe);
        // else console.log("- CheckSubscribe false", subscribe);
      });

      console.log("- RoomSaleReport otaSubscribePlace", this.otaSubscribePlace.length);
    });
  }

  update() {
    // 지난 24 시간 매출 집계(결제 시간 기준, 입실 기준 아님)
    RoomSalePay.selectRoomSalePayLast24Report((err, result) => {
      let data_list = {};

      _.map(result, (row, idx) => {
        // console.table("-- row", row);

        let {
          sale_date,
          room_id,
          channel,
          stay_type,
          stay_type_cnt,
          default_fee,
          add_fee,
          prepay_ota_amt,
          prepay_cash_amt,
          prepay_card_amt,
          prepay_point_amt,
          pay_cash_amt,
          pay_card_amt,
          pay_point_amt,
        } = row;

        let sum = prepay_ota_amt + prepay_cash_amt + prepay_card_amt + prepay_point_amt + pay_cash_amt + pay_card_amt + pay_point_amt;

        // console.table("-- sum", sum);

        if (room_id && channel && stay_type && sum) {
          // 초기화.
          if (!data_list[sale_date]) data_list[sale_date] = { sale_date };

          if (!data_list[sale_date][room_id]) {
            let date = moment(sale_date);

            data_list[sale_date][room_id] = {
              year: date.year(),
              month: date.month() + 1,
              day: date.date(),
              hour: date.hour(),
              room_id,
              default_fee: 0,
              add_fee: 0,
              prepay_ota_amt: 0,
              prepay_cash_amt: 0,
              prepay_card_amt: 0,
              prepay_point_amt: 0,
              ieg_rent_cnt: 0,
              ieg_stay_cnt: 0,
              ieg_long_cnt: 0,
              isg_rent_cnt: 0,
              isg_stay_cnt: 0,
              isg_long_cnt: 0,
              isg_pay_cash_amt: 0,
              isg_pay_card_amt: 0,
              isg_pay_point_amt: 0,
              ieg_pay_cash_amt: 0,
              ieg_pay_card_amt: 0,
              ieg_pay_point_amt: 0,
            };
          }

          let data = data_list[sale_date][room_id];

          data.default_fee += default_fee;
          data.add_fee += add_fee;
          data.prepay_ota_amt += prepay_ota_amt;

          // 무인
          if (channel === "isc") {
            // 대실
            if (stay_type === 2) data.isg_rent_cnt = stay_type_cnt;
            // 숙박
            else if (stay_type === 1) data.isg_stay_cnt = stay_type_cnt;
            // 장기
            else data.isg_long_cnt = stay_type_cnt;

            data.isg_pay_cash_amt += pay_cash_amt;
            data.isg_pay_card_amt += pay_card_amt;
            data.isg_pay_point_amt += pay_point_amt;
          }
          // 유인/자동입실.
          else {
            // 대실
            if (stay_type === 2) data.ieg_rent_cnt = stay_type_cnt;
            // 숙박
            else if (stay_type === 1) data.ieg_stay_cnt = stay_type_cnt;
            // 장기
            else data.ieg_long_cnt = stay_type_cnt;

            data.ieg_pay_cash_amt += pay_cash_amt;
            data.ieg_pay_card_amt += pay_card_amt;
            data.ieg_pay_point_amt += pay_point_amt;
          }

          // console.log("-- data", JSON.stringify(data));
        }
      });

      let newSaleReportList = [];

      _.map(data_list, (day_item, idx) => {
        _.map(day_item, (room_item) => {
          if (room_item.room_id) {
            // console.log("-- room_item", JSON.stringify(room_item));
            newSaleReportList.push(room_item);
          }
        });
      });

      let day = -1;

      // 입실 취소 된것 제가 하기 위해 24 시간 정보 삭제
      RoomSaleReportHour.deleteRoomSaleReportHour(day, (err, info) => {});

      console.log("- newSaleReportList", newSaleReportList.length);

      if (newSaleReportList.length) {
        setTimeout(() => {
          // 시간별 데이터 업데이트.
          RoomSaleReportHour.insertRoomSaleReportHour(newSaleReportList, (err, info) => {});
        }, 1000);
      }
    });
  }

  // 매출 리포트는 정산 시간 기준으로
  dailyReport() {
    let hh = moment().hour();

    console.log("-- dailyReport", { hh });

    PREFERENCES.List((list) => {
      _.each(list, (v) => {
        let { place_id, place_name, day_sale_end_time } = v;

        console.log("-- PREFERENCES", { place_id, place_name, day_sale_end_time });

        // TODO 테스트 용 시간 맞춤.
        // if (process.env.REACT_APP_MODE === "LOCAL") day_sale_end_time = hh;

        // 구독 여부 체크.
        let isSubscribe = _.find(this.otaSubscribePlace, { place_id });

        if (!isSubscribe) console.log("-- No Subscribe!");

        // 현재 정산 시간 업소.
        if (isSubscribe && hh === day_sale_end_time) {
          let begin = moment().hour(day_sale_end_time).add(-1, "day");
          let end = moment().hour(day_sale_end_time);

          // 정산 시간이 지났다면 어제 정산 시간 기준.
          if (day_sale_end_time > hh) {
            begin = begin.add(-1, "day");
            end = end.add(-1, "day");
          }

          begin = begin.format("YYYY-MM-DD HH:00");
          end = end.format("YYYY-MM-DD HH:00");

          console.log("-- report", { place_id, day_sale_end_time, begin, end });

          this.dailySale(place_id, place_name, begin, end);
          // this.dailyHours(place_id, begin, end);
        }
      });
    });
  }

  dailySale(place_id, place_name, begin, end) {
    // 초기화.
    let set = {
      count: 0,
      pay_count: 0,
      default_fee: 0,
      add_fee: 0,
      prepay_ota_amt: 0,
      pay_cash_amt: 0,
      prepay_cash_amt: 0,
      pay_card_amt: 0,
      prepay_card_amt: 0,
      pay_point_amt: 0,
      prepay_point_amt: 0,
      receptAmt: 0,
      returnAmt: 0,
    };

    let sum = { stay: set, rent: set, long: set };
    let total = 0;
    let receptAmtTot = 0;
    let returnAmtTot = 0;
    let searchType = 2; // 결제 현황

    let sales = [];

    const filter = "1=1",
      between = "a.reg_date",
      order = "a.id desc";

    console.log("-- dailySale", { place_id, place_name, begin, end });

    RoomSalePay.selectAllRoomSalePays(place_id, filter, between, begin, end, order, (err, list) => {
      let excelData = [];

      _.map(list, (sale, k) => {
        let type = sale.stay_type === 1 ? "stay" : sale.stay_type === 2 ? "rent" : "long";

        // 매출 취소는 합계에서 제외.
        if (sale.state !== "B") {
          let {
            count = 0,
            pay_count = 0,
            default_fee = 0,
            add_fee = 0,
            pay_cash_amt = 0,
            prepay_ota_amt = 0,
            prepay_cash_amt = 0,
            pay_card_amt = 0,
            prepay_card_amt = 0,
            pay_point_amt = 0,
            prepay_point_amt = 0,
            receptAmt = 0,
            returnAmt = 0,
          } = sum[type];

          let pay =
            sale.default_fee + sale.add_fee + sale.pay_cash_amt + sale.prepay_ota_amt + sale.prepay_cash_amt + sale.pay_card_amt + sale.prepay_card_amt + sale.pay_point_amt + sale.prepay_point_amt;

          if (searchType === 1) {
            count++;
          } else {
            if (!sale.move_from || sale.first_pay) {
              // console.log("- sale pay", pay);

              // 추가 매출 없으면 카운트 제외.
              if (pay) pay_count++; // 매출 보기 시 결제 건수.

              let isSaleId = _.find(sales, {
                sale_id: sale.sale_id,
              });

              // 매출 기준일때 숙박 카운팅
              if (!isSaleId) {
                count++; // 매출 보기 시 숙박 건수

                sales.push(sale);
              }
            }
          }

          let fee = sale.default_fee + sale.add_fee;

          let recept = fee - (sale.pay_cash_amt + sale.prepay_ota_amt + sale.prepay_cash_amt + sale.pay_card_amt + sale.prepay_card_amt + sale.pay_point_amt + sale.prepay_point_amt);

          sum[type] = {
            count,
            pay_count,
            default_fee: (default_fee += sale.default_fee),
            add_fee: (add_fee += sale.add_fee),
            pay_cash_amt: (pay_cash_amt += sale.pay_cash_amt),
            prepay_ota_amt: (prepay_ota_amt += sale.prepay_ota_amt),
            prepay_cash_amt: (prepay_cash_amt += sale.prepay_cash_amt),
            pay_card_amt: (pay_card_amt += sale.pay_card_amt),
            prepay_card_amt: (prepay_card_amt += sale.prepay_card_amt),
            pay_point_amt: (pay_point_amt += sale.pay_point_amt),
            prepay_point_amt: (prepay_point_amt += sale.prepay_point_amt),
            receptAmt: (receptAmt += recept > 0 ? Math.abs(recept) : 0),
            returnAmt: (returnAmt += recept < 0 ? Math.abs(recept) : 0),
          };

          total += sale.default_fee + sale.add_fee;
        }

        // 매출 목록 엑셀 생성.
        this.excel(sale, excelData);
      });

      // 미수/반환금 정리.
      _.map(sum, (v) => {
        if (v.receptAmt && v.returnAmt) {
          if (v.receptAmt > v.returnAmt) {
            v.receptAmt = v.receptAmt - v.returnAmt;
            v.returnAmt = 0;
          } else {
            v.receptAmt = 0;
            v.returnAmt = v.returnAmt - v.receptAmt;
          }
        }

        receptAmtTot += v.receptAmt;
        returnAmtTot += v.returnAmt;

        return v;
      });

      console.log("- sum ", { sum, total });

      let table = `
      <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
          <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
          <title>HTML Email Template</title>
          <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        </head>
        <style type="text/css">
          body { width:100%; height:100%; margin:0; padding:0; text-align: center;}
          table { width:70%; margin: 0 auto; text-align: center; border: 1px solid #333; background: #ccc; }
          th,
          td { background: #fff; padding: 1px; }
          .sum { width:70%; display: flex; justify-content: space-around; padding: 10px 0; margin: 20px auto; background: #fbfbfb; border-top: 1px solid #999; border-bottom: 1px solid  #999; }
        </style>
        <body>
            <div>
              <h2>${moment(begin).format("YYYY-DD-DD(ddd) HH:mm")} ~ ${moment(end).format("YYYY-DD-DD(ddd) HH:mm")}</h2>
              <div>
                <table>
                  <thead>
                    <tr>
                      <th></th>
                      <th>매출(숙박)건수</th>
                      <th>현금결제</th>
                      <th>카드결제</th>
                      <th>OTA결제</th>
                      <th>포인트결제</th>
                      <th>미수금</th>
                      <th>반환금</th>
                      <th>합계금액</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <th style="color: rgb(82, 211, 238);">대실</th>
                      <td>${format.toMoney(sum.rent.pay_count)} (${format.toMoney(sum.rent.count)})</td>
                      <td>${format.toMoney(sum.rent.pay_cash_amt + sum.rent.prepay_cash_amt)}</td>
                      <td>${format.toMoney(sum.rent.pay_card_amt + sum.rent.prepay_card_amt)}</td>
                      <td>${format.toMoney(sum.rent.prepay_ota_amt)}</td>
                      <td>${format.toMoney(sum.rent.pay_point_amt + sum.rent.prepay_point_amt)}</td>
                      <td>${format.toMoney(sum.rent.receptAmt)}</td>
                      <td>${format.toMoney(sum.rent.returnAmt)}</td>
                      <td>${format.toMoney(sum.rent.default_fee + sum.rent.add_fee)}</td>
                    </tr>
                    <tr>
                      <th style="color: rgb(255, 171, 62);">숙박</th>
                      <td>${format.toMoney(sum.stay.pay_count)} (${format.toMoney(sum.stay.count)})</td>
                      <td>${format.toMoney(sum.stay.pay_cash_amt + sum.stay.prepay_cash_amt)}</td>
                      <td>${format.toMoney(sum.stay.pay_card_amt + sum.stay.prepay_card_amt)}</td>
                      <td>${format.toMoney(sum.stay.prepay_ota_amt)}</td>
                      <td>${format.toMoney(sum.stay.pay_point_amt + sum.stay.prepay_point_amt)}</td>
                      <td>${format.toMoney(sum.stay.receptAmt)}</td>
                      <td>${format.toMoney(sum.stay.returnAmt)}</td>
                      <td>${format.toMoney(sum.stay.default_fee + sum.stay.add_fee)}</td>
                    </tr>
                    <tr>
                      <th style="color: rgb(255, 114, 122);">장기</th>
                      <td>${format.toMoney(sum.long.pay_count)} (${format.toMoney(sum.long.count)})</td>
                      <td>${format.toMoney(sum.long.pay_cash_amt + sum.long.prepay_cash_amt)}</td>
                      <td>${format.toMoney(sum.long.pay_card_amt + sum.long.prepay_card_amt)}</td>
                      <td>${format.toMoney(sum.long.prepay_ota_amt)}</td>
                      <td>${format.toMoney(sum.long.pay_point_amt + sum.long.prepay_point_amt)}</td>
                      <td>${format.toMoney(sum.long.receptAmt)}</td>
                      <td>${format.toMoney(sum.long.returnAmt)}</td>
                      <td>${format.toMoney(sum.long.default_fee + sum.long.add_fee)}</td>
                    </tr>
                  </tbody>
                  <tfoot>
                    <tr>
                      <th>합계</th>
                      <td>${format.toMoney(sum.rent.pay_count + sum.stay.pay_count + sum.long.pay_count)} (${format.toMoney(sum.rent.count + sum.stay.count + sum.long.count)})</td>
                      <td>${format.toMoney(sum.rent.pay_cash_amt + sum.rent.prepay_cash_amt + sum.stay.pay_cash_amt + sum.stay.prepay_cash_amt + sum.long.pay_cash_amt + sum.long.prepay_cash_amt)}</td>
                      <td>${format.toMoney(sum.rent.pay_card_amt + sum.rent.prepay_card_amt + sum.stay.pay_card_amt + sum.stay.prepay_card_amt + sum.long.pay_card_amt + sum.long.prepay_card_amt)}</td>
                       <td>${format.toMoney(sum.rent.prepay_ota_amt + sum.stay.prepay_ota_amt + sum.long.prepay_ota_amt)}</td>
                      <td>${format.toMoney(
                        sum.rent.pay_point_amt + sum.rent.prepay_point_amt + sum.stay.pay_point_amt + sum.stay.prepay_point_amt + sum.long.pay_point_amt + sum.long.prepay_point_amt
                      )}</td>
                      <td>${format.toMoney(sum.rent.receptAmtTot + sum.stay.receptAmtTot + sum.long.receptAmtTot)}</td>
                      <td>${format.toMoney(sum.rent.returnAmtTot + sum.stay.returnAmtTot + sum.long.returnAmtTot)}</td>
                      <td>${format.toMoney(sum.rent.default_fee + sum.rent.add_fee + sum.stay.default_fee + sum.stay.add_fee + sum.long.default_fee + sum.long.add_fee)}</td>
                    </tr>
                  </tfoot>
                </table>
                <div class="sum">
                  <div>
                    <label>
                      총매출
                    </label>
                    <span>${format.toMoney(total)}</span>
                  </div>
                  <div>
                    <label>
                      미수금
                    </label>
                    <span>${format.toMoney(receptAmtTot)}</span>
                  </div>
                  <div>
                    <label>
                      반환금
                    </label>
                    <span>${format.toMoney(returnAmtTot)}</span>
                  </div>
                </div>
              </div>
              <span class="sum-notice">※ 결제 금액은 결제 당시의 투숙형태에 합산 됩니다.</span>
            </div>
          </body>
        </html>`;

      // console.log("- table ", table);
      // console.log("- excelData", excelData);

      let dir = path.resolve(__dirname, "../../public/static/temp");
      // let name = `${moment().format("YYYYMMDD") + "_" + place_id}`;
      let name = `daily_report_${place_id}`; // 동일 업소는 계속 덮어 씌운다.(새로운 파일 생성 안함)

      // Create the log directory if it does not exist
      if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
      }

      console.log("- excel temp dir", dir, name);

      // 매출 집계표.
      makeExcel("sale_list", dir, name, excelData, (err, filename) => {
        console.log("- make excel ", err, filename);

        let pathToAttachment = filename;
        let attachment = fs.readFileSync(pathToAttachment).toString("base64");

        this.send_mail(place_id, place_name, table, attachment);
      });

      // 매출 집계표.
      // makeExcel("sale_summary", path, name, data, (err, filename) => {
      //   console.log("- make excel ", err, name);
      //   let pathToAttachment = filename;
      //   let attachment = fs.readFileSync(pathToAttachment).toString("base64");
      //   this.send_mail(place_id, place_name, table, attachment);
      // });

      // 매출 집계표.
      // let excel = new ExcelUtil("daily-sale-report", fileName).tableToExcel(table);
    });
  }

  excel(sale, excelData) {
    if (!excelData.length) {
      excelData.push({
        room_name: "객실명",
        stay_type: "투숙형태",
        sale_type: "상태",
        check_in: "입실시간",
        check_out: "퇴실시간",
        reg_date: "결제시간",
        pay_type: "결제타입",
        channel: "판매구분",
        default_fee: "기본요금",
        add_fee: "추가/할인",
        sale_fee: "판매요금",
        ota_prepay: "OTA결제",
        cash_prepay: "현금선결제",
        card_prepay: "카드선결제",
        point_prepay: "포인트선결제",
        cash_pay: "현금결제",
        card_pay: "카드결제",
        point_pay: "포인트결제",
        pay_amt: "결제금액",
        phone: "전화번호",
        card_approval_num: "카드승인번호",
        memo: "메모",
        car_no: "차량번호",
        user_name: "근무자",
      });
    }

    let {
      room_name,
      stay_type,
      state,
      check_in,
      check_out,
      reg_date,
      channel,
      phone,
      default_fee,
      add_fee,
      room_reserv_id,
      prepay_ota_amt = 0,
      prepay_cash_amt = 0,
      prepay_card_amt = 0,
      prepay_point_amt = 0,
      pay_cash_amt = 0,
      pay_card_amt = 0,
      pay_point_amt = 0,
      s_default_fee,
      s_add_fee,
      s_pay_cash_amt,
      s_pay_card_amt,
      s_pay_point_amt,
      s_prepay_ota_amt,
      s_prepay_cash_amt,
      s_prepay_card_amt,
      s_prepay_point_amt,
      s_memo,
      move_from,
      card_approval_num,
      sale_id,
      s_car_no,
      user_name,
      rollback,
      first_pay,
      prev_stay_type,
    } = sale;

    let sale_type = sale_id
      ? rollback === 1
        ? "입실취소"
        : rollback === 2
        ? "퇴실취소"
        : state === "C"
        ? "퇴실"
        : first_pay
        ? "체크인"
        : "사용중"
      : !check_out && state === "A"
      ? "사용중"
      : state === "B"
      ? "입실취소"
      : "퇴실";

    let pay_type = first_pay
      ? room_reserv_id
        ? "예약 체크인"
        : "체크인"
      : move_from
      ? "객실 이동"
      : prev_stay_type
      ? keyToValue("room_sale", "stay_type", prev_stay_type) + "->" + keyToValue("room_sale", "stay_type", stay_type)
      : "추가 결제";

    excelData.push({
      room_name,
      stay_type: keyToValue("room_sale", "stay_type", stay_type),
      sale_type,
      check_in: check_in ? moment(check_in).format("YYYY-MM-DD HH:mm") : "",
      check_out: check_out ? moment(check_out).format("YYYY-MM-DD HH:mm") : "",
      reg_date: moment(reg_date).format("YYYY-MM-DD HH:mm"),
      pay_type,
      channel: keyToValue("room_sale", "channel", channel),
      default_fee: format.toMoney(default_fee),
      add_fee: format.toMoney(first_pay ? s_add_fee : add_fee),
      sale_fee: format.toMoney(default_fee + add_fee),
      ota_prepay: format.toMoney(first_pay ? s_prepay_ota_amt : prepay_ota_amt),
      cash_prepay: format.toMoney(first_pay ? s_prepay_cash_amt : prepay_cash_amt),
      card_prepay: format.toMoney(first_pay ? s_prepay_card_amt : prepay_card_amt),
      point_prepay: format.toMoney(first_pay ? s_prepay_point_amt : prepay_point_amt),
      cash_pay: format.toMoney(first_pay ? s_pay_cash_amt : pay_cash_amt),
      card_pay: format.toMoney(first_pay ? s_pay_card_amt : pay_card_amt),
      point_pay: format.toMoney(first_pay ? s_pay_point_amt : pay_point_amt),
      pay_amt: format.toMoney(
        first_pay
          ? s_pay_cash_amt + s_pay_card_amt + s_pay_point_amt + s_prepay_ota_amt + s_prepay_cash_amt + s_prepay_card_amt + s_prepay_point_amt
          : pay_cash_amt + pay_card_amt + pay_point_amt + prepay_ota_amt + prepay_cash_amt + prepay_card_amt + prepay_point_amt
      ),
      phone: format.toPhone(phone),
      card_approval_num: format.toCard(card_approval_num),
      s_memo,
      s_car_no,
      user_name,
    });
  }

  dailyHours(place_id, place_name, begin, end) {
    RoomSaleReportHour.selectRoomSaleReportHourPlace(place_id, begin, end, (err, result) => {
      let data_list = {};

      let sum = {
        ieg_rent_cnt: 0,
        ieg_stay_cnt: 0,
        ieg_long_cnt: 0,
        isg_rent_cnt: 0,
        isg_stay_cnt: 0,
        isg_long_cnt: 0,
        default_fee: 0,
        add_fee: 0,
        prepay_ota_amt: 0,
        prepay_cash_amt: 0,
        prepay_card_amt: 0,
        prepay_point_amt: 0,
        isg_pay_cash_amt: 0,
        isg_pay_card_amt: 0,
        isg_pay_point_amt: 0,
        ieg_pay_cash_amt: 0,
        ieg_pay_card_amt: 0,
        ieg_pay_point_amt: 0,
        total: 0,
        recept: 0,
      };

      _.map(result, (row, idx) => {
        // console.table("-- row", row);

        let {
          year,
          month,
          day,
          hour,
          ieg_rent_cnt,
          ieg_stay_cnt,
          ieg_long_cnt,
          isg_rent_cnt,
          isg_stay_cnt,
          isg_long_cnt,
          default_fee,
          add_fee,
          prepay_ota_amt,
          prepay_cash_amt,
          prepay_card_amt,
          prepay_point_amt,
          isg_pay_cash_amt,
          isg_pay_card_amt,
          isg_pay_point_amt,
          ieg_pay_cash_amt,
          ieg_pay_card_amt,
          ieg_pay_point_amt,
        } = row;

        sum.ieg_rent_cnt += ieg_rent_cnt;
        sum.ieg_stay_cnt += ieg_stay_cnt;
        sum.ieg_long_cnt += ieg_long_cnt;
        sum.isg_rent_cnt += isg_rent_cnt;
        sum.isg_stay_cnt += isg_stay_cnt;
        sum.isg_long_cnt += isg_long_cnt;

        sum.prepay_ota_amt += prepay_ota_amt;
        sum.prepay_cash_amt += prepay_cash_amt;
        sum.prepay_card_amt += prepay_card_amt;
        sum.prepay_point_amt += prepay_point_amt;

        sum.isg_pay_cash_amt += isg_pay_cash_amt;
        sum.isg_pay_card_amt += isg_pay_card_amt;
        sum.isg_pay_point_amt += isg_pay_point_amt;
        sum.ieg_pay_cash_amt += ieg_pay_cash_amt;
        sum.ieg_pay_card_amt += ieg_pay_card_amt;
        sum.ieg_pay_point_amt += ieg_pay_point_amt;

        let feeSum = default_fee + add_fee;
        let paySum = prepay_ota_amt + prepay_cash_amt + prepay_card_amt + isg_pay_cash_amt + isg_pay_card_amt + isg_pay_point_amt + ieg_pay_cash_amt + ieg_pay_card_amt + ieg_pay_point_amt;

        sum.total += paySum;
        sum.recept += feeSum - paySum;
      });

      let table = `
              <div>
                <h2>${begin} ~ ${end}</h2>
                <div>
                  <table>
                    <thead>
                      <tr>
                        <th></th>
                        <th>결제건수</th>
                        <th>현금결제</th>
                        <th>카드결제</th>
                        <th>OTA결제</th>
                        <th>포인트결제</th>
                        <th>미수금</th>
                        <th>반환금</th>
                        <th>합계금액</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <th style="color: rgb(82, 211, 238);">대실</th>
                        <td>${sum.ieg_rent_cnt + sum.isg_rent_cnt}</td>
                        <td>${format.toMoney(sum.isg_pay_cash_amt + sum.ieg_pay_cash_amt)}</td>
                        <td>${format.toMoney(sum.isg_pay_card_amt + sum.ieg_pay_card_amt)}</td>
                        <td>${format.toMoney(sum.prepay_ota_amt)}</td>
                        <td>${format.toMoney(sum.ieg_pay_point_amt)}</td>
                        <td>${format.toMoney(sum.recept)}</td>
                        <td>${format.toMoney(sum.return)}</td>
                        <td>${format.toMoney(sum.total)}</td>
                      </tr>
                      <tr>
                        <th style="color: rgb(255, 171, 62);">숙박</th>
                       <td>${sum.ieg_stay_cnt + sum.isg_stay_cnt}</td>
                        <td>${format.toMoney(sum.isg_pay_cash_amt + sum.ieg_pay_cash_amt)}</td>
                        <td>${format.toMoney(sum.isg_pay_card_amt + sum.ieg_pay_card_amt)}</td>
                        <td>${format.toMoney(sum.prepay_ota_amt)}</td>
                        <td>${format.toMoney(sum.ieg_pay_point_amt)}</td>
                        <td>${format.toMoney(sum.recept)}</td>
                        <td>${format.toMoney(sum.return)}</td>
                        <td>${format.toMoney(sum.total)}</td>
                      </tr>
                      <tr>
                        <th style="color: rgb(255, 114, 122);">장기</th>
                       <td>${sum.ieg_rent_cnt + sum.ieg_stay_cnt + sum.isg_rent_cnt + sum.isg_stay_cnt}</td>
                        <td>${format.toMoney(sum.isg_pay_cash_amt + sum.ieg_pay_cash_amt)}</td>
                        <td>${format.toMoney(sum.isg_pay_card_amt + sum.ieg_pay_card_amt)}</td>
                        <td>${format.toMoney(sum.prepay_ota_amt)}</td>
                        <td>${format.toMoney(sum.ieg_pay_point_amt)}</td>
                        <td>${format.toMoney(sum.recept)}</td>
                        <td>${format.toMoney(sum.return)}</td>
                        <td>${format.toMoney(sum.total)}</td>
                      </tr>
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>합계</th>
                       <td>${sum.ieg_rent_cnt + sum.ieg_stay_cnt + sum.isg_rent_cnt + sum.isg_stay_cnt}</td>
                        <td>${format.toMoney(sum.isg_pay_cash_amt + sum.ieg_pay_cash_amt)}</td>
                        <td>${format.toMoney(sum.isg_pay_card_amt + sum.ieg_pay_card_amt)}</td>
                        <td>${format.toMoney(sum.prepay_ota_amt)}</td>
                        <td>${format.toMoney(sum.ieg_pay_point_amt)}</td>
                        <td>${format.toMoney(sum.recept)}</td>
                        <td>${format.toMoney(sum.return)}</td>
                        <td>${format.toMoney(sum.total)}</td>
                      </tr>
                    </tfoot>
                  </table>
                  <div>
                    <div>
                      <label>
                        총매출
                      </label>
                      <span> 395,000</span>
                    </div>
                    <div>
                      <label>
                        미수금
                      </label>
                      <span>0</span>
                    </div>
                    <div>
                      <label>
                        반환금
                      </label>
                      <span>0</span>
                    </div>
                  </div>
                </div>
                <span class="sum-notice">※ 결제 금액은 결제 당시의 투숙형태에 합산 됩니다.</span>
              </div>
            `;

      console.log("- table", table);

      let name = "매출 정보";

      // makeExcel("DailySales", name, all_room_sales, res, (err, name) => {
      //   console.log("- download ", err, name);
      // });
    });
  }

  // 메일 발송.
  send_mail(place_id, place_name, table, attachment) {
    console.log("---> report send_mail ", { place_id, place_name });

    let subject = `[아이크루] ${process.env.REACT_APP_MODE !== "PROD" ? "[" + process.env.REACT_APP_MODE + "]" : ""} ${place_name} ${moment().format("YYYY-DD-DD(ddd)")} 일일 매출 리포트 입니다.`;
    let content = `<br/><h4><font color=green>아이크루 [일일 매출 메일링 서비스] 리포트 메일 입니다.</font></h4>${table}<br/><br/>주식회사 아이크루컴퍼니<br/>1600-5356<br/><br/>`;

    if (place_id) {
      let users = _.filter(this.receiveMailUsers, (user) => user.place_id === place_id);

      _.each(users, (user) => {
        let { id, email, daily_report_mail_yn } = user;
        console.log("---> user", { id, email, daily_report_mail_yn });

        if (email && daily_report_mail_yn === "Y") {
          // 알림 메일 모두 수신 또는 실패시 수신 시 발송.
          let mail = {
            type: 0, // 메일 타입 (0: 고객메일, 1: 관리자메일, 9: 서버메일)
            to: email,
            from: "report@icrew.kr",
            subject,
            content,
            attachments: JSON.stringify([
              {
                content: attachment,
                filename: `${moment().format("YYYY-MM-DD")}_daily_sale_report.xls`,
                type: "application/xls",
                disposition: "attachment",
              },
            ]),
          };

          Mail.insertMail(mail, (err, info) => {});
        }
      });
    }
  }

  delete() {
    RoomSaleReportHour.deleteRoomSaleOldReportHour(-10000, (err, info) => {});
    RoomSaleReportDay.deleteRoomSaleOldReportDay(-10000, (err, info) => {});
    RoomSaleReportMonth.deleteRoomSaleOldReportMonth(-36, (err, info) => {});
    RoomSaleReportYear.deleteRoomSaleOldReportYear(10, (err, info) => {});
  }
}

module.exports = RoomSaleReport;
