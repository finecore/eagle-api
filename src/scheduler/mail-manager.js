import Mail from "model/mail";

import _ from "lodash";
import moment from "moment";
import chalk from "chalk";
import format from "utils/format-util";

import sgMail from "@sendgrid/mail";

// 메일 발송 관리자.
class MailManager {
  constructor() {
    console.log(chalk.yellow("----------- MailManager Scheduler Create ----------"));
    this.timer = null;

    console.log("- REACT_APP_MAIL_SENDGRID_API_KEY" + process.env.REACT_APP_MAIL_SENDGRID_API_KEY);

    setTimeout(
      () => {
        this.init();
      },
      process.env.REACT_APP_MODE === "LOCAL" ? 3000 : 30000
    );

    console.log("");
  }

  init() {
    sgMail.setApiKey(process.env.REACT_APP_MAIL_SENDGRID_API_KEY);

    this.run();
  }

  run() {
    console.log("--------------------------------------------");
    console.log("---- MailManager Run!");
    console.log("--------------------------------------------");

    if (this.timer) clearInterval(this.timer);

    // 10초 마다 실행.
    this.timer = setInterval(() => {
      this.sendMails();
    }, 10 * 1000);

    // 1일 마다 실행.
    this.timer = setInterval(() => {
      this.delMails();
    }, 24 * 60 * 60 * 1000);

    // setTimeout(() => {
    //   if (process.env.REACT_APP_MODE !== "LOCAL") {
    //     this.test();
    //   }
    // }, 5 * 1000);
  }

  test() {
    this.send({
      type: 1, // 메일 타입 (0: 고객메일, 1: 관리자메일, 9: 서버메일)
      to: "friendkj@icrew.kr",
      from: process.env.REACT_APP_MAIL_FROM_ADDR,
      subject: process.env.REACT_APP_MODE + " GCP SENDGRID 테스트 메일",
      content: "<strong>This is a sample email message.</strong>",
    });
  }

  send({ type = 0, to, from, subject, content }) {
    console.log("-> MailManager send", { to, from, subject });

    if (to && subject && content && content.length > 2) {
      const mail = {
        type,
        to,
        from: from || process.env.REACT_APP_MAIL_FROM_ADDR,
        subject,
        content,
      };

      Mail.insertMail(mail, (err, as) => {});
    }
  }

  sendMails() {
    let filter = "a.send_date IS NULL AND is_error = 0",
      between = "a.reserv_date",
      begin = moment().add(-1, "day").format("YYYY-MM-DD 00:00"),
      end = moment().add(1, "minute").format("YYYY-MM-DD HH:mm"),
      limit = "10000",
      order = "a.reg_date",
      desc = "asc";

    Mail.selectMails(filter, between, begin, end, order, desc, limit, (err, mails) => {
      _.each(mails, (mail) => {
        const { id, type, to, from, subject, content, attachments } = mail;

        if (to && subject && content) {
          const msg = {
            to,
            // from: from || process.env.REACT_APP_MAIL_FROM_ADDR,
            from: process.env.REACT_APP_MAIL_FROM_ADDR,
            subject: subject,
            text: content,
            html: content,
          };

          // const msg = {
          //   personalizations: [
          //     {
          //       to: [
          //         {
          //           email: to,
          //           name: "수신",
          //         },
          //       ],
          //     },
          //   ],
          //   from: {
          //     email: from || process.env.REACT_APP_MAIL_FROM_ADDR,
          //     name: "송신",
          //   },
          //   // replyTo: {
          //   //   email: process.env.REACT_APP_MAIL_FROM_ADDR,
          //   //   name: '참조'
          //   // },
          //   subject: subject,
          //   content: [
          //     {
          //       type: "text/html",
          //       value: content,
          //     },
          //   ],
          // };

          if (attachments) {
            msg.attachments = JSON.parse(attachments);
          }

          // console.log("- mail send", msg);

          sgMail.send(msg).then(
            () => {
              Mail.updateMail(id, { send_date: new Date() }, (err, mails) => {
                console.log("- mail send ", to);
              });
            },
            (error) => {
              // console.log("- mail send", msg);
              error.msg = msg;

              console.error(error);

              Mail.updateMail(id, { is_error: 1 }, (err, mails) => {});
            }
          );
        }
      });
    });
  }

  delMails() {
    let date = moment().add(-180, "day").format("YYYY-MM-DD HH:mm");

    Mail.deleteOldMail(date, (err, mails) => {
      console.log("- old mail delete ", date);
    });
  }
}

module.exports = MailManager;
