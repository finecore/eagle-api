import connection from "db/connection";

// MmsMo object constructor
const MmsMo = {
  selectMmsMoCount: (filter, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT count(*) count 
             FROM HY_MO_TRAN a
            WHERE ${filter} `,
          [],
          result
        );
      }
    });
  },

  selectMmsMoes: (filter, order, desc, limit, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*
             FROM HY_MO_TRAN a
            WHERE ${filter} 
            ORDER BY ${order} ${desc} LIMIT ${limit} `,
          [],
          result
        );
      }
    });
  },

  selectMmsMo: (mo_key, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*
             FROM HY_MO_TRAN a
            WHERE a.MO_KEY = ? `,
          [mo_key],
          result
        );
      }
    });
  },

  insertMmsMo: (mms_mo, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(`INSERT INTO HY_MO_TRAN SET ?`, [mms_mo], result);
      }
    });
  },

  updateMmsMo: (mo_key, mms_mo, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(`UPDATE HY_MO_TRAN SET ? WHERE MO_KEY = ?`, [mms_mo, mo_key], result);
      }
    });
  },

  deleteMmsMo: (mo_key, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(`DELETE FROM HY_MO_TRAN WHERE MO_KEY = ?`, [mo_key], result);
      }
    });
  },

  deleteOldDays: (days, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `DELETE FROM HY_MO_TRAN
            WHERE DATE_FORMAT(RESULT_DATE, '%Y%m%d') <= DATE_FORMAT(DATE_ADD(NOW(), INTERVAL ? DAY), '%Y%m%d');`,
          [days],
          result
        );
      }
    });
  },
};

module.exports = MmsMo;
