const connection = require("../db/connection");
import _ from "lodash";

// RoomReserv object constructor
const RoomReserv = {
  selectAllRoomReservs: (place_id, filter, between, begin, end, order, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.name room_name, c.name room_type_name, c.default_fee_stay, c.default_fee_rent, c.reserv_discount_fee_stay, c.reserv_discount_fee_rent
             FROM room_reserv a LEFT JOIN room b ON a.room_id = b.id LEFT JOIN room_type c ON a.room_type_id = c.id
            WHERE a.place_id = ? AND
            ${filter} AND ${between} BETWEEN ? AND ? ORDER BY ${order} LIMIT 1000;`,
          [place_id, begin, end],
          result
        );
      }
    });
  },

  selectRoomReservs: (filter, between, begin, end, order, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.name room_name, c.name room_type_name, c.default_fee_stay, c.default_fee_rent, c.reserv_discount_fee_stay, c.reserv_discount_fee_rent
             FROM room_reserv a LEFT JOIN room b ON a.room_id = b.id LEFT JOIN room_type c ON a.room_type_id = c.id
            WHERE ${filter} AND ${between} BETWEEN ? AND ? ORDER BY ${order} LIMIT 1000;`,
          [begin, end],
          result
        );
      }
    });
  },

  selectPlaceEnableReservs: (place_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, c.name room_type_name, d.name room_type_name
             FROM room_reserv a LEFT JOIN room_sale b ON a.id = b.room_reserv_id LEFT JOIN room c ON a.room_id = c.id LEFT JOIN room_type d ON a.room_type_id = d.id
            WHERE a.place_id = ?
              AND a.state = 'A'
              AND date_format(NOW(), '%Y-%m-%d') <= date_format(a.check_out_exp, '%Y-%m-%d')
         ORDER BY check_in_exp; `,
          [place_id],
          result
        );
      }
    });
  },

  selectRoomReserv: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          "SELECT a.*, b.name room_type_name, c.name place_name, c.tel place_tel FROM room_reserv a LEFT JOIN room_type b ON a.room_type_id = b.id LEFT JOIN place c ON a.place_id = c.id WHERE a.id = ? ",
          [id],
          result
        );
      }
    });
  },

  selectRoomReservNum: (num, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          "SELECT a.*, b.name room_type_name, c.name place_name, c.tel place_tel FROM room_reserv a LEFT JOIN room_type b ON a.room_type_id = b.id LEFT JOIN place c ON a.place_id = c.id WHERE reserv_num = ? ",
          [num],
          result
        );
      }
    });
  },

  selectRoomReservByMmsNo: (num, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          "SELECT a.*, b.name room_type_name, c.name place_name, c.tel place_tel FROM room_reserv a LEFT JOIN room_type b ON a.room_type_id = b.id LEFT JOIN place c ON a.place_id = c.id WHERE mms_mo_num = ? ",
          [num],
          result
        );
      }
    });
  },

  selectRoomReservByReservNum: (num, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          "SELECT a.*, b.name room_type_name, c.name place_name, c.tel place_tel FROM room_reserv a LEFT JOIN room_type b ON a.room_type_id = b.id LEFT JOIN place c ON a.place_id = c.id WHERE reserv_num = ? ",
          [num],
          result
        );
      }
    });
  },

  selectRoomReservNow: (room_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          "SELECT * FROM room_reserv WHERE room_id = ? AND state = 'A' AND check_out is not null AND check_out is null ORDER BY id DESC LIMIT 1 ",
          [room_id, filter, begin, end, order],
          result
        );
      }
    });
  },

  insertRoomReserv: (roomReserv, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("INSERT INTO room_reserv SET ? ON DUPLICATE KEY UPDATE ? ", [roomReserv, roomReserv], result);
      }
    });
  },

  updateRoomReserv: (id, roomReserv, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("UPDATE room_reserv SET ? WHERE id = ?", [roomReserv, id], result);
      }
    });
  },

  deleteRoomReservs: (room_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM room_reserv WHERE room_id = ? ", [room_id], result);
      }
    });
  },

  deleteRoomReserv: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM room_reserv WHERE id = ? ", [id], result);
      }
    });
  },
};

export default RoomReserv;
