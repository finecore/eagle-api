const connection = require("../db/connection");
import RoomState from "model/room-state";
import _ from "lodash";

// RoomSaleReport object constructor
const RoomSaleReport = {
  selectPlaceSaleReportHoursCount: (place_id, between, begin, end, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT count(*) count
          FROM room_sale_report_hour a INNER JOIN room b ON a.room_id = b.id INNER JOIN place c ON b.place_id=c.id
          WHERE ${Number(place_id) > 0 ? "b.place_id=" + place_id + " AND " : ""}  ${between} BETWEEN '${begin}' AND '${end}' `,
          [],
          result
        );
      }
    });
  },

  selectPlaceSaleReportHours: (place_id, between, begin, end, order, limit, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT c.id, c.name, a.*
          FROM room_sale_report_hour a INNER JOIN room b ON a.room_id = b.id INNER JOIN place c ON b.place_id=c.id
          WHERE ${Number(place_id) > 0 ? "b.place_id=" + place_id + " AND " : ""}  ${between} BETWEEN '${begin}' AND '${end}'
          ORDER BY ${order}
          LIMIT ${limit}`,
          [],
          result
        );
      }
    });
  },

  selectPlaceSaleReportDaysCount: (place_id, between, begin, end, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT count(*) count
          FROM room_sale_report_day a INNER JOIN room b ON a.room_id = b.id INNER JOIN place c ON b.place_id=c.id
          WHERE ${Number(place_id) > 0 ? "b.place_id=" + place_id + " AND " : ""} ${between} BETWEEN '${begin}' AND '${end}' `,
          [],
          result
        );
      }
    });
  },

  selectPlaceSaleReportDays: (place_id, between, begin, end, order, limit, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT c.id, c.name, a.*
          FROM room_sale_report_day a INNER JOIN room b ON a.room_id = b.id INNER JOIN place c ON b.place_id=c.id
          WHERE ${Number(place_id) > 0 ? "b.place_id=" + place_id + " AND " : ""} ${between} BETWEEN '${begin}' AND '${end}'
          ORDER BY ${order}
          LIMIT ${limit}`,
          [],
          result
        );
      }
    });
  },

  selectPlaceSaleReportMonthsCount: (place_id, between, begin, end, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT count(*) count
          FROM room_sale_report_month a INNER JOIN room b ON a.room_id = b.id INNER JOIN place c ON b.place_id=c.id
          WHERE ${Number(place_id) > 0 ? "b.place_id=" + place_id + " AND " : ""} ${between} BETWEEN '${begin}' AND '${end}' }`,
          [],
          result
        );
      }
    });
  },

  selectPlaceSaleReportMonths: (place_id, between, begin, end, order, limit, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT c.id, c.name, a.*
          FROM room_sale_report_month a INNER JOIN room b ON a.room_id = b.id INNER JOIN place c ON b.place_id=c.id
          WHERE ${Number(place_id) > 0 ? "b.place_id=" + place_id + " AND " : ""} ${between} BETWEEN '${begin}' AND '${end}'
          ORDER BY ${order}
          LIMIT ${limit}`,
          [],
          result
        );
      }
    });
  },

  selectPlaceSaleReportYearsCount: (place_id, between, begin, end, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT count(*) count
          FROM room_sale_report_year a INNER JOIN room b ON a.room_id = b.id INNER JOIN place c ON b.place_id=c.id
          WHERE ${Number(place_id) > 0 ? "b.place_id=" + place_id + " AND " : ""} ${between} BETWEEN '${begin}' AND '${end}' `,
          [],
          result
        );
      }
    });
  },

  selectPlaceSaleReportYears: (place_id, between, begin, end, order, limit, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT c.id, c.name, a.*
          FROM room_sale_report_year a INNER JOIN room b ON a.room_id = b.id INNER JOIN place c ON b.place_id=c.id
          WHERE ${Number(place_id) > 0 ? "b.place_id=" + place_id + " AND " : ""} ${between} BETWEEN '${begin}' AND '${end}'
          ORDER BY ${order}
          LIMIT ${limit}`,
          [],
          result
        );
      }
    });
  },
};

export default RoomSaleReport;
