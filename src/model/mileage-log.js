const connection = require("../db/connection");

// MileageLog object constructor
const MileageLog = {
  selectAllMileageLogs: (place_id, filter, between, begin, end, order, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, c.name as place_name, d.id as member_id, d.name as member_name
             FROM mileage_log a 
                  LEFT JOIN mileage b ON a.mileage_id = b.id 
                  INNER JOIN place c ON a.place_id = c.id 
                  LEFT JOIN eagle.member d ON a.place_id = d.place_id AND b.member_id = d.id
            WHERE a.change_point != 0 AND a.place_id = ? AND ${filter} AND ${between} BETWEEN ? AND ? 
            ORDER BY ${order} `,
          [place_id, begin, end],
          result
        );
      }
    });
  },

  selectMileageLogs: (place_id, limit, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, c.name as place_name, d.name as member_name
             FROM mileage_log a 
                  LEFT JOIN mileage b ON a.mileage_id = b.id 
                  INNER JOIN place c ON a.place_id = c.id 
                  LEFT JOIN eagle.member d ON b.member_id = d.id
            WHERE a.change_point != 0 AND a.place_id = ? 
            ORDER BY a.id DESC LIMIT ?;`,
          [place_id, limit],
          result
        );
      }
    });
  },

  selectMileageDayLogs: (place_id, day, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, c.name as place_name, d.name as member_name
             FROM mileage_log a 
                  LEFT JOIN mileage b ON a.mileage_id = b.id 
                  INNER JOIN place c ON a.place_id = c.id 
                  LEFT JOIN eagle.member d ON b.member_id = d.id
            WHERE a.change_point != 0 AND a.place_id = ? AND a.reg_date >= DATE_ADD(NOW(), INTERVAL ? DAY) 
            ORDER BY a.id DESC ;`,
          [place_id, day],
          result
        );
      }
    });
  },

  selectMileageLastLogs: (place_id, last_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, c.name as place_name, d.name as member_name
             FROM mileage_log a 
                  LEFT JOIN mileage b ON a.mileage_id = b.id 
                  INNER JOIN place c ON a.place_id = c.id 
                  LEFT JOIN eagle.member d ON b.member_id = d.id
            WHERE a.change_point != 0 AND a.place_id = ? 
              AND a.id > ?  
            ORDER BY a.id DESC LIMIT 300 `,
          [place_id, last_id],
          result
        );
      }
    });
  },

  selectMileageLog: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("SELECT * FROM mileage_log WHERE id = ? ", id, result);
      }
    });
  },

  insertMileageLog: (mileage_log, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("INSERT INTO mileage_log SET ? ", [mileage_log, mileage_log], result);
      }
    });
  },

  updateMileageLog: (id, mileage_log, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("UPDATE mileage_log SET ? WHERE id = ?", [mileage_log, id], result);
      }
    });
  },

  deleteMileageAfterKeepDay: (place_id, date, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `DELETE a FROM mileage_log a INNER JOIN mileage b ON a.mileage_id = b.id INNER JOIN place c ON a.place_id = c.id  
            WHERE a.place_id = ? AND a.reg_date < STR_TO_DATE(?, '%Y-%m-%d %H:%i'); `,
          [place_id, date],
          result
        );
      }
    });
  },

  deleteMileageLog: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM mileage_log WHERE id = ? ", [id], result);
      }
    });
  },
};

export default MileageLog;
