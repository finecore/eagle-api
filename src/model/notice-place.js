import connection from "db/connection";

// NoticePlace object constructor
const NoticePlace = {
  selectNoticePlaceCount: (filter, between, begin, end, all, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT count(*) count 
             FROM notice_place a
            WHERE ${filter} AND ${between} BETWEEN ? AND ? `,
          [begin, end],
          result
        );
      }
    });
  },

  selectNoticePlaces: (filter, between, begin, end, order, desc, limit, all, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*
             FROM notice_place a
            WHERE ${filter} AND ${between} BETWEEN ? AND ? 
            ORDER BY ${order} ${desc} LIMIT ${limit} `,
          [begin, end],
          result
        );
      }
    });
  },

  selectNoticePlace: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*
             FROM notice_place a WHERE a.id = ? `,
          [id],
          result
        );
      }
    });
  },

  insertNoticePlace: (newNoticePlace, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("INSERT INTO notice_place SET ? ", [newNoticePlace], result);
      }
    });
  },

  updateNoticePlace: (id, notice_place, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("UPDATE notice_place SET ? WHERE id = ?", [notice_place, id], result);
      }
    });
  },

  deleteNoticePlace: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM notice_place WHERE id = ?", [id], result);
      }
    });
  }
};

module.exports = NoticePlace;
