const connection = require("../db/connection");

// Room object constructor
const Room = {
  selectAllRooms: (result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.name as type_name, b.disc as type_disc, c.sale, c.isc_sale_1, c.isc_sale_2, c.isc_sale_3, c.main_power_type  
             FROM room a LEFT JOIN room_type b ON a.room_type_id = b.id LEFT JOIN room_state c ON a.id = c.room_id ;`,
          [],
          result
        );
      }
    });
  },

  selectPlaceRooms: (place_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.name as type_name, b.disc as type_disc, c.sale, c.isc_sale_1, c.isc_sale_2, c.isc_sale_3,c.main_power_type  
             FROM room a LEFT JOIN room_type b ON a.room_type_id = b.id LEFT JOIN room_state c ON a.id = c.room_id 
            WHERE a.place_id = ? 
            ORDER BY floor, LENGTH(a.name), a.name;`,
          [place_id],
          result
        );
      }
    });
  },

  selectRoomCount: (filter, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT count(*) count
             FROM room a LEFT JOIN room_type b ON a.room_type_id = b.id LEFT JOIN room_state c ON a.id = c.room_id 
            WHERE ${filter}`,
          [],
          result
        );
      }
    });
  },

  selectRooms: (filter, order, desc, limit, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.name as type_name, b.disc as type_disc, c.sale, c.isc_sale_1, c.isc_sale_2, c.isc_sale_3, c.main_power_type  
             FROM room a LEFT JOIN room_type b ON a.room_type_id = b.id LEFT JOIN room_state c ON a.id = c.room_id 
            WHERE ${filter} ORDER BY ${order} ${desc} LIMIT ${limit}`,
          [],
          result
        );
      }
    });
  },

  selectReservEnableRooms: (place_id, filter, order, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          "SELECT a.*, b.name as type_name, b.disc as type_disc, c.sale, d.state, d.check_in, d.check_out, d.check_in_exp, d.check_out_exp " +
            "  FROM room a LEFT JOIN room_type b ON a.room_type_id = b.id LEFT JOIN room_state c ON a.id = c.room_id LEFT JOIN room_sale d ON c.room_sale_id = d.id " +
            " WHERE a.reserv_yn = 1 AND a.place_id = ? AND " +
            filter +
            " ORDER BY " +
            order,
          [place_id],
          result
        );
      }
    });
  },

  selectRoom: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.name as type_name, b.disc as type_disc, c.sale, c.isc_sale_1, c.isc_sale_2, c.isc_sale_3, main_power_type 
             FROM room a LEFT JOIN room_type b ON a.room_type_id = b.id LEFT JOIN room_state c ON a.id = c.room_id 
            WHERE a.id = ? ;`,
          id,
          result
        );
      }
    });
  },

  selectRoomByName: (place_id, name, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.name as type_name, b.disc as type_disc, c.sale, c.isc_sale_1, c.isc_sale_2, c.isc_sale_3, main_power_type 
             FROM room a LEFT JOIN room_type b ON a.room_type_id = b.id LEFT JOIN room_state c ON a.id = c.room_id 
            WHERE a.place_id = ? AND CAST(a.name AS UNSIGNED) = CAST(? AS UNSIGNED) ;`,
          [place_id, name],
          result
        );
      }
    });
  },

  insertRoom: (newRoom, result, conn) => {
    if (conn) {
      conn.sql("INSERT INTO room SET ? ON DUPLICATE KEY UPDATE ? ", [newRoom, newRoom], result, true);
    } else {
      connection((_err, _connection) => {
        if (_err) result(_err, null);
        else {
          _connection.sql("INSERT INTO room SET ? ON DUPLICATE KEY UPDATE ? ", [newRoom, newRoom], result);
        }
      });
    }
  },

  updateRoom: (id, room, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("UPDATE room SET ? WHERE id = ?", [room, id], result);
      }
    });
  },

  deleteRoom: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM room WHERE id = ?", [id], result);
      }
    });
  },

  deletePlaceRooms: (place_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM room WHERE place_id = ?", [place_id], result);
      }
    });
  },
};

export default Room;
