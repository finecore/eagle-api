const connection = require("../db/connection");

// PlaceLock object constructor
const PlaceLock = {
  selectAllPlaceLocks: (place_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          "SELECT a.*, b.level FROM place_lock a INNER JOIN user b ON a.user_id = b.id WHERE a.place_id = ? ",
          [place_id],
          result
        );
      }
    });
  },

  selectPlaceLock: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          "SELECT a.*, b.level FROM place_lock a INNER JOIN user b ON a.user_id = b.id WHERE a.id = ? ",
          id,
          result
        );
      }
    });
  },

  insertPlaceLock: (newPlaceLock, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          "INSERT INTO place_lock SET ? ON DUPLICATE KEY UPDATE ? ",
          [newPlaceLock, newPlaceLock],
          result
        );
      }
    });
  },

  updatePlaceLock: (id, placeLock, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("UPDATE place_lock SET ? WHERE id = ?", [placeLock, id], result);
      }
    });
  },

  deletePlaceLock: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM place_lock WHERE id = ? ", [id], result);
      }
    });
  },

  deleteAllPlaceLock: (place_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM place_lock WHERE place_id = ?  ", [place_id], result);
      }
    });
  }
};

export default PlaceLock;
