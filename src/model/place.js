const connection = require("../db/connection");

// Place object constructor
const Place = {
  selectPlaceCount: (filter, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(`SELECT count(*) count FROM place a WHERE ${filter} `, [], result);
      }
    });
  },

  selectPlaces: (filter, order, desc, limit, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, a.id as place_id, a.name place_name, b.name as company_name, 
            (select count(*) from room where place_id = a.id) as room_cnt
             FROM place a INNER JOIN company b ON a.company_id = b.id
            WHERE ${filter} ORDER BY ${order} ${desc} LIMIT ${limit} `,
          [],
          result
        );
      }
    });
  },

  selectPlace: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("SELECT * FROM place WHERE id = ? ", id, result);
      }
    });
  },

  insertPlace: (newPlace, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          "INSERT INTO place SET ? ON DUPLICATE KEY UPDATE ? ",
          [newPlace, newPlace],
          result
        );
      }
    });
  },

  updatePlace: (id, place, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("UPDATE place SET ? WHERE id = ?", [place, id], result);
      }
    });
  },

  deletePlace: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM place WHERE id = ?", [id], result);
      }
    });
  }
};

export default Place;
