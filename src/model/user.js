const connection = require("../db/connection");

// User object constructor
const User = {
  selectUserCount: (filter, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(`SELECT count(*) count FROM user a INNER JOIN place b ON a.place_id = b.id WHERE ${filter} `, [], result);
      }
    });
  },

  selectUsers: (filter, order, desc, limit, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(`SELECT a.*, b.company_id, b.name place_name FROM user a INNER JOIN place b ON a.place_id = b.id WHERE ${filter} ORDER BY ${order} ${desc} LIMIT ${limit} `, [], result);
      }
    });
  },

  selectUser: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("SELECT a.*, b.company_id, b.name place_name FROM user a INNER JOIN place b ON a.place_id = b.id WHERE a.id = ? ", id, result);
      }
    });
  },

  selectUserByPms: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("SELECT a.*, b.company_id, b.name place_name FROM user a INNER JOIN place b ON a.place_id = b.id WHERE a.id = ? AND a.pms IS NOT NULL;", id, result);
      }
    });
  },

  selectUserByIdPwd: (id, pwd, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.expire_date >= CURRENT_TIMESTAMP place_expire, b.name place_name, c.id company_id, c.name company_name, d.pms pms, d.eagle_url
             FROM user a INNER JOIN place b ON a.place_id = b.id INNER JOIN company c ON b.company_id = c.id INNER JOIN preferences d ON b.id = d.place_id
            WHERE a.id = ? AND a.pwd = ? `,
          [id, pwd],
          result
        );
      }
    });
  },

  insertUser: (newUser, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("INSERT INTO user SET ? ON DUPLICATE KEY UPDATE ? ", [newUser, newUser], result);
      }
    });
  },

  updateUser: (id, user, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("UPDATE user SET ? WHERE id = ? ", [user, id], result);
      }
    });
  },

  deleteUser: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM user WHERE id = ?", [id], result);
      }
    });
  },

  deletePlaceUsers: (place_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM user WHERE place_id = ?", [place_id], result);
      }
    });
  },
};

module.exports = User;
