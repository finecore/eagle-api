const connection = require("../db/connection");

// Member object constructor
const Member = {
  selectMemberCount: (filter, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT count(*) count 
             FROM eagle.member a INNER JOIN place b ON a.place_id = b.id
            WHERE ${filter} `,
          [],
          result
        );
      }
    });
  },

  selectMembers: (filter, order, desc, limit, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.name as place_name
           FROM eagle.member a INNER JOIN place b ON a.place_id = b.id 
           WHERE ${filter} 
           ORDER BY ${order} ${desc} 
           LIMIT ${limit} `,
          [],
          result
        );
      }
    });
  },

  selectPlaceMemberByPhone: (place_id, phone, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.name as place_name
           FROM eagle.member a INNER JOIN place b ON a.place_id = b.id 
            WHERE a.place_id = ? AND a.phone = ?`,
          [place_id, phone],
          result
        );
      }
    });
  },

  selectPlaceMembers: (place_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.name as place_name
           FROM eagle.member a INNER JOIN place b ON a.place_id = b.id 
           WHERE a.place_id = ? `,
          [place_id],
          result
        );
      }
    });
  },

  selectMember: (id, phone, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.name as place_name
           FROM eagle.member a INNER JOIN place b ON a.place_id = b.id 
            WHERE a.id = ? OR a.phone = ?`,
          [id, phone],
          result
        );
      }
    });
  },

  insertMember: (member, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          "INSERT INTO eagle.member SET ? ON DUPLICATE KEY UPDATE ? ",
          [member, member],
          result
        );
      }
    });
  },

  updateMember: (id, member, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("UPDATE eagle.member SET ? WHERE id = ?", [member, id], result);
      }
    });
  },

  updatePlaceMemberByPhone: (place_id, phone, member, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          "UPDATE eagle.member SET ? WHERE place_id = ? AND phone = ?",
          [member, place_id, phone],
          result
        );
      }
    });
  },

  deleteMember: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM eagle.member WHERE id = ?", [id], result);
      }
    });
  },

  deletePlaceMembers: (place_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM eagle.member WHERE place_id = ?", [place_id], result);
      }
    });
  }
};

module.exports = Member;
