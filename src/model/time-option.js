import connection from "db/connection";

// TimeOption object constructor
const TimeOption = {
  selectTimeOptionCount: (filter, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(`SELECT count(*) count FROM time_option a WHERE ${filter} `, [], result);
      }
    });
  },

  selectTimeOptions: (filter, order, desc, limit, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*
             FROM time_option a
            WHERE ${filter} ORDER BY ${order} ${desc} LIMIT ${limit} `,
          [],
          result
        );
      }
    });
  },

  selectTimeOption: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*
             FROM time_option a WHERE id = ? `,
          [id],
          result
        );
      }
    });
  },

  insertTimeOption: (newTimeOption, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("INSERT INTO time_option SET ? ", [newTimeOption], result);
      }
    });
  },

  updateTimeOption: (id, time_option, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("UPDATE time_option SET ? WHERE id = ?", [time_option, id], result);
      }
    });
  },

  deleteTimeOption: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM time_option WHERE id = ?", [id], result);
      }
    });
  }
};

module.exports = TimeOption;
