const connection = require("../db/connection");

// RoomFee object constructor
const RoomFee = {
  selectAllRoomFees: (place_id, channel, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.* FROM room_fee a INNER JOIN room_type b ON a.room_type_id = b.id 
            WHERE b.place_id = ? AND channel = ?
            ORDER BY a.room_type_id, day, stay_type, begin ;`,
          [place_id, channel],
          result
        );
      }
    });
  },

  selectRoomFees: (room_type_id, channel, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          "SELECT * FROM room_fee WHERE room_type_id = ? AND channel = ? ORDER BY day, stay_type, begin ",
          [room_type_id, channel],
          result
        );
      }
    });
  },

  selectRoomDayFees: (room_type_id, channel, day, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          "SELECT * FROM room_fee WHERE room_type_id = ? AND channel = ? AND day = ? ORDER BY day, stay_type, begin ",
          [room_type_id, channel, day],
          result
        );
      }
    });
  },

  selectRoomFee: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("SELECT * FROM room_fee WHERE id = ? ", id, result);
      }
    });
  },

  insertRoomFee: (newRoomFee, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          "INSERT INTO room_fee SET ? ON DUPLICATE KEY UPDATE ? ",
          [newRoomFee, newRoomFee],
          result
        );
      }
    });
  },

  updateRoomFee: (id, roomFee, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("UPDATE room_fee SET ? WHERE id = ?", [roomFee, id], result);
      }
    });
  },

  deleteRoomFee: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM room_fee WHERE id = ? ", [id], result);
      }
    });
  },

  deleteRoomFees: (room_type_id, channel, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          "DELETE FROM room_fee WHERE room_type_id = ? AND channel = ? ",
          [room_type_id, channel],
          result
        );
      }
    });
  },

  deleteAllRoomFee: (place_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          "DELETE FROM room_fee a INNER JOIN room_type b ON a.room_type_id = b.id WHERE b.place_id = ?  ",
          [place_id],
          result
        );
      }
    });
  }
};

export default RoomFee;
