import connection from "db/connection";

// Subscribe object constructor
const Subscribe = {
  selectSubscribeCount: (filter, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(`SELECT count(*) count FROM subscribe a WHERE ${filter} `, [], result);
      }
    });
  },

  selectSubscribes: (filter, order, desc, limit, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*
             FROM subscribe a
            WHERE ${filter} ORDER BY ${order} ${desc} LIMIT ${limit} `,
          [],
          result
        );
      }
    });
  },

  selectSubscribe: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*
             FROM subscribe a WHERE a.id = ? `,
          [id],
          result
        );
      }
    });
  },

  insertSubscribe: (newSubscribe, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("INSERT INTO subscribe SET ? ", [newSubscribe], result);
      }
    });
  },

  updateSubscribe: (id, subscribe, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("UPDATE subscribe SET ? WHERE id = ?", [subscribe, id], result);
      }
    });
  },

  deleteSubscribe: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM subscribe WHERE id = ?", [id], result);
      }
    });
  }
};

module.exports = Subscribe;
