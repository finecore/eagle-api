import connection from "db/connection";

// Notice object constructor
const Notice = {
  selectNoticeCount: (filter, between, begin, end, all, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT count(*) count 
             FROM notice a
            WHERE ${filter} AND ${between} BETWEEN ? AND ? ` + (all === "0" ? ` AND NOW() BETWEEN a.begin_date AND a.end_date ` : ""),
          [begin, end],
          result
        );
      }
    });
  },

  selectNotices: (filter, between, begin, end, order, desc, limit, all, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*
             FROM notice a
            WHERE ${filter} AND ${between} BETWEEN ? AND ? ` +
            (all === "0" ? ` AND NOW() BETWEEN a.begin_date AND a.end_date ` : "") +
            ` ORDER BY ${order} ${desc} LIMIT ${limit} `,
          [begin, end],
          result
        );
      }
    });
  },

  selectNotice: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*
             FROM notice a WHERE a.id = ? `,
          [id],
          result
        );
      }
    });
  },

  insertNotice: (newNotice, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("INSERT INTO notice SET ? ", [newNotice], result);
      }
    });
  },

  updateNotice: (id, notice, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("UPDATE notice SET ? WHERE id = ?", [notice, id], result);
      }
    });
  },

  deleteNotice: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM notice WHERE id = ?", [id], result);
      }
    });
  }
};

module.exports = Notice;
