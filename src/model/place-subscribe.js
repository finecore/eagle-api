import connection from "db/connection";

// PlaceSubscribe object constructor
const PlaceSubscribe = {
  selectPlaceSubscribeCount: (filter, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT count(*) count 
             FROM place_subscribe a
            INNER JOIN subscribe b ON a.subscribe_id = b.id
            WHERE ${filter} `,
          [],
          result
        );
      }
    });
  },

  selectPlaceSubscribes: (filter, order, desc, limit, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.name, b.pay_yn, b.pay_type, b.code, b.discription, b.default_fee, b.license_yn, c.name place_name, d.ota_place_name, d.ota_place_name_1, d.ota_place_name_2, d.ota_place_name_3
             FROM place_subscribe a 
            INNER JOIN subscribe b ON a.subscribe_id = b.id
            INNER JOIN place c ON a.place_id = c.id
            INNER JOIN preferences d ON a.place_id = d.place_id
            WHERE ${filter} ORDER BY ${order} ${desc} LIMIT ${limit} `,
          [],
          result
        );
      }
    });
  },

  selectPlaceSubscribe: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.name, b.pay_yn, b.pay_type, b.code, b.discription, b.default_fee, b.license_yn, c.name place_name
             FROM place_subscribe a INNER JOIN subscribe b ON a.subscribe_id = b.id INNER JOIN place c ON a.place_id = c.id
            WHERE a.id = ? `,
          [id],
          result
        );
      }
    });
  },

  insertPlaceSubscribe: (newPlaceSubscribe, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("INSERT INTO place_subscribe SET ? ", [newPlaceSubscribe], result);
      }
    });
  },

  updatePlaceSubscribe: (id, place_subscribe, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("UPDATE place_subscribe SET ? WHERE id = ?", [place_subscribe, id], result);
      }
    });
  },

  deletePlaceSubscribe: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM place_subscribe WHERE id = ?", [id], result);
      }
    });
  },
};

module.exports = PlaceSubscribe;
