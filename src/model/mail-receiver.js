const connection = require("../db/connection");

// MailReceiver object constructor
const MailReceiver = {
  selectMailReceiverCount: (filter, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(`SELECT count(*) count FROM mail_receiver a INNER JOIN user b ON a.user_id = b.id WHERE ${filter} `, [], result);
      }
    });
  },

  selectMailReceivers: (filter, order, desc, limit, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(`SELECT a.*, b.name, b.email, b.level FROM mail_receiver a INNER JOIN user b ON a.user_id = b.id WHERE ${filter} ORDER BY ${order} ${desc} LIMIT ${limit} `, [], result);
      }
    });
  },

  selectMailReceiver: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("SELECT a.*, b.name, b.email, b.level FROM mail_receiver a INNER JOIN user b ON a.user_id = b.id WHERE id = ? ", id, result);
      }
    });
  },

  insertMailReceiver: (mail_receiver, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("INSERT INTO mail_receiver SET ? ON DUPLICATE KEY UPDATE ? ", [mail_receiver, mail_receiver], result);
      }
    });
  },

  updateMailReceiver: (id, mail_receiver, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("UPDATE mail_receiver SET ? WHERE id = ? ", [mail_receiver, id], result);
      }
    });
  },

  deleteMailReceiver: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM mail_receiver WHERE id = ?", [id], result);
      }
    });
  },
};

module.exports = MailReceiver;
