const connection = require("../db/connection");

// UserPlace object constructor
const UserPlace = {
  selectUserPlaceCount: (filter, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT count(*) count 
             FROM user_place a INNER JOIN user b ON a.user_id = b.id INNER JOIN place c ON a.place_id = c.id
            WHERE ${filter} `,
          [],
          result
        );
      }
    });
  },

  selectUserPlaces: (filter, order, desc, limit, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.name user_name, c.name place_name, c.sido, c.gugun, c.company_id
             FROM user_place a INNER JOIN user b ON a.user_id = b.id INNER JOIN place c ON a.place_id = c.id 
            WHERE ${filter} 
            ORDER BY ${order} ${desc} 
            LIMIT ${limit} `,
          [],
          result
        );
      }
    });
  },

  selectUserPlace: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("SELECT * FROM user_place WHERE id = ? ", id, result);
      }
    });
  },

  insertUserPlace: (newUserPlace, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("INSERT INTO user_place SET ? ON DUPLICATE KEY UPDATE ? ", [newUserPlace, newUserPlace], result);
      }
    });
  },

  updateUserPlace: (id, user_place, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("UPDATE user_place SET ? WHERE id = ? ", [user_place, id], result);
      }
    });
  },

  deleteUserPlace: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM user_place WHERE id = ?", [id], result);
      }
    });
  },

  deletePlaceUserPlaces: (place_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM user_place WHERE place_id = ?", [place_id], result);
      }
    });
  },
};

module.exports = UserPlace;
