const connection = require("../db/connection");

// Dashboard object constructor
const Dashboard = {
  selectTotalPlaceSido: (result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT sido, COUNT(*) count  
             FROM place 
            WHERE id > 1
            GROUP BY sido 
             WITH ROLLUP; `,
          [],
          result
        );
      }
    });
  },
  selectTotalNowIeg: (result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT page_reload connect, COUNT(*) count  
             FROM preferences a INNER JOIN place b ON a.place_id = b.id 
            WHERE a.place_id > 1
            GROUP BY page_reload
             WITH ROLLUP; `,
          [],
          result
        );
      }
    });
  },
  selectListNowIeg: (filter, order, desc, limit, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.name, b.last_event_date
             FROM preferences a INNER JOIN place b ON a.place_id = b.id
            WHERE ${filter} ORDER BY ${order} ${desc} LIMIT ${limit} ; `,
          [],
          result
        );
      }
    });
  },
  selectTotalNowIsg: (result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT sale_stop, COUNT(*) count  
             FROM device a INNER JOIN place b ON a.place_id = b.id 
             WHERE a.type = '02' 
             GROUP BY sale_stop WITH ROLLUP; `,
          [],
          result
        );
      }
    });
  },
  selectSumPayTypeIsg: (result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT sale_stop, COUNT(*) count  
             FROM device a INNER JOIN place b ON a.place_id = b.id 
             WHERE a.type = '02' 
             GROUP BY sale_stop WITH ROLLUP; `,
          [],
          result
        );
      }
    });
  },
  selectListNowIsg: (filter, order, desc, limit, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.name place_name, b.last_event_date
             FROM device a INNER JOIN place b ON a.place_id = b.id
            WHERE a.type = '02' AND ${filter} ORDER BY ${order} ${desc} LIMIT ${limit} ; `,
          [],
          result
        );
      }
    });
  },
  selectTotalReservSum: (filter, begin, end, format, groups, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT DATE_FORMAT(check_in_exp, ${format}) date, ${groups}, COUNT(*) count  
             FROM room_reserv 
             WHERE ${filter} AND state != 'B' 
               AND DATE_FORMAT(check_in_exp, ${format}) >= DATE_FORMAT(?, ${format})  
               AND DATE_FORMAT(check_in_exp, ${format}) <= DATE_FORMAT(?, ${format})
             GROUP BY DATE_FORMAT(check_in_exp, ${format}), ${groups} ; `,
          [begin, end],
          result
        );
      }
    });
  },
  selectTotalRoomStatusSum: (filter, groups, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT \`${groups}\`, COUNT(*) count  
             FROM room_state a INNER JOIN room b ON a.room_id = b.id
             WHERE ${filter}
             GROUP BY \`${groups}\` ; `,
          [],
          result
        );
      }
    });
  },
  selectTotalIsgStatusSum: (filter, groups, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT ${groups}, COUNT(*) count  
             FROM isc_state a INNER JOIN device b ON a.serialno = b.serialno
             WHERE ${filter}
             GROUP BY ${groups} ; `,
          [],
          result
        );
      }
    });
  },
  selectListNowEvent: (filter, limit, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.name, c.name place_name
             FROM (SELECT * FROM eagle.room_state_log  WHERE ${filter} ORDER BY id DESC LIMIT ${limit} ) AS a
            INNER JOIN room b ON a.room_id = b.id 
            INNER JOIN place c ON b.place_id = c.id ;`,
          [],
          result
        );
      }
    });
  },
  selectTotalReportIeg: (filter, begin, end, format, groups, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT DATE_FORMAT(reg_date, ${format})  date, ${groups}, COUNT(*) count  
             FROM preferences a
             WHERE ${filter}
               AND DATE_FORMAT(reg_date, ${format}) >= DATE_FORMAT(?, ${format})  
               AND DATE_FORMAT(reg_date, ${format}) <= DATE_FORMAT(?, ${format})
             GROUP BY DATE_FORMAT(reg_date, ${format}), ${groups} WITH ROLLUP; `,
          [begin, end],
          result
        );
      }
    });
  },
  selectTotalReportIsg: (filter, begin, end, format, groups, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT DATE_FORMAT(a.reg_date, ${format}) date, ${groups}, COUNT(*) count  
             FROM device a INNER JOIN place b ON a.place_id = b.id
             WHERE ${filter}
               AND a.type = '02' 
               AND DATE_FORMAT(a.reg_date, ${format}) >= DATE_FORMAT(?, ${format})  
               AND DATE_FORMAT(a.reg_date, ${format}) <= DATE_FORMAT(?, ${format})
             GROUP BY DATE_FORMAT(a.reg_date, ${format}), ${groups} WITH ROLLUP; `,
          [begin, end],
          result
        );
      }
    });
  },
};

module.exports = Dashboard;
