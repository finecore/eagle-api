import connection from "db/connection";
import _ from "lodash";

// RoomSaleReport object constructor
const RoomSaleReportHour = {
  selectRoomSaleReportHours: (room_id, filter, between, begin, end, order, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT * FROM room_sale_report_hour 
          WHERE room_id = ${room_id} AND ${filter} AND ${between} BETWEEN ${begin} AND ${end}
          ORDER BY ${order}`,
          [],
          result
        );
      }
    });
  },

  selectRoomSaleReportHourLast: (result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT * FROM room_sale_report_hour 
          ORDER BY year DESC, month DESC, day DESC, hour DESC LIMIT 1;`,
          [],
          result
        );
      }
    });
  },

  selectRoomSaleReportHour: (room_id, year, month, day, hour, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT * FROM room_sale_report_hour 
            WHERE room_id = ? AND year = ? AND month = ? AND day = ? AND hour = ? ;`,
          [room_id, year, month, day, hour],
          result
        );
      }
    });
  },

  selectRoomSaleReportHourPlace: (place_id, sdate, edate, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.name
             FROM room_sale_report_hour a INNER JOIN room b ON a.room_id = b.id
            WHERE DATE_FORMAT(CONCAT(year, '-', month, '-', day, ' ', hour), '%Y%m%d%H') >= DATE_FORMAT(?, '%Y%m%d%H')  
                AND DATE_FORMAT(CONCAT(year, '-', month, '-', day, ' ', hour), '%Y%m%d%H') < DATE_FORMAT(?, '%Y%m%d%H')  
                AND b.place_id = ?
            ORDER BY a.room_id ;`,
          [sdate, edate, place_id],
          result
        );
      }
    });
  },

  insertRoomSaleReportHour: (newSaleReportList, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        let sql = "INSERT INTO room_sale_report_hour (year,month,day,hour,room_id,default_fee,add_fee,ieg_rent_cnt,ieg_stay_cnt,ieg_pay_cash_amt) ";
        sql += "VALUES";

        _.map(newSaleReportList, (report, idx) => {
          let { year, month, day, hour, room_id, default_fee, add_fee, ieg_rent_cnt, ieg_stay_cnt, ieg_pay_cash_amt } = report;

          if (idx > 0) sql += ",";

          sql += `(${year}, ${month}, ${day}, ${hour}, ${room_id}, ${default_fee}, ${add_fee}, ${ieg_rent_cnt}, ${ieg_stay_cnt}, ${ieg_pay_cash_amt})`;
        });

        sql += ";";

        _connection.sql(sql, [], (err, info) => {
          result(err, info);
        });
      }
    });
  },

  updateRoomSaleReportHour: (id, deviceReport, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("UPDATE room_sale_report_hour SET ? WHERE id = ?", [deviceReport, id], result);
      }
    });
  },

  deleteRoomSaleReportHour: (day, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `DELETE FROM room_sale_report_hour 
            WHERE DATE_FORMAT(reg_date, '%Y%m%d%H') >= DATE_FORMAT(DATE_ADD(NOW(),  INTERVAL ? DAY), '%Y%m%d%H');`,
          [day],
          result
        );
      }
    });
  },

  deleteRoomSaleOldReportHour: (day, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `DELETE FROM room_sale_report_hour 
            WHERE DATE_FORMAT(CONCAT(year, '-', month, '-', day), '%Y%m%d') <= DATE_FORMAT(DATE_ADD(NOW(), INTERVAL ? DAY), '%Y%m%d');`,
          [day],
          result
        );
      }
    });
  },
};

export default RoomSaleReportHour;
