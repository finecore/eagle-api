const connection = require("../db/connection");

// Mileage object constructor
const Mileage = {
  selectMileageCount: (filter, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT count(*) count 
             FROM mileage a INNER JOIN place b ON a.place_id = b.id LEFT JOIN eagle.member c ON a.member_id = c.id
            WHERE ${filter} `,
          [],
          result
        );
      }
    });
  },

  selectMileages: (filter, order, desc, limit, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.name as place_name, c.name as member_name
           FROM mileage a INNER JOIN place b ON a.place_id = b.id 
                          LEFT JOIN eagle.member c ON a.member_id = c.id
           WHERE ${filter} 
           ORDER BY ${order} ${desc} 
           LIMIT ${limit} `,
          [],
          result
        );
      }
    });
  },

  selectPlaceMileageByPhone: (place_id, phone, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.name as place_name, c.name as member_name
           FROM mileage a INNER JOIN place b ON a.place_id = b.id 
                          LEFT JOIN eagle.member c ON a.member_id = c.id
            WHERE a.place_id = ? AND a.phone = ?`,
          [place_id, phone],
          result
        );
      }
    });
  },

  selectPlaceMileages: (place_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.name as place_name, c.name as member_name
           FROM mileage a INNER JOIN place b ON a.place_id = b.id 
                          LEFT JOIN eagle.member c ON a.member_id = c.id
           WHERE a.place_id = ? `,
          [place_id],
          result
        );
      }
    });
  },

  selectMileage: (id, phone, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.name as place_name, c.name as member_name
           FROM mileage a INNER JOIN place b ON a.place_id = b.id 
                          LEFT JOIN eagle.member c ON a.member_id = c.id
            WHERE a.id = ? OR a.phone = ?`,
          [id, phone],
          result
        );
      }
    });
  },

  insertMileage: (mileage, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("INSERT INTO mileage SET ? ", [mileage], result);
      }
    });
  },

  updateMileage: (id, mileage, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("UPDATE mileage SET ? WHERE id = ?", [mileage, id], result);
      }
    });
  },

  updatePlaceMileageByPhone: (place_id, phone, mileage, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          "UPDATE mileage SET ? WHERE place_id = ? AND phone = ?",
          [mileage, place_id, phone],
          result
        );
      }
    });
  },

  increaseMileage: (id, point, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(`UPDATE mileage SET point = point + ${point} WHERE id = ? `, [id], result);
      }
    });
  },

  decreaseMileage: (id, point, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(`UPDATE mileage SET point = point - ${point} WHERE id = ? `, [id], result);
      }
    });
  },

  deleteMileage: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM mileage WHERE id = ?", [id], result);
      }
    });
  },

  deletePlaceMileages: (place_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM mileage WHERE place_id = ?", [place_id], result);
      }
    });
  }
};

module.exports = Mileage;
