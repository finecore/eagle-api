import connection from "db/connection";

// RoomSaleReport object constructor
const RoomSaleReportYear = {
  selectRoomSaleReportYears: (room_id, filter, between, begin, end, order, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT * FROM room_sale_report_year 
          WHERE room_id = ${room_id} AND ${filter} AND ${between} BETWEEN ${begin} AND ${end}
          ORDER BY ${order}`,
          [],
          result
        );
      }
    });
  },

  selectRoomSaleReportYear: (room_id, year, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT * FROM room_sale_report_year 
            WHERE room_id = ? AND year = ?;`,
          [room_id, year],
          result
        );
      }
    });
  },

  selectRoomSaleReportYearSum: (sdate, edate, order, filter, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT year, 
                  SUM(ieg_long_cnt) ieg_long_cnt, SUM(ieg_stay_cnt) ieg_stay_cnt, SUM(ieg_rent_cnt) ieg_rent_cnt, 
                  SUM(isg_long_cnt) isg_long_cnt, SUM(isg_stay_cnt) isg_stay_cnt, SUM(isg_rent_cnt) isg_rent_cnt, 
                  SUM(reserv_cnt) reserv_cnt,
                   SUM(prepay_ota_amt) prepay_ota_amt, SUM(prepay_cash_amt) prepay_cash_amt, SUM(prepay_card_amt) prepay_card_amt, 
                  SUM(ieg_pay_cash_amt) ieg_pay_cash_amt, SUM(ieg_pay_card_amt) ieg_pay_card_amt,
                  SUM(isg_pay_cash_amt) isg_pay_cash_amt, SUM(isg_pay_card_amt) isg_pay_card_amt
             FROM room_sale_report_year 
            WHERE year >= DATE_FORMAT(?, '%Y%m%d')  
                AND year <= DATE_FORMAT(?, '%Y%m%d')  
                AND ${filter || "1=1"}
             GROUP BY year  
            ORDER BY ${order};`,
          [sdate, edate],
          result
        );
      }
    });
  },

  insertRoomSaleReportYear: (syear, eyear, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `INSERT  INTO room_sale_report_year (year, room_id, ieg_stay_cnt, ieg_rent_cnt, isg_stay_cnt, isg_rent_cnt, prepay_ota_amt, prepay_cash_amt, prepay_card_amt, ieg_pay_cash_amt, ieg_pay_card_amt, isg_pay_cash_amt, isg_pay_card_amt) 
            SELECT year, room_id, ieg_stay_cnt, ieg_rent_cnt, isg_stay_cnt, isg_rent_cnt, prepay_ota_amt, prepay_cash_amt, prepay_card_amt, ieg_pay_cash_amt, ieg_pay_card_amt, isg_pay_cash_amt, isg_pay_card_amt 
            FROM ( 
              SELECT year, room_id, 
                  SUM(ieg_long_cnt) ieg_long_cnt, SUM(ieg_stay_cnt) ieg_stay_cnt, SUM(ieg_rent_cnt) ieg_rent_cnt, 
                  SUM(isg_long_cnt) isg_long_cnt, SUM(isg_stay_cnt) isg_stay_cnt, SUM(isg_rent_cnt) isg_rent_cnt,
                  SUM(prepay_ota_amt) prepay_ota_amt, SUM(prepay_cash_amt) prepay_cash_amt, SUM(prepay_card_amt) prepay_card_amt, 
                  SUM(ieg_pay_cash_amt) ieg_pay_cash_amt, SUM(ieg_pay_card_amt) ieg_pay_card_amt,
                  SUM(isg_pay_cash_amt) isg_pay_cash_amt, SUM(isg_pay_card_amt) isg_pay_card_amt
                FROM room_sale_report_month  
            	 WHERE year >= DATE_FORMAT(DATE_ADD(NOW(), INTERVAL ? YEAR), '%Y') 
                 AND year <= DATE_FORMAT(DATE_ADD(NOW(), INTERVAL ? YEAR), '%Y') 
            	 GROUP BY year, room_id 
            )  AS T 
            ON DUPLICATE KEY UPDATE ieg_stay_cnt=T.ieg_stay_cnt, ieg_rent_cnt=T.ieg_rent_cnt, isg_stay_cnt=T.isg_stay_cnt, isg_rent_cnt=T.isg_rent_cnt,
            prepay_ota_amt=T.prepay_ota_amt, prepay_cash_amt=T.prepay_cash_amt, prepay_card_amt=T.prepay_card_amt, isg_pay_cash_amt=T.isg_pay_cash_amt, reg_date=now(); `,
          [syear, eyear],
          (err, info) => {
            result(err, info);
          }
        );
      }
    });
  },

  updateRoomSaleReportYear: (id, deviceReport, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("UPDATE room_sale_report_year SET ? WHERE id = ?", [deviceReport, id], result);
      }
    });
  },

  deleteRoomSaleReportYear: (date, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `DELETE FROM room_sale_report_year 
            WHERE year = DATE_FORMAT(?, '%Y');`,
          [date],
          result
        );
      }
    });
  },

  deleteRoomSaleOldReportYear: (year, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `DELETE FROM room_sale_report_year 
            WHERE year <= DATE_FORMAT(DATE_ADD(NOW(), INTERVAL ? YEAR), '%Y');`,
          [year],
          result
        );
      }
    });
  },
};

export default RoomSaleReportYear;
