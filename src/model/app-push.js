import connection from "db/connection";
import _ from "lodash";

// AppPush object constructor
const AppPush = {
  selectAppPushCount: (filter, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT count(*) count  
             FROM app_push a INNER JOIN app_info b ON a.token = b.token INNER JOIN user c ON b.user_id = c.id 
           WHERE ${filter} `,
          [],
          result
        );
      }
    });
  },

  selectAppPushs: (filter, limit, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.user_id, b.channel, b.os, b.version, b.badge, c.name, c.level
           FROM app_push a INNER JOIN app_info b ON a.token = b.token INNER JOIN user c ON b.user_id = c.id
           WHERE ${filter} 
           ORDER BY a.id desc 
           LIMIT ${limit} `,
          [],
          result
        );
      }
    });
  },

  selectTokenAppPushs: (token, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.user_id, b.channel, b.os, b.version, b.badge, c.name, c.level
           FROM app_push a INNER JOIN app_info b ON a.token = b.token INNER JOIN user c ON b.user_id = c.id
           WHERE a.token = ? 
           ORDER BY a.id desc `,
          [token],
          result
        );
      }
    });
  },

  selectAppPush: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.user_id, b.channel, b.os, b.version, b.badge, c.name, c.level
             FROM app_push a INNER JOIN app_info b ON a.token = b.token INNER JOIN user c ON b.user_id = c.id 
            WHERE a.id = ? `,
          id,
          result
        );
      }
    });
  },

  insertAppPush: (app_push, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          "INSERT INTO app_push SET ?  ON DUPLICATE KEY UPDATE ? ",
          [app_push, app_push],
          (err, push) => {
            result(err, push);
          }
        );
      }
    });
  },

  updateAppPush: (id, app_push, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("UPDATE app_push SET ? WHERE id = ? ", [app_push, id], (err, push) => {
          result(err, push);
        });
      }
    });
  },

  updateTokenAppPush: (token, app_push, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          "UPDATE app_push SET ? WHERE token = ? ",
          [app_push, token],
          (err, push) => {
            result(err, push);
          }
        );
      }
    });
  },

  deleteAppPush: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM app_push WHERE id = ? ", [id], result);
      }
    });
  },

  deleteTokenAppPush: (token, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM app_push WHERE token = ? ", [token], result);
      }
    });
  },

  deleteOldAppPush: (day, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          "DELETE FROM app_push WHERE reg_date < DATE_ADD(NOW(), INTERVAL ? DAY);  ",
          [day],
          result
        );
      }
    });
  }
};

export default AppPush;
