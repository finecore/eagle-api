import connection from "db/connection";

// File object constructor
const File = {
  selectFileCount: (filter, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(`SELECT count(*) count FROM file a WHERE ${filter} `, [], result);
      }
    });
  },

  selectFiles: (filter, order, desc, limit, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(`SELECT * FROM file a WHERE ${filter} ORDER BY ${order} ${desc} LIMIT ${limit} `, [], result);
      }
    });
  },

  selectFile: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(`SELECT * FROM file a WHERE id = ? `, [id], result);
      }
    });
  },

  selectFileByName: (path, name, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(`SELECT * FROM file a WHERE path = ? AND name = ? `, [path, name], result);
      }
    });
  },

  insertFile: (newFile, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("INSERT INTO file SET ? ON DUPLICATE KEY UPDATE ? ", [newFile, newFile], result);
      }
    });
  },

  updateFile: (id, file, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("UPDATE file SET ? WHERE id = ?", [file, id], result);
      }
    });
  },

  deleteFile: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM file WHERE id = ?", [id], result);
      }
    });
  }
};

module.exports = File;
