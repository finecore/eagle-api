const connection = require("../db/connection");

// RoomInterrupt object constructor
const RoomInterrupt = {
  selectAllRoomInterrupts: result => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          "SELECT a.*, b.place_id FROM room_interrupt a INNER JOIN room b ON a.room_id = b.id ;",
          [],
          result
        );
      }
    });
  },

  selectPlaceRoomInterrupts: (place_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          "SELECT a.*, b.place_id FROM room_interrupt a INNER JOIN room b ON a.room_id = b.id WHERE b.place_id = ? AND (a.state = 1 OR a.sale = 1) ",
          place_id,
          result
        );
      }
    });
  },

  selectRoomInterrupt: (room_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          "SELECT a.*, b.place_id FROM room_interrupt a INNER JOIN room b ON a.room_id = b.id WHERE a.room_id = ?  AND (a.state = 1 OR a.sale = 1) ",
          room_id,
          result
        );
      }
    });
  },

  selectUserRoomInterrupt: (user_id, channel, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          "SELECT a.*, b.place_id FROM room_interrupt a INNER JOIN room b ON a.room_id = b.id WHERE a.user_id = ? AND channel = ? AND (a.state = 1 OR a.sale = 1) ",
          [user_id, channel],
          result
        );
      }
    });
  },

  insertRoomInterrupt: (newRoomInterrupt, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          "INSERT INTO room_interrupt SET ? ON DUPLICATE KEY UPDATE ? ",
          [newRoomInterrupt, newRoomInterrupt],
          result
        );
      }
    });
  },

  updateRoomInterrupt: (room_id, roomInterrupt, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          "UPDATE room_interrupt SET ? WHERE room_id = ?",
          [roomInterrupt, room_id],
          result
        );
      }
    });
  },

  deleteRoomInterrupt: (room_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM room_interrupt WHERE room_id = ?", [room_id], result);
      }
    });
  },

  deleteUserRoomInterrupt: (user_id, channel, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          "DELETE FROM room_interrupt WHERE user_id = ? AND channel = ?",
          [user_id, channel],
          result
        );
      }
    });
  },

  deleteAllRoomInterrupts: (place_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          "DELETE FROM room_interrupt a INNER JOIN room b ON a.room_id = a.id WHERE place_id = ?",
          [place_id],
          result
        );
      }
    });
  }
};

export default RoomInterrupt;
