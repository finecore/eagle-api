const connection = require("../db/connection");

// SocketLog object constructor
const SocketLog = {
  putSocketLog: (newSocketLog, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("INSERT INTO socket_log SET ? ", newSocketLog, result);
      }
    });
  },

  updateSocketLog: (id, socketLog, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          "UPDATE socket_log SET ? WHERE id = ?",
          [socketLog, id],
          result
        );
      }
    });
  },

  deleteSocketLog: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM socket_log WHERE id = ?", [id], result);
      }
    });
  },

  getAllSocketLog: result => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("SELECT * FROM socket_log", result);
      }
    });
  },

  getSocketLogById: (socketLogId, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          "Select socketLog from socket_log WHERE id = ? ",
          socketLogId,
          result
        );
      }
    });
  }
};

export default SocketLog;
