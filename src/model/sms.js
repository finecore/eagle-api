const connection = require("../db/connection");

// Sms object constructor
const Sms = {
  selectSmsCount: (filter, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT count(*) count 
             FROM SC_TRAN a LEFT JOIN member b ON a.TR_PHONE = b.hp
            WHERE ${filter} `,
          [],
          result
        );
      }
    });
  },

  selectSmses: (filter, order, desc, limit, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.name
           FROM SC_TRAN a LEFT JOIN member b ON a.TR_PHONE = b.hp 
           WHERE ${filter} 
           ORDER BY ${order} ${desc} 
           LIMIT ${limit} `,
          [],
          result
        );
      }
    });
  },

  selectSms: (tr_num, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql(
          `SELECT a.*, b.name
           FROM SC_TRAN a LEFT JOIN member b ON a.TR_PHONE = b.hp 
            WHERE a.tr_num = ?`,
          [tr_num],
          result
        );
      }
    });
  },

  insertSms: (sms, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("INSERT INTO SC_TRAN SET ? ", [sms, sms], result);
      }
    });
  },

  updateSms: (tr_num, sms, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("UPDATE SC_TRAN SET ? WHERE tr_num = ?", [sms, tr_num], result);
      }
    });
  },

  deleteSms: (tr_num, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM SC_TRAN WHERE tr_num = ?", [tr_num], result);
      }
    });
  },
};

module.exports = Sms;
