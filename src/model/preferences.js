const connection = require("../db/connection");

// Preferences object constructor
const Preferences = {
  selectAllPreferences: result => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("SELECT a.*, b.name place_name FROM preferences a INNER JOIN place b ON a.place_id = b.id ", [], result);
      }
    });
  },

  selectPreferences: (place_id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("SELECT a.*, b.name place_name FROM preferences a INNER JOIN place b ON a.place_id = b.id WHERE place_id = ? ", place_id, result);
      }
    });
  },

  insertPreferences: (newPreferences, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("INSERT INTO preferences SET ? ON DUPLICATE KEY UPDATE ? ", [newPreferences, newPreferences], result);
      }
    });
  },

  updatePreferences: (id, preferences, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("UPDATE preferences SET ? WHERE id = ?", [preferences, id], result);
      }
    });
  },

  deletePreferences: (id, result) => {
    connection((_err, _connection) => {
      if (_err) result(_err, null);
      else {
        _connection.sql("DELETE FROM preferences WHERE id = ?", [id], result);
      }
    });
  },
};

module.exports = Preferences;
