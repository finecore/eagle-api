import moment from "moment";
import _ from "lodash";
import chalk from "chalk";
import format from "utils/format-util";

import RoomReserv from "model/room-reserv";
import RoomType from "model/room-type";

import { ERROR, PREFERENCES } from "constants/constants";
import { keyToValue, keyCodes } from "constants/key-map";

export const hotelnow_fix = (ota, reserv, line, place, callback) => {
  console.log(chalk.yellow("- hotelnow_fix"), line);

  //  0  [호텔나우] 예약 알림
  //  1  더 스테이 송도 호텔 예약 알림
  //  2  호텔나우 예약번호: 13784656
  //  3  투숙자: 이 ** 호
  //  4  연락처: 0507 -**** -2396
  //  5  체크인: 2022 - 01 - 13(1박)
  //  6  체크아웃: 2022 - 01 - 14
  //  7  룸타입: [ 특가야놀자☆ ]디럭스 트윈
  //  8  객실수: 1
  //  9  입금가: 60, 900 원
  // 10  판매가: 70, 000 원
  // 11  최종매출가: 70, 000 원

  // * 체크인 : 2022 - 01 - 13 해당 룸타입 모두 판매되었습니다.객실 추가 등록 부탁드립니다.
  // * 야놀자 예약 고객입니다.
  // * 050 안심번호는 전화 및 문자 연락 가능한 번호이며, 체크아웃 이후 5일까지 유효합니다.
  // * 자세한 예약 내용은 엑스트라넷에서 확인 부탁드립니다.

  let stay_type = line[5] ? line[5].match(/(\d{1,2})박/) : null;

  if (stay_type) {
    console.log(chalk.yellow("- stay_type"), stay_type[1]);
    reserv.stay_type = stay_type[1] > 0 ? 1 : 2;
  }

  let reserv_num = line[2] ? line[2].split(/:/) : null;

  if (reserv_num[1]) {
    console.log(chalk.yellow("- reserv_num"), reserv_num[1]);
    reserv.reserv_num = Number(reserv_num[1]);
  }

  // 고객명
  let name = line[3] ? line[3].split(/:/) : null;
  if (name) {
    console.log(chalk.yellow("- name"), name[1]);
    reserv.name = name[1];
  }

  let hp = line[4] ? line[4].split(/:/) : null;
  if (hp) {
    console.log(chalk.yellow("- hp"), hp[1]);
    reserv.hp = hp[1];
  }

  let ci = line[5] ? line[5].match(/(?::)(\d{4})-?(\d{2})-?(\d{2})/) : null;
  let co = line[6] ? line[6].match(/(?::)(\d{4})-?(\d{2})-?(\d{2})/) : null;

  console.log(chalk.yellow("- ci"), ci);
  console.log(chalk.yellow("- co"), co);

  if (ci && co) {
    reserv.check_in_exp = `${ci[1]}-${ci[2]}-${ci[3]}`;
    reserv.check_out_exp = `${co[1]}-${co[2]}-${co[3]}`;

    ota.setCheckInOutTime(reserv, () => {
      console.log(chalk.yellow("- check_in_exp"), reserv.check_in_exp);
      console.log(chalk.yellow("- check_out_exp"), reserv.check_out_exp);
    });
  }

  let visit_type = 1;
  if (visit_type) {
    console.log(chalk.yellow("- visit_type"), visit_type);
    reserv.visit_type = visit_type; // visit_type[0] === "도보" ? 1 : 2;
  }

  let prepay_ota_amt = line[9] ? line[9].match(/([0-9,?]+)(원|만원)+/) : null;

  if (prepay_ota_amt) {
    console.log(chalk.yellow("- prepay_ota_amt"), prepay_ota_amt[1]);
    reserv.prepay_ota_amt = Number(prepay_ota_amt[1].replace(/[^\d]/g, ""));
  }

  if (place && place.place_id && line[7]) {
    let name = line[7].split(/:/)[1];
    let typeName = name.replace(/\([^)]*\)?/g, "");

    console.log(chalk.yellow("- typeName"), typeName);

    // 한글,영문,숫자만
    var regexp = new RegExp(/[^ㄱ-ㅎ|가-힣|a-z|0-9|]/gi);

    typeName = typeName.replace(regexp, "");

    ota.getRoomTypes(reserv.place_id, (roomTypes) => {
      let roomType = _.findLast(roomTypes, (v) => {
        let name = v.name.replace(regexp, "");
        let ota_name_1 = v.ota_name_1 ? v.ota_name_1.replace(regexp, "") : "";
        let ota_name_2 = v.ota_name_2 ? v.ota_name_2.replace(regexp, "") : "";
        let ota_name_3 = v.ota_name_3 ? v.ota_name_3.replace(regexp, "") : "";
        let ota_name_4 = v.ota_name_4 ? v.ota_name_4.replace(regexp, "") : "";
        let ota_name_5 = v.ota_name_5 ? v.ota_name_5.replace(regexp, "") : "";

        return name === typeName || ota_name_1 === typeName || ota_name_2 === typeName || ota_name_3 === typeName || ota_name_4 === typeName || ota_name_5 === typeName;
      });

      if (!roomType && process.env.REACT_APP_MODE !== "PROD") {
        typeName = roomTypes[Math.floor(Math.random() * roomTypes.length)];
        console.log(chalk.red("- TEST 객실 타입 전환"), typeName);
      }

      console.log(chalk.yellow("- roomType"), roomType ? roomType.name : "no room type");

      if (roomType) {
        let { id, name, default_fee_stay, default_fee_rent, reserv_discount_fee_stay, reserv_discount_fee_rent } = roomType;

        reserv.room_type_id = id;
        reserv.room_type_name = name;

        ota.setRoomFee(reserv, (add_fee) => {
          let room_fee = reserv.stay_type === 2 ? default_fee_rent : default_fee_stay; //  기본 요금
          let reserv_fee = reserv.stay_type === 2 ? default_fee_rent - reserv_discount_fee_rent : default_fee_stay - reserv_discount_fee_stay; // 예약 할인 요금

          // 요일/시간별 추가/할인 요금 적용.
          room_fee += add_fee;

          if (reserv.prepay_ota_amt > 0) {
            reserv_fee = reserv.prepay_ota_amt; // OTA 결제 시 결제 요금을 예약 가로 설정 한다.(잔금 발생 방지)
          } else {
            reserv_fee += add_fee; // OTA 결제 시 옵션 추가 요금은 적용 하지 않는다.
          }

          reserv.room_fee = room_fee;
          reserv.reserv_fee = reserv_fee;
          if (reserv.stat === "A") reserv.memo = line[4]; // 객실 타입 정보 상세 추가.

          callback({ reserv });
        });
      } else {
        callback({ reserv });
      }
    });
  } else {
    callback({ reserv });
  }
};

export const hotelnow_cancel = (ota, reserv, line, place, callback) => {
  console.log(chalk.yellow("- hotelnow_cancel"), line);

  // 0 [ 호텔나우 ] 예약 취소 알림
  // 1 더 스테이 송도 호텔 예약 취소 알림
  // 2 호텔나우 예약번호: 13703347
  // 3 투숙자: 김 ** 빈
  // 4 연락처: 0503 -**** -4616
  // 5 체크인: 2022 - 01 - 17(1박)
  // 6 체크아웃: 2022 - 01 - 18
  // 7 룸타입: 스탠다드
  // 8 객실수: 1
  // 9 입금가: 33, 768 원

  let reserv_num = line[2] ? line[2].split(/:/) : null;

  if (reserv_num[1]) {
    console.log(chalk.yellow("- reserv_num"), reserv_num[1]);
    reserv.reserv_num = Number(reserv_num[1]);

    ota.getReservByReservNum(reserv.reserv_num, (room_reserv) => {
      if (room_reserv) {
        reserv = _.merge({}, reserv, room_reserv);
      }
      reserv.state = "B";

      callback({ reserv });
    });
  } else {
    callback({ reserv });
  }
};
