const fs = require("fs");
const path = require("path");
const mkdirp = require("mkdirp");
const _ = require("lodash");

const multer = require("multer"); // multer모듈 적용 (for 파일업로드)

const File = require("model/file");

const BASE_DIR = process.env.FILE_BASE_PAHT;

/*
// 현재 파일 경로
__filename; // D:\workspace\diagram\main.js
// 현재 디렉토리
__dirname; // D:\workspace\diagram
// 경로 연결
path.join(__dirname, '/test') // /home/dirname/test
// 상대적인 경로로 연결.
path.resolve('/foo/bar', './baz') // /foo/bar/baz
// 경로에 해당하는 속성을 가져온다.(root, dir, base, ext, name)
path.parse("/usr/local/test.jpg")
// 경로에서 파일명만 가져오기
path.basename('/foo/bar/baz/asdf/quux.html') // Returns: 'quux.html'
path.basename('/foo/bar/baz/asdf/quux.html', '.html') // Returns: 'quux'
// 디렉토리 이동. cwd 변경.
process.chdir('/tmp');
// 파일이동, 이름 변경
fs.renameSync(oldPath, newPath);
// 파일 삭제
fs.unlinkSync(path);
*/

const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    const dir = BASE_DIR + "/" + req.body.path;

    console.log("--- file upload dir", dir);

    // 폴더 없으면 생성 한다.
    mkdirp(dir, function(err) {
      // cb 콜백함수를 통해 전송된 파일 저장 디렉토리 설정
      if (!err) cb(null, dir);
      else console.error(err);
    });
  },
  filename: function(req, file, cb) {
    file.uploadedFile = {
      name: req.body.name,
      ext: _.last(file.originalname.split("."))
    };

    let extInx = file.uploadedFile.name.indexOf(file.uploadedFile.ext);
    if (extInx > -1) file.uploadedFile.name = file.uploadedFile.name.substring(0, extInx - 1);

    console.log("--- file uploadedFile", file.uploadedFile, extInx);

    cb(null, file.uploadedFile.name + "." + file.uploadedFile.ext);
  }
});

const fileFilter = function(req, file, cb) {
  console.log("--- file fileFilter", file);

  // 이 함수는 boolean 값과 함께 `cb`를 호출함으로써 해당 파일을 업로드 할지 여부를 나타낼 수 있습니다.
  // 이 파일을 거부하려면 다음과 같이 `false` 를 전달합니다:
  // cb(null, false);

  // 이 파일을 허용하려면 다음과 같이 `true` 를 전달합니다:
  cb(null, true);

  // 무언가 문제가 생겼다면 언제나 에러를 전달할 수 있습니다:
  // cb(new Error("I don't have a clue!"));
};

// multer라는 모듈이 함수라서 함수에 옵션을 줘서 실행을 시키면, 해당 함수는 미들웨어를 리턴한다.(여러 옵션이 있다.)
const upload = multer({ storage: storage, fileFilter: fileFilter });

const rename = (req, res, next) => {
  const oldPath = BASE_DIR + "/" + req.params.path + "/" + req.params.name;
  const newPath = BASE_DIR + "/" + req.params.path + "/" + req.params.newname;

  console.log("--- file rename oldPath", oldPath, "newPath", newPath);

  // 파일이동, 이름 변경
  fs.renameSync(oldPath, newPath);
};

const remove = (dir, name, cb) => {
  dir = BASE_DIR + "/" + dir;

  const file_path = dir + "/" + name;

  console.log("--- file remove ", file_path);

  // 파일 삭제
  fs.unlink(file_path, function(err) {
    if (err) {
      console.error(err);
      if (cb) cb(err);
    } else {
      console.log("- File deleted!");

      try {
        var files = fs.readdirSync(dir); // 디렉토리를 읽어온다

        // 파일이 없다면 현재 대렉터리 삭제.
        if (!files.length) {
          // let arr = dir.split("/");
          // let parent = arr.slice(0, arr.length - 1).join("/");
          console.log("- File remove dir", dir);
          fs.rmdirSync(dir);
        }
      } catch (e) {
        console.error(e);
      }

      if (cb) cb();
    }
  });
};

const download = (req, res, next) => {
  const file_path = BASE_DIR + "/" + req.params.path + "/" + req.params.name;

  console.log("--- file download file_path", file_path);

  res.download(file_path); // Set disposition and send it.
};

const deleteFolderRecursive = function(dir) {
  if (fs.existsSync(dir)) {
    fs.readdirSync(dir).forEach((file, index) => {
      const curPath = path.join(dir, file);
      if (fs.lstatSync(curPath).isDirectory()) {
        // recurse
        deleteFolderRecursive(curPath);
      } else {
        // delete file
        fs.unlinkSync(curPath);
      }
    });
    fs.rmdirSync(dir);
  }
};

module.exports = { upload, rename, remove, download };
