import moment from "moment";
import _ from "lodash";
import chalk from "chalk";
import format from "utils/format-util";

import RoomReserv from "model/room-reserv";
import RoomType from "model/room-type";

import {ERROR, PREFERENCES} from "constants/constants";
import {keyToValue, keyCodes} from "constants/key-map";

export const airbnb_fix = (ota, reserv, row, place, callback) => {
  console.log(chalk.yellow("- airbnb_fix"), row);

  let {type, reserv_num, name, hp, room, amt, address, description, checkInDate, checkOutDate} = row;

  // 한글,영문,숫자만
  var regexp = new RegExp(/[^ㄱ-ㅎ|가-힣|a-z|0-9|]/gi);

  let typeName = room.replace(regexp, "");

  console.log(chalk.yellow("- typeName"), typeName);

  ota.getRoomTypes(reserv.place_id, (roomTypes) => {
    let roomType = _.findLast(roomTypes, (v) => {
      let name = v.name.replace(regexp, "");

      let ota_name_1 = v.ota_name_1 ? v.ota_name_1.replace(regexp, "") : "";
      let ota_name_2 = v.ota_name_2 ? v.ota_name_2.replace(regexp, "") : "";
      let ota_name_3 = v.ota_name_3 ? v.ota_name_3.replace(regexp, "") : "";
      let ota_name_4 = v.ota_name_4 ? v.ota_name_4.replace(regexp, "") : "";
      let ota_name_5 = v.ota_name_5 ? v.ota_name_5.replace(regexp, "") : "";

      return name === typeName || ota_name_1 === typeName || ota_name_2 === typeName || ota_name_3 === typeName || ota_name_4 === typeName || ota_name_5 === typeName;
    });

    if (!roomType && process.env.REACT_APP_MODE !== "PROD") {
      typeName = roomTypes[Math.floor(Math.random() * roomTypes.length)];
      console.log(chalk.red("- TEST 객실 타입 전환"), typeName);
    }

    console.log(chalk.yellow("- roomType"), roomType ? roomType.name : "no room type");

    if (roomType) {
      let {id, name, default_fee_stay, default_fee_rent, reserv_discount_fee_stay, reserv_discount_fee_rent} = roomType;

      reserv.room_type_id = id;
      reserv.room_type_name = name;

      ota.setRoomFee(reserv, (add_fee) => {
        let room_fee = reserv.stay_type === 2 ? default_fee_rent : default_fee_stay; //  기본 요금
        let reserv_fee = reserv.stay_type === 2 ? default_fee_rent - reserv_discount_fee_rent : default_fee_stay - reserv_discount_fee_stay; // 예약 할인 요금

        // 요일/시간별 추가/할인 요금 적용.
        room_fee += add_fee;

        if (reserv.prepay_ota_amt > 0) {
          reserv_fee = reserv.prepay_ota_amt; // OTA 결제 시 결제 요금을 예약 가로 설정 한다.(잔금 발생 방지)
        } else {
          reserv_fee += add_fee; // OTA 결제 시 옵션 추가 요금은 적용 하지 않는다.
        }

        reserv.room_fee = room_fee;
        reserv.reserv_fee = reserv_fee;
        if (reserv.stat === "A") reserv.memo = line[4]; // 객실 타입 정보 상세 추가.

        callback({reserv});
      });
    } else {
      callback({reserv});
    }
  });
};

export const airbnb_cancel = (ota, reserv, callback) => {
  console.log(chalk.yellow("- airbnb_cancel"), reserv.reserv_num);

  if (reserv.reserv_num) {
    ota.getReservByReservNum(reserv.reserv_num, (room_reserv) => {
      if (room_reserv) {
        reserv = _.merge({}, reserv, room_reserv);
      }

      reserv.state = "B";

      callback({reserv});
    });
  } else {
    callback({reserv});
  }
};
