import _ from "lodash";
import moment from "moment";

const CheckSubscribe = (subscribe) => {
  // console.log("- CheckSubscribe", subscribe);

  if (!subscribe) {
    return false;
  } else {
    // 구독 시작 일자.
    if (moment(subscribe.begin_date).isAfter(moment())) {
      console.log("- CheckSubscribe begin_date false", subscribe.begin_date);
      return false;
    }

    // 무료 서비스 상태.
    if (subscribe.valid_yn === 0) {
      // 무료 서비스 기간 지났다면.
      if (moment(subscribe.trial_end_date).isBefore(moment())) {
        console.log("- CheckSubscribe trial_end_date false", subscribe.trial_end_date);
        return false;
      }
    }
    // 정상 서비스 상태.
    else if (subscribe.valid_yn === 1) {
      // 구독 만료 일자 지났다면.
      if (moment(subscribe.end_date).isBefore(moment())) {
        console.log("- CheckSubscribe end_date false", subscribe.end_date);
        return false;
      }
    }
    // 서비스 중지
    else {
      return false;
    }
  }

  return true;
};

const CheckLicense = (sub, use_license_copy, callback) => {
  // 오류 방지 차원에서 없는 구독이나 사용 안하는 구독일 때는 true 반환.
  if (!sub || sub.use_yn === "N") {
    return false;
  }

  if (sub.license_yn !== "Y") {
    return false;
  }

  // 미 구독 시 기본 라이선스 적용.
  let license_copy = Number(sub.default_license);

  if (sub) {
    license_copy = Number(sub.default_license) + Number(sub.license_copy);
  }

  if (license_copy <= Number(use_license_copy)) {
    return false;
  }

  if (callback) callback(true);

  return license_copy;
};

export { CheckSubscribe, CheckLicense };
