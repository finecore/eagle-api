// .env config.
require("dotenv").config();

// mysql db
const config =
  process.env.DB_HOST_LOCAL !== "N"
    ? {
        connectionLimit: 500,
        connectTimeout: 10000,
        multipleStatements: true,
        host: "localhost",
        port: 3306,
        user: process.env.DB_HOST_USER,
        password: process.env.DB_HOST_PWD,
        database: "eagle",
        charset: "utf8mb4",
        debug: false,
      }
    : {
        connectionLimit: 500,
        connectTimeout: 10000,
        multipleStatements: true,
        host: process.env.DB_HOST_IP,
        port: process.env.DB_HOST_PORT,
        user: process.env.DB_HOST_USER,
        password: process.env.DB_HOST_PWD,
        database: "eagle",
        charset: "utf8mb4",
        debug: false,
      };

console.info("-------------------------------------------------");
console.info("- DB Config DB_HOST_LOCAL : " + process.env.DB_HOST_LOCAL + " " + JSON.stringify(config));
console.table(config);
console.info("-------------------------------------------------\n");

module.exports = config;
