const mysql = require("mysql");
const config = require("./config");

console.log("- DB Config", config);

// mysql db pool
const pool = mysql.createPool(config);

pool.on("connection", function(connection) {
  console.log("- DB New connection for pool");
  connection.query("SET SESSION auto_increment_increment=1");
});

pool.on("enqueue", function() {
  //console.log("- DB Waiting for available connection slot");
});

pool.on("error", function(err) {
  console.log("- DB Connection pool error");
});

pool.on("acquire", function(connection) {
  // console.log("- DB Connection %d acquired", connection.threadId);
});

pool.on("release", function(connection) {
  //console.log("- DB Connection %d released", connection.threadId);
});

pool.query("SELECT 1 + 1 AS solution", function(error, results, fields) {
  if (error) throw error;
  //console.log("- DB The solution is: ", results[0].solution);
});

module.exports = pool;
