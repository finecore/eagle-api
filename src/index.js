/**
 * Module dependencies.
 */
let app = require("./app");
let debug = require("debug")("eagle-node:server");
let http = require("http");

const chalk = require("chalk");
const moment = require("moment");
const _ = require("lodash");
const ip = require("ip");
const util = require("util");

const { spawn } = require("child_process");

const stackTrace = require("stack-trace");

// winston log
const logger = require("../config/logger");

const Dispatcher = require("dispatcher/dispatcher");
const DataManager = require("scheduler/data-manager");
const RoomSaleReport = require("scheduler/room-sale-report");
const MmsMoManager = require("scheduler/mms-mo-manager");
const MailManager = require("scheduler/mail-manager");

const Server = require("./model/server");

require("dotenv").config();

// locale:ko를 등록한다. 한번 지정하면 자동적으로 사용된다.
moment.updateLocale("ko", {
  weekdays: ["일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일"],
  weekdaysShort: ["일", "월", "화", "수", "목", "금", "토"],
});

/**
 * Get port from environment and store in Express.
 */
let port = parseInt(process.env.PORT || 3000, 10);
app.set("port", port);

const config = {};

_.map(process.env, (v, k) => {
  // console.log(v, k);
  let chr = k.charAt(0);
  if (chr === chr.toUpperCase()) config[k] = v;
});

console.log("=== CONFIG === ", config);

/**
 * Create HTTP server.
 */
let server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */
server.listen(port);
server.on("error", onError);
server.on("listening", onListening);

/**
 * Event listener for HTTP server "error" event.
 */
function onError(error) {
  if (error.syscall !== "listen") {
    throw error;
  }

  console.log("==> server error " + error);

  let bind = typeof port === "string" ? "Pipe " + port : "Port " + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case "EACCES":
      console.error(bind + " requires elevated privileges");
      process.exit(1);
      break;
    case "EADDRINUSE":
      console.error(bind + " is already in use");
      process.exit(1);
      break;
    default:
      throw error;
  }
}

const ipaddr = ip.address();

let isWriteLog = false;
let lastMailContent = "";
let lastLogData = [];
let mailSendDelay = true;
let logTimer = null;

/**
 * Event listener for HTTP server "listening" event.
 */
function onListening() {
  let addr = server.address();
  let bind = typeof addr === "string" ? "pipe " + addr : "port " + addr.port;

  console.info(`========== [${process.env.REACT_APP_MODE}] api server started =============`);
  console.info("- Listening on " + ipaddr + ":" + addr.port);
  console.info("- REACT_APP_SVR_NAME", process.env.REACT_APP_SVR_NAME);
  console.info("- NODE_APP_INSTANCE", process.env.NODE_APP_INSTANCE);
  console.info("- pm_id", process.env.pm_id);
  console.info(`- DB HOST ${process.env.DB_HOST_IP}:${process.env.DB_HOST_PORT}`);
  console.info("============================================\n");

  console.info(`========== scheduler started =============`);
  // PM2 Cluster 사용 시 첫번째 인스턴스만 실행함!
  if (!process.env.NODE_APP_INSTANCE || Number(process.env.NODE_APP_INSTANCE) === 0) {
    console.info("- cluster first instance", process.env.NODE_APP_INSTANCE);

    // 백업 서버 중복 처리 방지
    if (process.env.REACT_APP_SVR_NAME === "RUNNING") {
      console.info("- IS RUNNING Server");

      global.dataManager = new DataManager();
      global.roomSaleHelper = new RoomSaleReport();
    }

    // OTA 자동 문자 예약은 백업 서버에서 한번더 체크 한다.
    global.mmsMoManager = new MmsMoManager();
  }

  // 데이터를 각 단말 (Web, Device 등)에 실시간 소켓 전송.
  global.dispatcher = new Dispatcher();

  console.info("============================================\n");

  // 인스턴스 별로 시간 차를 두어서 동시 작업 하지 않게 한다.
  setTimeout(() => {
    // 메일 전송 스케줄러.
    global.mailManager = new MailManager();

    mailSendDelay = false;

    let content =
      moment().format("YYYY-MM-DD HH:mm:ss") +
      "<br/><br/><strong>Api Server Started!</strong><br/><br/>SVR: " +
      process.env.REACT_APP_SVR_NAME +
      "<br/>PM2: " +
      process.env.pm_id +
      "<br/>Ip: " +
      ipaddr +
      "<br/>Port: " +
      addr.port;

    sendErrMail("info@icrew.kr", "Server " + process.env.pm_id + " Started!", content);
  }, 5 * 1000 * Number(process.env.pm_id));

  let filter = `name='${process.env.REACT_APP_SVR_NAME}'`;

  setInterval(() => {
    getServerInfo(filter);
  }, 60 * 1000);

  getServerInfo(filter);

  // 시간 타이머. (서버 시작시 바로 시작 금지!)
  this.timer = setInterval(() => {
    let HHmmss = moment().format("HHmmss");

    // console.log("- Server Restart Timer ", HHmm);

    // 매일 새벽 5 시에 재기동 한다.
    if (HHmmss === "050000") {
      // console.log("- Server Restart Time!");

      // 인스턴스 별로 10 초의 텀을 주고 순차적으로 재기동.
      setTimeout(() => {
        // console.log("- Server instance " + process.env.pm_id + " Restart!!");
        // spawn(process.argv[1], process.argv.slice(2), {
        //   detached: true,
        //   stdio: ["ignore"],
        // }).unref();
        // process.exit();
      }, Number(process.env.pm_id) * 10 * 1000);
    }
  }, 1000);
}

const getServerInfo = (filter) => {
  console.log("- getServerInfo ", filter);

  try {
    Server.selectServers(filter, (err, servers) => {
      console.log("- selectServer ", { err, servers });

      if (!err && servers[0]) {
        let { name, status, write_log } = servers[0];

        if (write_log && !isWriteLog) {
          console.info("- isWriteLog start", { name, status });
          isWriteLog = true;
        } else if (!write_log && isWriteLog) {
          console.info("- isWriteLog stop", { name, status });
          isWriteLog = false;
        }
      } else {
        console.error("- getServerInfo error", err);
      }
    });
  } catch (e) {
    console.error("- getServerInfo error catch", err);
  }
};

// 오류 메일 발송 시 마지막 로그 정보.
const addLastLog = (log) => {
  lastLogData.push(log);
  if (lastLogData.length > 5) lastLogData.shift();
};

const sendErrMail = (from, subject, content) => {
  subject = `${process.env.REACT_APP_MODE !== "PROD" ? "[" + process.env.REACT_APP_MODE + "]" : ""} Api ${subject || ""} (${process.env.REACT_APP_SVR_NAME})`;
  from = from || process.env.REACT_APP_MAIL_FROM_ADDR;

  if (global.mailManager && process.env.REACT_APP_MODE !== "LOCAL" && content && content.length > 3 && !mailSendDelay) {
    // if (global.mailManager && content && content.length > 3 && !mailSendDelay) {

    mailSendDelay = true;

    content += "<br/><br/><br/>------------ Stack ------------<br/><font color='#bbb'" + lastLogData.join("<br/>") + "</font><br/>" + content;

    // 메일 타입 (0: 고객메일, 1: 관리자메일, 9: 서버메일)
    global.mailManager.send({
      type: 9,
      to: process.env.REACT_APP_MAIL_ERR_TO_ADDR,
      from,
      subject,
      content,
    });

    setTimeout(() => {
      mailSendDelay = false;
    }, 60 * 1000);
  }
};

/**
 * Make node.js not exit on error
 */
process.on("uncaughtException", function (err) {
  console.info(err);
  console.error(err);
});

/** Console Log Format */
console.logCopy = console.log.bind(console);

console.log = function (...args) {
  let data = _.map(args, (arg) => (typeof arg === "object" ? util.inspect(arg) : arg)).join(" ");

  data = moment().format("YYYY-MM-DD HH:mm:ss") + " " + data;
  this.infoCopy(data);

  if (_.trim(data)) {
    data = data.replace(/.token":([\'\"][^\'\"]+[\'\"])/g, `"token":"........"`); // 토큰정보 제거.
    data = data.replace(/(\[\d{1,2}m)/gi, ""); // chalk ascii 값 삭제. ([1m, [32m...)

    if (isWriteLog) {
      try {
        logger.debug(data);
      } catch (e) {}
    }

    data = data.replace(/\n/g, "<br/>");
    addLastLog(data);
  }
};

console.infoCopy = console.info.bind(console);

console.info = function (...args) {
  let data = _.map(args, (arg) => (typeof arg === "object" ? util.inspect(arg) : arg)).join(" ");

  data = moment().format("YYYY-MM-DD HH:mm:ss") + " " + data;
  this.infoCopy(data);

  if (_.trim(data)) {
    data = data.replace(/.token":([\'\"][^\'\"]+[\'\"])/g, `"token":"........"`); // 토큰정보 제거.
    data = data.replace(/(\[\d{1,2}m)/gi, ""); // chalk ascii 값 삭제.  ([1m, [32m...)

    try {
      logger.info(data);
    } catch (e) {}

    data = data.replace(/\n/g, "<br/>");
    addLastLog(data);
  }
};

console.warn = function (...args) {
  if (args && !_.isEmpty(args[0])) {
    let data = _.map(args, (arg) => (typeof arg === "object" ? util.inspect(arg) : arg)).join("<br/>");

    if (_.trim(data)) {
      data = data.replace(/.token":([\'\"][^\'\"]+[\'\"])/g, `"token":"........"`); // 토큰정보 제거.

      try {
        logger.warn(data); // 오류로그
      } catch (e) {}

      var trace = stackTrace.get();

      let file = trace[1].getFileName().replace(__dirname, "");
      let line = trace[1].getLineNumber() + ":" + trace[1].getColumnNumber();

      console.log("-> warn file", { file, line }, args);
    }
  }
};

console.error = function (...args) {
  if (args && !_.isEmpty(args[0])) {
    let data = _.map(args, (arg) => (typeof arg === "object" ? util.inspect(arg) : arg)).join("<br/>");

    if (_.trim(data)) {
      data = data.replace(/.token":([\'\"][^\'\"]+[\'\"])/g, `"token":"........"`); // 토큰정보 제거.

      try {
        logger.error(data);
      } catch (e) {}

      data = data.replace(/\s\[\d{2}m/g, "").replace(/\n/g, "<br/>"); // chalk ascii 값 삭제.  ([90m, [32m...)
      data = moment().format("YYYY-MM-DD HH:mm:ss") + "<br/><br/>" + data;

      var trace = stackTrace.get();

      let file = trace[1].getFileName().replace(__dirname, "");
      let line = trace[1].getLineNumber() + ":" + trace[1].getColumnNumber();

      console.log("-> error file", { file, line }, args);

      data += `<br/><br/>${file} ${line}`;

      sendErrMail("error@icrew.kr", "Error", data);
    }
  }
};

const cTable = require("console.table");

console.cTable = console.table.bind(console);

console.table = function (title, data) {
  if (title && data) this.logCopy(title);
  data = data || title;
  if (data) {
    const table = cTable.getTable([data]);
    console.log(chalk.hex("#9f7462")(table));
  }
};
