const pool = require("./pool");

const chalk = require("chalk");
const _ = require("lodash");
const moment = require("moment");

// logger
const logger = require("../../config/logger");

// 테이블 컬럼 정보 저장.
let tableSchema = {};

let inx = 0;

var connection = function (callback) {
  pool.getConnection(function (_err, _connection) {
    if (_err || !_connection) {
      if (_connection) _connection.release();
      callback(_err, null);
      // throw _err;
    } else {
      inx++;

      _connection.inx = inx;

      let sqlLog = "=== DataBase ===";

      // query wrap.
      _connection.sql = function (sql, params, result, isTransaction) {
        let sql_syntax = _.filter(_.trim(sql).split(" "), (v) => v);

        const type = sql_syntax[0].toUpperCase();
         const dupUpdate = sql.indexOf("ON DUPLICATE KEY UPDATE") > -1;
        const join = sql_syntax[4] && sql_syntax[4].toUpperCase() === "JOIN";

        // Insert, Update 시 테이블에 없는 컬럼이 들어오면 자동으로 제거 한다.

        if (type === "INSERT" || type === "UPDATE") {
          let tableName = type === "INSERT" ? sql_syntax[2] : sql_syntax[1];

          // console.log("--> con sql tableName", { tableName, type, join, params });

          const isRemove = true; // 없는 필드 자동 삭제 여부.

          // 테이블 필드 체크.
          let res = existColumn(tableName.toLowerCase(), type, join, params, dupUpdate);

          const { bad_column, new_params } = res;

          params = new_params;

          if (!isRemove && bad_column.length) {
            _connection.release();
            result(
              {
                code: 500,
                message: "테이블에 컬럼이 존재 하지 않습니다.",
                detail: "입력 항목을 확인해 주세요.",
              },
              bad_column
            );
            return false;
          }
        }

        sqlLog += "\n>>>----------(" + this.inx + ")------------>\n";

        var query = this.query(sql, params, (_err, _row) => {
          if (!_err) {
            if (_row[0]) {
              sqlLog += chalk.magenta("row count [" + _row.length + "] \n");

              // 최대 10개만 로깅
              _.each(_row, (v, k) => {
                if (k < 3) sqlLog += `[${k + 1}] ` + chalk.cyan(JSON.stringify(v)) + "\n";
                else if (k === 3) sqlLog += chalk.gray(`\n.... more [${_row.length - 3}] ...\n`);
                else return false;
              });
            } else {
              sqlLog += chalk.red("No data!");
            }

            sqlLog += "\n- QUERY OK! \n";
          }
          sqlLog += "<------------(" + this.inx + ")----------<<<\n";

          inx--;

          // 인터럽트 주기적 삭제 로그는 안찍는다.
          if (sql.indexOf("DELETE FROM room_interrupt") === -1) {
            console.log("");
            console.log(sqlLog);
            console.log("");
          }

          result(_err, _row);
        });

        sqlLog += "> Sql parse \n" + chalk.yellow(query.sql, 3000) + "\n";

        if (!isTransaction) this.release();
      };

      callback(null, _connection);
    }
  });
};

// 테이블 스키마 저장.
function getTableSchema() {
  pool.getConnection(function (_err, _connection) {
    if (_err || !_connection) {
      if (_connection) _connection.release();
      // throw _err;
    } else {
      let sql = "SELECT * FROM information_schema.columns WHERE TABLE_SCHEMA = 'eagle';";

      var query = _connection.query(sql, null, (_err, _row) => {
        _connection.release();

        if (!_err) {
          tableSchema = {}; // 테이블 스키마 초기화.

          _.map(_row, (item) => {
            let table = item.TABLE_NAME.toLowerCase();
            if (!tableSchema[table]) tableSchema[table] = [];
            tableSchema[table].push({ column_name: item.COLUMN_NAME.toLowerCase() });
          });

          // console.log("---> init tableSchema", tableSchema);
        }
      });
    }
  });
}

function isJson(json) {
  // if (json) console.log("--- isJson", /^\{(.+)\}$/.test(_.trim(json)));
  return json ? /^\{(.+)\}$/.test(_.trim(json)) : false;
}

function toJson(json) {
  if (typeof json === "object") return json;

  try {
    return JSON.parse(json);
  } catch (err) {
    return {};
  }
}

function existColumn(tableName, type, join, params,dupUpdate) {
  tableName = tableName.replace(/\`/g, ""); // table name ` 제거

  const table = tableSchema[tableName];

  // console.log("---> existColumn", tableName, type, table, params);

  let bad_column = [];
  let new_params = [];

  if (table) {
    _.map(params, (data, i) => {
      if (data instanceof Object) {
        new_params[i] = {};

        if (data && !data.length) {
          _.map(data, (v, k) => {
            if (v !== undefined) {
              // console.log("- has column", k, v, isJson(v));

              let row = _.find(table, { column_name: k.toLowerCase() });

              if (!row) bad_column.push(k);
              else {
                if (_.trim(v).length <= 25 && !isJson(v)) {
                  if (v instanceof Date || /(\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d{3}Z)/.test(String(v))) {
                    console.log("---- param Date", k, String(v));
                    v = moment(_.trim(v)).format("YYYY-MM-DD HH:mm:ss.SSS"); // 날자 포멧.
                    console.log("---> format Date", k, v);
                  }
                }
                new_params[i][k] = v;
              }
            }
          });

          // 등록일/수정일 자동 설정.
          if (type === "INSERT") {
            delete new_params[i].reg_date;
            delete new_params[i].mod_date;
            if (dupUpdate) new_params[i].mod_date = new Date();
          } else if (type === "UPDATE") {
            delete new_params[i].mod_date;
            delete new_params[i].reg_date;
            if (!join && _.find(table, { column_name: "mod_date" })) new_params[i].mod_date = new Date();
          }
        }
      } else {
        new_params[i] = data;
      }
    });
  } else {
    new_params = params;
  }

  if (bad_column.length) {
    console.log("");
    console.log(chalk.yellow("- auto remove table bad column "), {
      tableName,
      bad_column,
    });
    console.log("");
  }

  // if (new_params.length) {
  //   console.log("");
  //   console.log(chalk.yellow("- table new params "), {
  //     tableName,
  //     new_params,
  //   });
  //   console.log("");
  // }

  return { bad_column, new_params };
}

getTableSchema();

setInterval(() => {
  getTableSchema();
}, 10 * 60 * 1000);

module.exports = connection;
