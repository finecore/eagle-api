import MmsMo from "controllers/mms-mo";
import { jsonRes, sendRes, isValidToken, checkPermission } from "utils/api-util";

module.exports = (app) => {
  app.route(["/mms/mo/list/:filter/:limit", "/mms/mo/list/:filter/:order/:desc/:limit"]).get(isValidToken, MmsMo.list_a_mms_mos);

  app.route("/mms/mo").post(isValidToken, checkPermission, MmsMo.create_a_mms_mo);

  app.route("/mms/mo/:id").get(isValidToken, MmsMo.read_a_mms_mo).put(isValidToken, checkPermission, MmsMo.update_a_mms_mo).delete(isValidToken, checkPermission, MmsMo.delete_a_mms_mo);
};
