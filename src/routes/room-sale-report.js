import roomSaleReport from "controllers/room-sale-report";
import { jsonRes, sendRes, isValidToken, checkPermission } from "utils/api-util";

const valid = (req, res, next) => {
  return next();
};

module.exports = (app) => {
  app.route(["/room/sale/report/hours/:place_id/:begin/:end/:order/:desc/:limit/:excel?"]).get(valid, isValidToken, roomSaleReport.list_place_sale_hours_report);
  app.route(["/room/sale/report/days/:place_id/:begin/:end/:order/:desc/:limit/:excel?"]).get(valid, isValidToken, roomSaleReport.list_place_sale_days_report);
  app.route(["/room/sale/report/months/:place_id/:begin/:end/:order/:desc/:limit/:excel?"]).get(valid, isValidToken, roomSaleReport.list_place_sale_months_report);
  app.route(["/room/sale/report/years/:place_id/:begin/:end/:order/:desc/:limit/:excel?"]).get(valid, isValidToken, roomSaleReport.list_place_sale_years_report);
};
