import appPush from "controllers/app-push";
import { jsonRes, sendRes, isValidToken, checkPermission } from "utils/api-util";

import PushHelper from "helper/push-helper";

const { ERROR } = require("constants/constants");
const _ = require("lodash");

const valid = (req, res, next) => {
  let validationError = _.cloneDeep(ERROR.INVALID_PARAMETER);

  const { url, method } = req;
  const { id, token } = req.params;

  if (url.indexOf("/token/") > -1) {
    if (!token) validationError.detail.push("app push token 을 입력해 주세요!");
  } else if (!id) validationError.detail.push("app push id 을 입력해 주세요!");

  if (Object.keys(validationError.detail).length > 0) return jsonRes(req, res, validationError);
  else return next();
};

module.exports = app => {
  // app.route("/app/push/all").post(valid, isValidToken,  appPush.all_push);

  // app
  //   .route("/app/push/place/:place_id")
  //   .post(valid, isValidToken,  appPush.place_push);

  // app.route("/app/push/user/:user_id").post(valid, isValidToken,  appPush.user_push);

  app
    .route("/app/push/list/:filter/:order/:desc/:limit")
    .get(isValidToken, appPush.list_a_app_pushs);

  app
    .route("/app/push/token/:token")
    .get(valid, isValidToken, appPush.list_token_app_pushs)
    .put(valid, isValidToken, checkPermission, appPush.update_token_app_push)
    .delete(valid, isValidToken, checkPermission, appPush.delete_token_app_push);

  app.route("/app/push").post(isValidToken, appPush.create_a_app_push);

  app
    .route("/app/push/:id")
    .get(valid, isValidToken, appPush.read_a_app_push)
    .put(valid, isValidToken, checkPermission, appPush.update_a_app_push)
    .delete(valid, isValidToken, checkPermission, appPush.delete_a_app_push);
};
