import place from "controllers/place";
import { jsonRes, sendRes, isValidToken, checkPermission } from "utils/api-util";
import { ERROR } from "constants/constants";
const _ = require("lodash");

const valid = (req, res, next) => {
  var validationError = _.cloneDeep(ERROR.INVALID_PARAMETER);

  const { method } = req;
  const { id, name } = req.params;

  if (!id) validationError.detail.push("place id 를 입력해 주세요!");
  if (isNaN(id)) validationError.detail.push("place id 는 숫자를 입력해 주세요!");

  if (Object.keys(validationError.detail).length > 0) return jsonRes(req, res, validationError);
  else return next();
};

module.exports = app => {
  app.route(["/place/list/:filter/:order/:desc/:limit"]).get(isValidToken, place.list_all_places);

  app
    .route("/place/:id")
    .get(valid, isValidToken, place.read_a_place)
    .put(valid, isValidToken, checkPermission, place.update_a_place)
    .delete(valid, isValidToken, checkPermission, place.delete_a_place);

  app.route("/place").post(isValidToken, checkPermission, place.create_a_place);
};
