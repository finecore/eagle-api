import company from "controllers/company";
import { jsonRes, sendRes, isValidToken, checkPermission } from "utils/api-util";
const { ERROR } = require("constants/constants");
const _ = require("lodash");

const valid = (req, res, next) => {
  var validationError = _.cloneDeep(ERROR.INVALID_PARAMETER);

  const { method } = req;
  const { id, name } = req.params;

  if (!id) validationError.detail.push("company id 를 입력해 주세요!");
  if (isNaN(id)) validationError.detail.push("company id 는 숫자를 입력해 주세요!");

  if (Object.keys(validationError.detail).length > 0) return jsonRes(res, validationError);
  else return next();
};

module.exports = app => {
  app.route("/company/list/:filter/:order/:desc/:limit").get(isValidToken, company.list_a_companys);

  app
    .route("/company/:id")
    .get(valid, isValidToken, company.read_a_company)
    .put(valid, isValidToken, checkPermission, company.update_a_company)
    .delete(valid, isValidToken, checkPermission, company.delete_a_company);

  app.route("/company").post(isValidToken, checkPermission, company.create_a_company);
};
