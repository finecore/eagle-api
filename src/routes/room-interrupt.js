import roomInterrupt from "controllers/room-interrupt";
import { jsonRes, sendRes, isValidToken, checkPermission } from "utils/api-util";
const { ERROR } = require("constants/constants");
const _ = require("lodash");

const valid = (req, res, next) => {
  var validationError = _.cloneDeep(ERROR.INVALID_PARAMETER);

  const { url, method } = req;
  const { room_id, user_id } = req.params;

  if (url.indexOf("/user/") > -1) {
    if (!user_id) validationError.detail.push("room_interrupt user 을 입력해 주세요!");
  } else if (!room_id) validationError.detail.push("room_interrupt room_id 을 입력해 주세요!");

  if (Object.keys(validationError.detail).length > 0) return jsonRes(req, res, validationError);
  else return next();
};

module.exports = (app) => {
  // room interrupt route.
  app.route("/room/interrupt/all/:place_id").get(isValidToken, roomInterrupt.list_all_room_interrupts).delete(isValidToken, checkPermission, roomInterrupt.delete_all_room_interrupt);

  app
    .route("/room/interrupt/user/:user_id/:channel")
    .get(valid, isValidToken, roomInterrupt.read_user_room_interrupt)
    .delete(valid, isValidToken, checkPermission, roomInterrupt.delete_user_room_interrupt);

  app
    .route("/room/interrupt/:room_id")
    .get(valid, isValidToken, roomInterrupt.read_a_room_interrupt)
    .put(valid, isValidToken, checkPermission, roomInterrupt.update_a_room_interrupt)
    .delete(valid, isValidToken, checkPermission, roomInterrupt.delete_a_room_interrupt);

  app.route("/room/interrupt").post(isValidToken, checkPermission, roomInterrupt.create_a_room_interrupt);
};
