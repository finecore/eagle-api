import mileage from "controllers/mileage";
import { jsonRes, sendRes, isValidToken, checkPermission } from "utils/api-util";
const { ERROR } = require("constants/constants");
const _ = require("lodash");

const valid = (req, res, next) => {
  var validationError = _.cloneDeep(ERROR.INVALID_PARAMETER);

  const { method } = req;
  const { id, type, name } = req.params;

  if (!id) validationError.detail.push("mileage id 를 입력해 주세요!");
  if (isNaN(id)) validationError.detail.push("mileage id 는 숫자를 입력해 주세요!");

  if (Object.keys(validationError.detail).length > 0) return jsonRes(req, res, validationError);
  else return next();
};

module.exports = (app) => {
  app.route(["/mileage/all", "/mileage/list/:filter/:order/:desc/:limit"]).get(isValidToken, mileage.list_a_mileages);
  app.route("/mileage/place/:place_id").get(isValidToken, mileage.list_place_mileages);

  app.route("/mileage").post(mileage.create_a_mileage);

  app.route("/mileage/:id").get(valid, isValidToken, mileage.read_a_mileage).put(valid, isValidToken, mileage.update_a_mileage);

  app.route("/mileage/:id/:user_id").delete(valid, isValidToken, checkPermission, mileage.delete_a_mileage);

  app.route("/mileage/:place_id/phone/:phone").get(isValidToken, mileage.read_place_phone_mileage);

  app.route("/mileage/:place_id/phone/:phone").put(isValidToken, mileage.update_place_mileage_phone);
  app.route(["/mileage/increase/:place_id/:phone/:user_id/:point", "/mileage/increase/:place_id/:phone/:user_id/:point/:rollback"]).put(isValidToken, mileage.increase_place_mileage_phone);
  app.route(["/mileage/decrease/:place_id/:phone/:user_id/:point", "/mileage/decrease/:place_id/:phone/:user_id/:point/:rollback"]).put(isValidToken, mileage.decrease_place_mileage_phone);
};
