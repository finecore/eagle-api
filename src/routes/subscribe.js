import Service from "controllers/subscribe";
import { jsonRes, sendRes, isValidToken, checkPermission } from "utils/api-util";

module.exports = app => {
  app.route(["/subscribe/all", "/subscribe/list/:filter", "/subscribe/list/:filter/:order/:desc/:limit"]).get(isValidToken, Service.list_a_subscribes);

  app.route("/subscribe").post(isValidToken, checkPermission, Service.create_a_subscribe);

  app
    .route("/subscribe/:id")
    .get(isValidToken, Service.read_a_subscribe)
    .put(isValidToken, checkPermission, Service.update_a_subscribe)
    .delete(isValidToken, checkPermission, Service.delete_a_subscribe);

  app.route("/subscribe/:path/:name/:newname").put(isValidToken, checkPermission, Service.update_a_subscribe);
};
