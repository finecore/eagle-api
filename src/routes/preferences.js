import preferences from "controllers/preferences";
import { jsonRes, sendRes, isValidToken, checkPermission } from "utils/api-util";
const { ERROR } = require("constants/constants");
const _ = require("lodash");

const valid = (req, res, next) => {
  var validationError = _.cloneDeep(ERROR.INVALID_PARAMETER);

  const { id } = req.params;

  if (!id) validationError.detail.push("preferences id 를 입력해 주세요!");
  if (isNaN(id)) validationError.detail.push("preferences id 는 숫자를 입력해 주세요!");

  if (Object.keys(validationError.detail).length > 0) return jsonRes(req, res, validationError);
  else return next();
};

module.exports = (app) => {
  // preferences route.
  app.route("/preferences/all").get(isValidToken, preferences.list_all_preferences);
  app.route("/preferences/:place_id").get(isValidToken, preferences.read_a_preferences);

  app.route("/preferences/:id").put(valid, isValidToken, checkPermission, preferences.update_a_preferences).delete(valid, isValidToken, checkPermission, preferences.delete_a_preferences);

  app.route("/preferences").post(isValidToken, checkPermission, preferences.create_a_preferences);
};
