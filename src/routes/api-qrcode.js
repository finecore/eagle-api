import ApiQrcode from "controllers/api-qrcode";
import { isValidToken, checkPermission } from "utils/api-util";
import { verify, sign } from "utils/jwt-util";
import { RES_JSON } from "constants/api-constants";

const preprocess = (req, res, next) => {
  const { url, method } = req;
  const { pms, room } = req.params;

  console.log("- api route", { url, method, room });

  // pms 인증 정보.
  let {
    headers: { channel = "pms", token },
  } = req;

  if (!token) {
    return RES_JSON(pms, res, 400, "토큰 정보가 없습니다.");
  } else {
    verify(token, function (err, user) {
      if (err) {
        return RES_JSON(pms, res, 400, "토큰 정보가 올바르지 앖습니다.");
      } else {
        req.auth = user; // request 에 auth 정보 설정.
        return next();
      }
    });
  }
};

module.exports = (app) => {
  app.route("/api/qrcode/:pms/:room/:check_in/:check_out").get(preprocess, ApiQrcode.get_yy_qrcode); // 객실 체크인 QR Code 조회.
};
