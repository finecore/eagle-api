import roomReserv from "controllers/room-reserv";
import { jsonRes, sendRes, isValidToken, checkPermission } from "utils/api-util";
const { ERROR } = require("constants/constants");
const _ = require("lodash");

const valid = (req, res, next) => {
  var validationError = _.cloneDeep(ERROR.INVALID_PARAMETER);

  const { id } = req.params;

  if (!id) validationError.detail.push("room_reserv id 를 입력해 주세요!");
  if (isNaN(id)) validationError.detail.push("room_reserv id 는 숫자를 입력해 주세요!");

  if (Object.keys(validationError.detail).length > 0) return jsonRes(req, res, validationError);
  else return next();
};

module.exports = (app) => {
  // room  reserv route.
  app
    .route(["/room/reserv/all/:place_id", "/room/reserv/all/:place_id/:begin/:end"])
    .get(isValidToken, roomReserv.list_all_room_reserv)
    .delete(isValidToken, checkPermission, roomReserv.delete_all_room_reserv);

  app.route(["/room/reserv/list/:filter/:begin/:end"]).get(isValidToken, roomReserv.list_a_room_reservs);
  app.route(["/room/reserv/enable/:place_id"]).get(isValidToken, roomReserv.list_a_place_enable_reservs);

  // QR 코드 조회는 validateion 체크 안함.
  app.route("/room/reserv/qr/:id").get(roomReserv.read_a_room_reserv);

  app
    .route("/room/reserv/:id")
    .get(isValidToken, roomReserv.read_a_room_reserv)
    .put(isValidToken, checkPermission, roomReserv.update_a_room_reserv)
    .delete(isValidToken, checkPermission, roomReserv.delete_a_room_reserv);

  app.route(["/room/reserv/num/:num"]).get(isValidToken, roomReserv.read_a_room_reserv_num);
  app.route(["/room/reserv/mms/mo/:num"]).get(isValidToken, roomReserv.read_a_room_reserv_mms_mo_num);

  app.route("/room/reserv").post(isValidToken, checkPermission, roomReserv.create_a_room_reserv);
};
