import ApiOta from "controllers/api-ota";
import { isValidToken, checkPermission } from "utils/api-util";
import { verify, sign } from "utils/jwt-util";

const valid = (req, res, next) => {
  var validationError = _.cloneDeep(ERROR.INVALID_PARAMETER);

  const { method } = req;
  const { id, name } = req.params;

  if (!id) validationError.detail.push("ota id 를 입력해 주세요!");
  if (isNaN(id)) validationError.detail.push("ota id 는 숫자를 입력해 주세요!");

  if (Object.keys(validationError.detail).length > 0) return jsonRes(res, validationError);
  else return next();
};

module.exports = (app) => {
  // OTA Inbound
  app.route("/api/ota/mms/:name?").post(ApiOta.mms);
};
