import sms from "controllers/sms";
import { jsonRes, sendRes, isValidToken, checkPermission } from "utils/api-util";
const { ERROR } = require("constants/constants");
const _ = require("lodash");

const valid = (req, res, next) => {
  var validationError = _.cloneDeep(ERROR.INVALID_PARAMETER);

  const { method } = req;
  const { tr_num } = req.params;

  if (!tr_num) validationError.detail.push("sms tr_num 를 입력해 주세요!");
  if (isNaN(tr_num)) validationError.detail.push("sms tr_num 는 숫자를 입력해 주세요!");

  if (Object.keys(validationError.detail).length > 0) return jsonRes(req, res, validationError);
  else return next();
};

module.exports = (app) => {
  app.route(["/sms/all", "/sms/list/:filter/:order/:desc/:limit"]).get(isValidToken, sms.list_a_smses);

  app.route("/sms").post(isValidToken, sms.create_a_sms);
  app.route("/sms/authno/:phone").post(isValidToken, sms.send_auth_no);

  app.route("/sms/:tr_num").get(valid, isValidToken, sms.read_a_sms).put(valid, isValidToken, checkPermission, sms.update_a_sms).delete(valid, isValidToken, checkPermission, sms.delete_a_sms);
};
