import appInfo from "controllers/app-info";
import { jsonRes, sendRes, isValidToken, checkSerialno, checkPermission } from "utils/api-util";
const { ERROR } = require("constants/constants");
const _ = require("lodash");

const valid = (req, res, next) => {
  var validationError = _.cloneDeep(ERROR.INVALID_PARAMETER);

  const { token } = req.params;

  if (!token) validationError.detail.push("app token 을 입력해 주세요!");

  if (Object.keys(validationError.detail).length > 0) return jsonRes(req, res, validationError);
  else return next();
};

module.exports = app => {
  app
    .route("/app/info/list/:filter/:order/:desc/:limit")
    .get(isValidToken, appInfo.list_a_app_infos);

  app.route("/app/info/place/:place_id").get(isValidToken, appInfo.list_place_app_infos);

  app.route("/app/info").post(isValidToken, checkSerialno, appInfo.create_a_app_info);

  app
    .route("/app/info/:token")
    .get(valid, isValidToken, checkSerialno, appInfo.read_a_app_info)
    .put(valid, isValidToken, checkSerialno, appInfo.update_a_app_info)
    .delete(valid, isValidToken, checkSerialno, checkPermission, appInfo.delete_a_app_info);
};
