import roomDoorLock from "controllers/room-doorlock";
import { jsonRes, sendRes, isValidToken, checkPermission } from "utils/api-util";
const { ERROR } = require("constants/constants");
const _ = require("lodash");

const valid = (req, res, next) => {
  var validationError = _.cloneDeep(ERROR.INVALID_PARAMETER);

  const { id } = req.params;

  if (!id) validationError.detail.push("room_doorlock id 를 입력해 주세요!");
  if (isNaN(id)) validationError.detail.push("room_doorlock id 는 숫자를 입력해 주세요!");

  if (Object.keys(validationError.detail).length > 0) return jsonRes(req, res, validationError);
  else return next();
};

module.exports = (app) => {
  // room  doorlock route.
  app.route(["/room/doorlock/all/:place_id", "/room/doorlock/all/:place_id/:begin/:end"]).get(isValidToken, roomDoorLock.list_all_room_doorlock);

  app.route(["/room/doorlock/list/:filter/:begin/:end"]).get(isValidToken, roomDoorLock.list_a_room_doorlocks);

  // QR 코드 조회는 validateion 체크 안함.
  app.route("/room/doorlock/qr/:id").get(roomDoorLock.read_a_room_doorlock);

  // QR 코드 YYLOCK 로그인 정보.
  app.route("/room/doorlock/login").post(isValidToken, checkPermission, roomDoorLock.login_yy_qrcode);

  // QR 코드 전송.
  app.route("/room/doorlock/send/:id").put(isValidToken, checkPermission, roomDoorLock.send_yy_qrcode);

  // QR 코드 정보 초기화.
  app.route("/room/doorlock/clear/:id").put(isValidToken, checkPermission, roomDoorLock.clear_doorlock_data);

  app
    .route("/room/doorlock/:id")
    .get(isValidToken, roomDoorLock.read_a_room_doorlock)
    .put(isValidToken, checkPermission, roomDoorLock.update_a_room_doorlock)
    .delete(isValidToken, checkPermission, roomDoorLock.delete_a_room_doorlock);

  app.route(["/room/doorlock/roomid/:room_id"]).get(isValidToken, roomDoorLock.read_a_room_doorlock_roomid);

  app.route("/room/doorlock").post(isValidToken, checkPermission, roomDoorLock.create_a_room_doorlock);
};
