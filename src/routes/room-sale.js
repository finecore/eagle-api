import roomSale from "controllers/room-sale";
import { jsonRes, sendRes, isValidToken, checkPermission } from "utils/api-util";
const { ERROR } = require("constants/constants");
const _ = require("lodash");

const valid = (req, res, next) => {
  var validationError = _.cloneDeep(ERROR.INVALID_PARAMETER);

  const { id } = req.params;

  if (!id) validationError.detail.push("room_sale id 를 입력해 주세요!");
  if (isNaN(id)) validationError.detail.push("room_sale id 는 숫자를 입력해 주세요!");

  if (Object.keys(validationError.detail).length > 0) return jsonRes(req, res, validationError);
  else return next();
};

module.exports = (app) => {
  // room  sale route.

  app.route(["/room/sale/all/:place_id", "/room/sale/all/:place_id/:begin/:end"]).get(isValidToken, roomSale.list_all_room_sale);
  app.route(["/room/sale/all/:place_id/:excel", "/room/sale/all/:place_id/:begin/:end/:excel"]).get(roomSale.list_all_room_sale);
  app.route(["/room/sale/now/:place_id"]).get(isValidToken, roomSale.list_now_room_sale);

  app
    .route("/room/sale/:id")
    .get(valid, isValidToken, roomSale.read_a_room_sale)
    .put(valid, isValidToken, checkPermission, roomSale.update_a_room_sale)
    .delete(valid, isValidToken, checkPermission, roomSale.delete_a_room_sale);

  app.route(["/room/sale/list/:room_id", "/room/sale/list/:room_id/:begin/:end"]).get(isValidToken, roomSale.list_a_room_sale);
  app.route(["/room/sale/list/:room_id/:excel", "/room/sale/list/:room_id/:begin/:end/:excel"]).get(roomSale.list_a_room_sale);
  app.route("/room/sale/sum/place/:place_id/:begin/:end").get(isValidToken, roomSale.read_a_room_sale_sum_place);
  app.route(" /room/sale/sum/place/:place_id/:begin/:end/:excel").get(roomSale.read_a_room_sale_sum_place);
  app.route(["/room/sale/today/:room_id", "/room/sale/sum/:room_id/:begin/:end"]).get(isValidToken, roomSale.read_a_room_sale_sum);
  app.route(["/room/sale/today/:room_id/:excel"]).get(roomSale.read_a_room_sale_sum);
  app.route("/room/sale/sum/room/:place_id/:begin/:end").get(isValidToken, roomSale.read_a_room_sale_sum_room);
  app.route("/room/sale/sum/room/:place_id/:begin/:end/:excel").get(roomSale.read_a_room_sale_sum_room);

  app.route("/room/sale").post(isValidToken, checkPermission, roomSale.create_a_room_sale);
};
