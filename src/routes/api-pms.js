import ApiPmsIn from "controllers/api-pms-in";
import { isValidToken, checkPermission } from "utils/api-util";
import { verify, sign } from "utils/jwt-util";
import { RES_JSON } from "constants/api-constants";

const preprocess = (req, res, next) => {
  const { url, method } = req;
  const { pms, room } = req.params;

  console.log("- api pms route", { url, method, pms, room });

  // pms 인증 정보.
  let {
    headers: { channel = "pms", token },
  } = req;

  if (!token) {
    return RES_JSON(pms, res, 400, "토큰 정보가 없습니다.");
  } else {
    verify(token, function (err, user) {
      if (err) {
        return RES_JSON(pms, res, 400, "토큰 정보가 올바르지 앖습니다.");
      } else {
        req.auth = user; // request 에 auth 정보 설정.
        return next();
      }
    });
  }
};

module.exports = (app) => {
  // PMS Inbound
  app.route("/api/pms/:pms/token").post(ApiPmsIn.create_token); // 토큰 조회.

  app.route("/api/pms/:pms/state").get(preprocess, ApiPmsIn.read_all_room); // 객실 상태 조회.
  app.route("/api/pms/:pms/state/:room").get(preprocess, ApiPmsIn.read_room); // 객실 상태 조회.
  app.route("/api/pms/:pms/checkin/:room/:start/:end").put(preprocess, ApiPmsIn.checkin_room); // 체크인 처리
  app.route("/api/pms/:pms/checkout/:room").put(preprocess, ApiPmsIn.checkout_room); // 체크아웃 처리
  app.route("/api/pms/:pms/rooms/init").put(preprocess, ApiPmsIn.init_rooms); // 객실 초기화(동기화)
};
