import File from "controllers/file";
import { jsonRes, sendRes, isValidToken, checkPermission } from "utils/api-util";
import { upload, rename, remove, download } from "utils/file-util";

module.exports = app => {
  app.route(["/file/list/:filter", "/file/list/:filter/:order/:desc/:limit"]).get(isValidToken, File.list_a_files);

  app
    .route("/file/:id")
    .get(isValidToken, File.read_a_file)
    .put(isValidToken, checkPermission, upload.single("file"), File.update_a_file)
    .delete(isValidToken, checkPermission, File.delete_a_file);

  app.route("/file").post(isValidToken, checkPermission, upload.single("file"), File.create_a_file);

  app.route("/upload").post(isValidToken, checkPermission, upload.single("file"), File.upload_a_file);
  app.get(["/download/:path/:name", "/download/:file_id"], File.download_a_file);

  app.route("/image/:path/:name").delete(isValidToken, checkPermission, File.remove_a_image);
};
