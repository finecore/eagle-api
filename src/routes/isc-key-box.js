import iscKeyBox from "controllers/isc-key-box";
import { jsonRes, sendRes, isValidToken, checkPermission } from "utils/api-util";
const { ERROR } = require("constants/constants");
const _ = require("lodash");

const valid = (req, res, next) => {
  var validationError = _.cloneDeep(ERROR.INVALID_PARAMETER);

  const { method } = req;
  const { id } = req.params;

  if (!id) validationError.detail.push("isc-key-box id 를 입력해 주세요!");
  if (isNaN(id)) validationError.detail.push("isc-key-box id 는 숫자를 입력해 주세요!");

  if (Object.keys(validationError.detail).length > 0) return jsonRes(req, res, validationError);
  else return next();
};

module.exports = (app) => {
  app.route("/isc/key/box/all/:serialno").get(isValidToken, iscKeyBox.list_all_isc_key_boxs);
  app.route("/isc/key/box/list/:filter/:order/:desc/:limit").get(isValidToken, iscKeyBox.list_a_isc_key_boxs);
  app.route("/isc/key/box/use/list").get(isValidToken, iscKeyBox.list_a_isc_key_boxs_use);

  app.route("/isc/key/box").post(isValidToken, iscKeyBox.create_a_isc_key_box);

  app
    .route("/isc/key/box/:id")
    .get(valid, isValidToken, iscKeyBox.read_a_isc_key_box)
    .put(valid, isValidToken, iscKeyBox.update_a_isc_key_box)
    .delete(valid, isValidToken, checkPermission, iscKeyBox.delete_a_isc_key_box);
};
