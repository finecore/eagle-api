import roomFee from "controllers/room-fee";
import { jsonRes, sendRes, isValidToken, checkPermission } from "utils/api-util";

module.exports = app => {
  // room  fee route.
  app
    .route("/room/fee/all/:place_id/:channel")
    .get(isValidToken, roomFee.list_all_room_fee)
    .delete(isValidToken, checkPermission, roomFee.delete_all_room_fee);

  app
    .route("/room/fee/type/:room_type_id/:channel")
    .get(isValidToken, roomFee.list_room_type_fee)
    .delete(isValidToken, checkPermission, roomFee.delete_room_type_fee);

  app
    .route("/room/fee/:id")
    .get(isValidToken, roomFee.read_a_room_fee)
    .put(isValidToken, checkPermission, roomFee.update_a_room_fee)
    .delete(isValidToken, checkPermission, roomFee.delete_a_room_fee);

  app.route("/room/fee").post(isValidToken, checkPermission, roomFee.create_a_room_fee);

  app
    .route("/room/fees/list") // 주의) 다른 라우터의 url 과 겹치지 않게 한다.
    .put(isValidToken, checkPermission, roomFee.update_list_room_fee)
    .post(isValidToken, checkPermission, roomFee.create_list_room_fee)
    .delete(isValidToken, checkPermission, roomFee.delete_list_room_fee);
};
