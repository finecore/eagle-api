import mileageLog from "controllers/mileage-log";
import { jsonRes, sendRes, isValidToken, checkPermission } from "utils/api-util";

module.exports = app => {
  // room  state route.
  app.route("/mileage/log/all/:place_id/:begin/:end").get(mileageLog.list_all_mileage_log);
  app.route("/mileage/log/list/:place_id/:limit").get(mileageLog.read_a_mileage_logs);
  app.route("/mileage/log/day/:place_id/:day").get(mileageLog.read_a_mileage_day_logs);
  app.route("/mileage/log/last/:place_id/:last_id").get(mileageLog.read_a_mileage_last_logs);

  app
    .route("/mileage/log/:id")
    .get(mileageLog.read_a_mileage_log)
    .put(mileageLog.update_a_mileage_log)
    .delete(isValidToken, checkPermission, mileageLog.delete_a_mileage_log);

  app.route("/mileage/log").post(mileageLog.create_a_mileage_log);
};
