import Version from "controllers/version";
import { jsonRes, sendRes, isValidToken, checkPermission } from "utils/api-util";

module.exports = app => {
  app
    .route([
      "/version/list/:type",
      "/version/list/:type/:model",
      "/version/list/:filter/:order/:desc/:limit"
    ])
    .get(isValidToken, Version.list_a_versions);

  app.route("/version").post(isValidToken, checkPermission, Version.create_a_version);

  app
    .route("/version/:id")
    .get(isValidToken, Version.read_a_version)
    .put(isValidToken, checkPermission, Version.update_a_version)
    .delete(isValidToken, checkPermission, Version.delete_a_version);

  app
    .route("/version/:path/:name/:newname")
    .put(isValidToken, checkPermission, Version.update_a_version);
};
