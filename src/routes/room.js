import room from "controllers/room";
import { jsonRes, sendRes, isValidToken, checkPermission } from "utils/api-util";
const { ERROR } = require("constants/constants");
const _ = require("lodash");

const valid = (req, res, next) => {
  var validationError = _.cloneDeep(ERROR.INVALID_PARAMETER);

  const { id } = req.params;

  if (!id) validationError.detail.push("room id 를 입력해 주세요!");
  if (isNaN(id)) validationError.detail.push("room id 는 숫자를 입력해 주세요!");

  if (Object.keys(validationError.detail).length > 0) return jsonRes(req, res, validationError);
  else return next();
};

module.exports = (app) => {
  app.route("/room/all").get(isValidToken, room.list_all_rooms);
  app.route("/room/all/:place_id").get(isValidToken, room.list_place_rooms);
  app.route("/room/list/:filter/:order/:desc/:limit").get(isValidToken, room.list_a_rooms);

  // 현재 예약 가능 하고 판매 상태가 아닌 객실.
  app
    .route(["/room/enable/reserv/list/:filter/:order/:desc/:limit", "/room/enable/reserv/:place_id", "/room/enable/reserv/:place_id/:room_type_id"])
    .get(isValidToken, room.list_a_enable_reserv_rooms);

  app.route("/room/").post(isValidToken, checkPermission, room.create_a_room);

  app.route("/room/:id").get(valid, isValidToken, room.read_a_room).put(valid, isValidToken, checkPermission, room.update_a_room).delete(valid, isValidToken, checkPermission, room.delete_a_room);

  app.route("/room/name/:place_id/:name").get(isValidToken, room.read_a_room_by_name);
};
