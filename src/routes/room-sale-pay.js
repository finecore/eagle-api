import roomSalePay from "controllers/room-sale-pay";
import { jsonRes, sendRes, isValidToken, checkPermission } from "utils/api-util";
const { ERROR } = require("constants/constants");
const _ = require("lodash");

const valid = (req, res, next) => {
  var validationError = _.cloneDeep(ERROR.INVALID_PARAMETER);

  const { id } = req.params;

  if (!id) validationError.detail.push("room_sale_pay id 를 입력해 주세요!");
  if (isNaN(id)) validationError.detail.push("room_sale_pay id 는 숫자를 입력해 주세요!");

  if (Object.keys(validationError.detail).length > 0) return jsonRes(req, res, validationError);
  else return next();
};

module.exports = app => {
  // room  sale route.

  app
    .route(["/room/salepay/all/:place_id", "/room/salepay/all/:place_id/:begin/:end"])
    .get(isValidToken, roomSalePay.list_all_room_sale_pay);

  app
    .route(["/room/salepay/all/:place_id/:excel", "/room/salepay/all/:place_id/:begin/:end/:excel"])
    .get(roomSalePay.list_all_room_sale_pay);

  app
    .route("/room/salepay/:id")
    .get(valid, isValidToken, roomSalePay.read_a_room_sale_pay)
    .put(valid, isValidToken, checkPermission, roomSalePay.update_a_room_sale_pay)
    .delete(valid, isValidToken, checkPermission, roomSalePay.delete_a_room_sale_pay);

  app
    .route(["/room/salepay/list/:room_id", "/room/salepay/list/:room_id/:begin/:end"])
    .get(isValidToken, roomSalePay.list_a_room_sale_pay);

  app
    .route(["/room/salepay/list/:room_id/:excel", "/room/salepay/list/:room_id/:begin/:end/:excel"])
    .get(roomSalePay.list_a_room_sale_pay);

  app
    .route("/room/salepay/sum/place/:place_id/:begin/:end")
    .get(isValidToken, roomSalePay.read_a_room_sale_pay_sum_place);

  app
    .route(" /room/salepay/sum/place/:place_id/:begin/:end/:excel")
    .get(roomSalePay.read_a_room_sale_pay_sum_place);

  app
    .route(["/room/salepay/today/:room_id", "/room/salepay/sum/:room_id/:begin/:end"])
    .get(isValidToken, roomSalePay.read_a_room_sale_pay_sum);

  app.route(["/room/salepay/today/:room_id/:excel"]).get(roomSalePay.read_a_room_sale_pay_sum);

  app
    .route("/room/salepay/sum/all/:begin/:end")
    .get(isValidToken, roomSalePay.read_all_room_sale_pay_sum);

  app.route("/room/salepay/sum/all/:begin/:end/:excel").get(roomSalePay.read_all_room_sale_pay_sum);

  app
    .route("/room/salepay")
    .post(isValidToken, checkPermission, roomSalePay.create_a_room_sale_pay);
};
