import roomView from "controllers/room-view";
import { jsonRes, sendRes, isValidToken, checkPermission } from "utils/api-util";

module.exports = app => {
  // room view route.
  app.route("/room/view/all/:place_id").get(isValidToken, roomView.list_all_room_views);

  app
    .route("/room/view/:place_id")
    .post(isValidToken, checkPermission, roomView.create_a_room_view);

  app
    .route("/room/view/:id")
    .get(isValidToken, roomView.read_a_room_view)
    .put(isValidToken, checkPermission, roomView.update_a_room_view)
    .delete(isValidToken, checkPermission, roomView.delete_a_room_view);
};
