import User from "controllers/user.js";
import { jsonRes, sendRes, isValidToken, checkPermission } from "utils/api-util";

module.exports = (app) => {
  app.route(["/user/list/place/:place_id", "/user/list/:filter/:order/:desc/:limit"]).get(isValidToken, User.list_a_users);

  app.route("/user").post(isValidToken, checkPermission, User.create_a_user);

  app.route("/user/:id").get(isValidToken, User.read_a_user).put(isValidToken, checkPermission, User.update_a_user).delete(isValidToken, checkPermission, User.delete_a_user);
};
