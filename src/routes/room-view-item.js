import roomViewItem from "controllers/room-view-item";
import {
  jsonRes,
  sendRes,
  isValidToken,
  checkPermission
} from "utils/api-util";

module.exports = app => {
  // room view item route.
  app
    .route("/room/view/item")
    .post(isValidToken, checkPermission, roomViewItem.create_a_room_view_item);

  app
    .route("/room/view/item/all/:place_id")
    .get(isValidToken, roomViewItem.list_all_room_view_items);

  app
    .route("/room/view/item/view/:view_id")
    .get(isValidToken, roomViewItem.list_room_view_items)
    .put(isValidToken, checkPermission, roomViewItem.update_a_room_view_items)
    .delete(
      isValidToken,
      checkPermission,
      roomViewItem.delete_a_room_view_items
    );

  app
    .route("/room/view/item/:id")
    .get(isValidToken, roomViewItem.read_a_room_view_item)
    .put(isValidToken, checkPermission, roomViewItem.update_a_room_view_item)
    .delete(
      isValidToken,
      checkPermission,
      roomViewItem.delete_a_room_view_item
    );
};
