import iscSet from "controllers/isc-set";
import { jsonRes, sendRes, isValidToken, checkPermission } from "utils/api-util";
const { ERROR } = require("constants/constants");
const _ = require("lodash");

const valid = (req, res, next) => {
  var validationError = _.cloneDeep(ERROR.INVALID_PARAMETER);

  const { method } = req;
  const { id } = req.params;

  if (!id) validationError.detail.push("isc-set id 를 입력해 주세요!");
  if (isNaN(id)) validationError.detail.push("isc-set id 는 숫자를 입력해 주세요!");

  if (Object.keys(validationError.detail).length > 0) return jsonRes(req, res, validationError);
  else return next();
};

module.exports = app => {
  app.route("/isc/set/list/:filter/:order/:desc/:limit").get(isValidToken, iscSet.list_a_isc_sets);
  app.route("/isc/set/serialno/:serialno").get(isValidToken, iscSet.read_a_isc_set_serialno);
  app.route("/isc/set/place/:place_id").get(isValidToken, iscSet.list_a_isc_set_place_id);

  app.route("/isc/set").post(isValidToken, iscSet.create_a_isc_set);

  app
    .route("/isc/set/:id")
    .get(valid, isValidToken, iscSet.read_a_isc_set)
    .put(valid, isValidToken, iscSet.update_a_isc_set)
    .delete(valid, isValidToken, checkPermission, iscSet.delete_a_isc_set);
};
