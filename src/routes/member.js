import member from "controllers/member";
import { jsonRes, sendRes, isValidToken, checkPermission } from "utils/api-util";
const { ERROR } = require("constants/constants");
const _ = require("lodash");

const valid = (req, res, next) => {
  var validationError = _.cloneDeep(ERROR.INVALID_PARAMETER);

  const { method } = req;
  const { id, type, name } = req.params;

  if (!id) validationError.detail.push("member id 를 입력해 주세요!");
  if (isNaN(id)) validationError.detail.push("member id 는 숫자를 입력해 주세요!");

  if (Object.keys(validationError.detail).length > 0) return jsonRes(req, res, validationError);
  else return next();
};

module.exports = app => {
  app
    .route(["/member/all", "/member/list/:filter/:order/:desc/:limit"])
    .get(isValidToken, member.list_a_members);
  app.route("/member/place/:place_id").get(isValidToken, member.list_place_members);

  app.route("/member").post(member.create_a_member);

  app
    .route("/member/:id")
    .get(valid, isValidToken, member.read_a_member)
    .put(valid, isValidToken, member.update_a_member)
    .delete(valid, isValidToken, checkPermission, member.delete_a_member);

  app.route("/member/:place_id/phone/:phone").get(isValidToken, member.read_place_phone_member);
  app.route("/member/:place_id/phone/:phone").put(isValidToken, member.update_place_member_phone);
};
