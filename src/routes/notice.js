import Notice from "controllers/notice";
import { jsonRes, sendRes, isValidToken, checkPermission } from "utils/api-util";

module.exports = app => {
  app
    .route(["/notice/list/:filter/:between/:begin/:end", "/notice/list/:filter/:between/:begin/:end/:limit", "/notice/list/:filter/:between/:begin/:end/:limit/:all"])
    .get(isValidToken, Notice.list_a_notices);

  app.route("/notice").post(isValidToken, checkPermission, Notice.create_a_notice);

  app.route("/notice/:id").get(isValidToken, Notice.read_a_notice).put(isValidToken, checkPermission, Notice.update_a_notice).delete(isValidToken, checkPermission, Notice.delete_a_notice);
};
