const _ = require("lodash");
const moment = require("moment");
const Preferences = require("../model/preferences");
const MailReceiver = require("model/mail-receiver");

/** 오류 정의 */
const ERROR = {
  DEFAULT: {
    code: 400,
    message: "요청 처리중 오류가 발생 했습니다.",
    detail: "관리자에게 문의해 주세요.",
  },
  INVALID_PARAMETER: {
    code: 400,
    message: "요청 정보가 올바르지 않습니다.",
    detail: [],
  },
  NO_DATA: {
    code: 400,
    message: "요청 정보가 존재 하지 않습니다.",
    detail: "요청 정보 확인 후 다시 시도해 주세요.",
  },
  DUPLICATE_DATA: {
    code: 400,
    message: "동일한 정보가 이미 존재 합니다.",
    detail: "입력 정보 확인 후 다시 시도해 주세요.",
  },
  INVALID_ARGUMENT: {
    code: 400,
    message: "입력 정보가 올바르지 않습니다.",
    detail: "입력 정보 확인 후 다시 시도해 주세요.",
  },
  INVALID_USER: {
    code: 401,
    message: "유효한 사용자가 아닙니다.",
    detail: "관리자에게 문의해 주세요.",
  },
  NO_CHANNEL: {
    code: 401,
    message: "채널 정보가 없습니다.",
    detail: "채널 정보를 확인해 주세요.",
  },
  NO_TOKEN: {
    code: 401,
    message: "토큰 정보가 없습니다.",
    detail: "토큰 정보를 확인해 주세요.",
  },
  NO_CERTIFICATION: {
    code: 401,
    message: "인증 정보가 없습니다.",
    detail: "인증 후 다시 시도해 주세요.",
  },
  INVALID_CERTIFICATION: {
    code: 401,
    message: "인증 정보가 유효하지 않습니다.",
    detail: "관리자에게 문의해 주세요.",
  },
  INVALID_USER_INFO: {
    code: 401,
    message: "사용자 인증 정보가 올바르지 않습니다.",
    detail: "사용자 정보를 확인해 주세요.",
  },
  INVALID_DEVICE: {
    code: 401,
    message: "인증 장비가 유효하지 않습니다.",
    detail: "관리자에게 문의해 주세요.",
  },
  REQUIRE_CERTIFICATION: {
    code: 401,
    message: "필수 인증 정보가 없습니다.",
    detail: "업소 아이디를 넣어 주세요.",
  },
  EXPIRE_CERTIFICATION: {
    code: 401,
    message: "인증 기간이 만료 되었습니다.",
    detail: "관리자에게 문의해 주세요.",
  },
  IDM_EXPIRE_CERTIFICATION: {
    code: 401,
    message: "IDM 인증 기간이 만료 되었습니다.",
    detail: "관리자에게 문의해 주세요.",
  },
  ISG_EXPIRE_CERTIFICATION: {
    code: 401,
    message: "ISG 인증 기간이 만료 되었습니다.",
    detail: "관리자에게 문의해 주세요.",
  },
  INVALID_AUTHORITY: {
    code: 401,
    message: "접근 권한이 없습니다.",
    detail: "로그인 후 다시 시도해 주세요.",
  },
  NO_SERIALNO: {
    code: 401,
    message: "장비 일련번호가 없습니다.",
    detail: "일련번호를 넣어 주세요.",
  },
  NO_SERVER_MODE: {
    code: 401,
    message: "서버 모드 정보가 없습니다.",
    detail: "전송 서버를 확인해 주세요.",
  },
  INVALID_SERIALNO: {
    code: 401,
    message: "장비 일련번호가 올바르지 않습니다.",
    detail: "일련번호를 확인해 주세요.",
  },
  ALREADY_INTERRUPT: {
    code: 403,
    message: "다른 고객이 사용 중 입니다.",
    detail: "잠시 후 다시 시도해 주세요.",
  },
  INVALID_USER_ID: {
    code: 403,
    message: "사용 할 수 없는 아이디 입니다.",
    detail: "다른 아이디를 입력해 주세요.",
  },
  DUPLICATE_ROOM_SALE: {
    code: 403,
    message: "중복 판매를 할 수 없습니다.",
    detail: "이미 판매된 객실 입니다.",
  },
  CHECK_OUT_ROOM_SALE: {
    code: 403,
    message: "요금 변경을 할 수 없습니다.",
    detail: "이미 퇴실 처리된 객실 입니다.",
  },
  BAD_FIELD_ERROR: {
    code: 500,
    message: "테이블에 컬럼이 존재 하지 않습니다.",
    detail: "입력 항목을 확인해 주세요.",
  },
  MAIL_DUPLICATE_ERROR: {
    code: 400,
    message: "최근 동일한 수신자에게 동일한 내용으로 메일이 발송되었습니다.",
    detail: "잠시후에 다시 시도 하시기 바랍니다.",
  },
};

/** 환경 설정값 (room_state 에서 자주 사용 되므로 메모리에 올려놓고 사용한다.) */
class preferences {
  constructor() {
    this.timer = null;
    this.list = [];
    this.mail_receivers = [];
    this.data = {};
    this.expireMin = 60; // 캐싱 시간(분)

    this.Init();
  }

  Init() {
    if (this.timer) clearInterval(this.timer);

    // 타이머.
    this.timer = setInterval(() => {
      this.Select();
    }, 60 * 1000);

    this.Select();

    console.log("- PREFERENCES Init");
  }

  Select() {
    Preferences.selectAllPreferences((err, preferences) => {
      if (!err && preferences) {
        this.list = [];

        _.map(preferences, (row) => {
          const { place_id } = row;

          if (!this.data[place_id]) {
            row.expire = moment().add(this.expireMin, "minute");
            row.sale_next_delete = undefined;
            row.state_next_delete = undefined;
          }

          // 기존 정보에 덮어씌운다.
          this.list.push(this.Set(place_id, row));
        });

        console.log("- PREFERENCES Select", this.list.length);
      }
    });

    let filter = "1=1",
      limit = "10000",
      order = "a.id",
      desc = "asc";

    MailReceiver.selectMailReceivers(filter, order, desc, limit, (err, mail_receivers) => {
      if (!err && mail_receivers) {
        this.mail_receivers = mail_receivers;
        console.log("- MailReceiver Select", this.mail_receivers.length);
      }
    });
  }

  List(callback) {
    callback(this.list);
  }

  Set(place_id, row) {
    let data = this.data[place_id] || {};

    if (!data.id || JSON.stringify(data) !== JSON.stringify(row)) {
      this.data[place_id] = _.merge({}, data || {}, row); // 덮어쓰기.
      // console.log("- PREFERENCES Set", { place_id });
    }

    return this.data[place_id];
  }

  Get(place_id, callback) {
    let preference = this.data[place_id];

    // console.log("- PREFERENCES Get", place_id);

    if (preference && preference.expire < moment()) preference = null;

    if (!preference || !preference.id || !preference.auto_check_in) {
      Preferences.selectPreferences(place_id, (err, preferences) => {
        if (!err && preferences[0]) {
          preferences[0].expire = moment().add(this.expireMin, "minute");

          this.Set(place_id, preferences[0]);

          callback(this.data[place_id]);
        } else {
          callback({});
        }
      });
    } else {
      callback(preference);
    }
  }

  MailReceiverList(type, callback) {
    let list = _.filter(this.mail_receivers, (v) => v.type === type && v.email);
    callback(list);
  }
}

const PREFERENCES = new preferences();

module.exports = { ERROR, PREFERENCES };
