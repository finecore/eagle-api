import createError from "http-errors";
import express from "express";
import ReactEngine from "express-react-views";
import path from "path";
import cookieParser from "cookie-parser";
import bodyParser from "body-parser";
import logger from "morgan";
import cors from "cors";

// app routes..
import appRouters from "./routes/approutes";
import { jsonRes, sendRes, isValidToken, checkPermission } from "utils/api-util";

var app = express();

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");
app.set("view engine", "jsx");
app.engine("jsx", ReactEngine.createEngine());

app.use(logger("dev"));
app.use(express.json({ limit: "10mb" }));
app.use(express.urlencoded({ extended: false, limit: "10mb" }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

// CORS 설정
app.use(cors());

app.use("/static", express.static(path.resolve(__dirname, "../public/static")));
app.use("/upload", express.static(path.resolve(__dirname, "../public/static/upload")));

// Middlewares
app.use(bodyParser.json({ limit: "10mb" }));
app.use(bodyParser.urlencoded({ extended: false, limit: "10mb" }));

// JWT add.
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Headers", "content-type, x-access-token"); //1
  next();
});

// App Routers.
appRouters(app);

// error handler
app.use(function (err, req, res, next) {
  console.log("app error:" + err.stack.split("at"), req.baseUrl, req.originalUrl);

  // 공통 포멧으로 오류 반환.
  jsonRes(req, res, {
    code: err.status || 500,
    message: err.message || err,
    detail: req.baseUrl + req.originalUrl + (err.stack && req.app.get("env") === "development") ? " at" + err.stack.split("at").join("\n ") : "",
  });
});

module.exports = app;
