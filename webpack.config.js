const path = require("path");

require("babel-polyfill");

module.exports = {
  context: __dirname,
  devtool: "source-map",
  entry: ["babel-polyfill", "./src/index.js"],
  output: {
    filename: "./build/build.js",
    path: path.resolve(__dirname)
  },
  serve: {}
};
